'''
/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

Generates compressed files of selected sizes via trail and error approach.  It tweeks the size of the sequences if required.


Created on 23 Oct 2014

@author: arobinson
'''
import random, sys, os, shutil, subprocess

seqcount = 100
seqlen = 251

tmpfile = '/tmp/dna.fa'

# compress = ['gzip', '-f', tmpfile]
# compext = "gz"

# compress = ['bzip2', '-f', tmpfile]
# compext = "bz2"

compress = ['xz', '-f', tmpfile]
compext = "xz"

tmpfilecomp = "%s.%s" % (tmpfile, compext)


# targetsizes = [8190, 8191, 8192, 8193, 8194]
# targetsizes = [8184, 8188, 8192, 8196, 8200] # xz can only generate files in multiples of 4 bytes
targetsizes = [1020,1024,1028] # xz can only generate files in multiples of 4 bytes
targetsize = 1024


# if len(sys.argv) == 3:
#     seqcount = int(sys.argv[1])
#     seqlen = int(sys.argv[2])

# loop until all targets are made
while len(targetsizes) > 0:

    # generate file
    f = open(tmpfile, 'w')
    if not f:
        sys.exit(1)
    for seqi in xrange(seqcount):
        f.write(">seq%s\n" % (str(seqi+1).zfill(3)))
        dna = ""
        for basei in xrange(seqlen): # + random.choice(range(-5,6))
            dna += random.choice('ACGT')
        f.write("%s\n" % dna)
    f.close()
        
    # compress file
    subprocess.call(compress)
    
    # get file size
    size = os.path.getsize(tmpfilecomp)
#     print "size: %s" % size
    print size
    
    # copy back if needed
    if size in targetsizes:
        
        print "Generated file of size: %s bytes (seqs: %s, len: %s)" % (size, seqcount, seqlen)
        
        shutil.move(tmpfilecomp, "%s-%sx%s.fa.%s" % (size, seqcount, seqlen, compext))
        
        targetsizes.remove(size)
    
    # adjust size if needed
    if size > targetsize * 1.1:
        seqlen -= 10
        print "Seqlen: %s" % seqlen
    elif size > targetsize * 1.01:
        seqlen -= 1
        print "Seqlen: %s" % seqlen
    elif size < targetsize / 1.1:
        seqlen += 10
        print "Seqlen: %s" % seqlen
    elif size < targetsize / 1.01:
        seqlen += 1
        print "Seqlen: %s" % seqlen

# next attempt

