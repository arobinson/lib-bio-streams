# settings.sh
# Created on: 10 Nov 2014
#     Author: arobinson

# contains a few build settings that are used by build.sh and clean-build.sh

# make shared libraries (1 = on, other = off)
SHARED=1

# Number of CPU's for building
CPUS=1

# Debug build (i.e. gdb symbols) (1 = on, other = off)
DEBUG=1

# paths
INSTALLDIR=`pwd`/deploy
BUILDDIR=`pwd`/build


