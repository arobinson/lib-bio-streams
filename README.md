# Lib Bio Streams #

A C++ library for bioinformatics tool development that support unix/linux streams for interconnection

## Install ##

1. Install prerequisites:
    1. **cmake**: should come with your computer. (http://www.cmake.org/)
    2. [optional] **zlib**: for .gz file support. Should come with your computer. (http://www.zlib.net/)
    3. [optional] **bzlib**: for .bz2 file support. Should come with your computer. (http://www.bzip.org/)
    4. [optional] **liblzma**: for .xz file support. Should come with your computer. (http://tukaani.org/xz/)
2. Download source: (https://bitbucket.org/arobinson/lib-bio-streams/downloads#tag-downloads [click the 'tags' tab])
    1. export VERSION=0.4.0 #(update number as needed)
    2. wget https://bitbucket.org/arobinson/lib-bio-streams/get/v$VERSION.tar.gz -O libbiostreams_v$VERSION.tar.gz
3. Build:
    1. tar xf libbiostreams_v$VERSION.tar.gz
    2. mv arobinson-lib-bio-streams-* libbiostreams_v$VERSION
    3. cd libbiostreams_v$VERSION
    4. mkdir build
    5. cd build
    6. cmake ..
        1. Check that it found each of the optional libaries that you installed and the correct version.  If not, you may need to help cmake find them with (e.g. -DCMAKE_FIND_ROOT_PATH="PATH_TO_LIB_ROOT")
        2. If you want to install lib-bio-streams in an alternate PATH (i.e. not /usr/local) add: -DCMAKE_INSTALL_PREFIX=PATH
    7. make
    8. make install


## Examples ##

### Example project/tool ###

See the Examples directory for an few small example projects showing what is needed to get started with LibBioStreams

### Bio Stream Tools ###

For example tools and project setup requirements see the Bio Stream Tools repository which contains a number of fully functional applications which demonstrates some of the more advanced features.

https://bitbucket.org/arobinson/bio-stream-tools/overview