/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Configurator.cpp
 *
 *  Created on: 05/09/2014
 *      Author: arobinson
 */

#include <cstdio>

#include <iostream>

#include <libbiostreams/version.h>

#include <libbiostreams/io/EmptyInput.h>
#include <libbiostreams/io/OpenErrorInput.h>
#include <libbiostreams/io/PlainInput.h>
#include <libbiostreams/io/PlainOutput.h>
#include <libbiostreams/io/UnsupportedInput.h>

#ifdef LBS_ZLIB_FOUND
# include <libbiostreams/io/ZLibInput.h>
# include <libbiostreams/io/ZLibOutput.h>
#endif
#ifdef LBS_BZLIB_FOUND
# include <libbiostreams/io/BZLibInput.h>
# include <libbiostreams/io/BZLibOutput.h>
#endif
#ifdef LBS_LZMA_FOUND
# include <libbiostreams/io/LZMAInput.h>
# include <libbiostreams/io/LZMAOutput.h>
#endif

#include "Configurator.h"

#define Q(x) #x
#define QUOTE(x) Q(x)

Configurator::Configurator(int argc, char ** argv) {
	this->argc = argc;
	this->argv = argv;

	cmdname = "Unknown";
	defaultInput = NULL;
	defaultOutput = NULL;
	defaultLog = NULL;

	minAnonymousArgs = 0;
	maxAnonymousArgs = 0;
	variableAnonymousArgs = false;
	cacheInStream = true;

	major = -1;
	minor = 0;
	patch = 0;
	versionDate = "";

	/// add pre-defined flags/options
	this->addFlag("help", 'h', new std::string("help"), 0, 1, "Display (this) help and exit");
	this->addFlag("version", '\0', new std::string("version"), 0, 1, "Display version information and exit");
}

Configurator::~Configurator() {
	for (std::map<std::string,Flag*>::iterator it=flagsByName.begin(); it!=flagsByName.end(); ++it)
		delete it->second;
	for (std::map<std::string,Option*>::iterator it=optionsByName.begin(); it!=optionsByName.end(); ++it)
		delete it->second;
	for (std::map<std::string,Stream*>::iterator it=streamsByName.begin(); it!=streamsByName.end(); ++it)
		delete it->second;
	for (std::map<std::string,AnonymousOption*>::iterator it=anonymousArgsByName.begin(); it!=anonymousArgsByName.end(); ++it)
		delete it->second;

	//TODO: clean up all lists of owned instances
	for (std::map<std::string, InputBase*>::iterator it=inStreamObjs.begin(); it != inStreamObjs.end(); ++it) {
		delete it->second;
	}
	inStreamObjs.clear();
	for (std::vector<InputBase*>::iterator it=inStreamObjsList.begin(); it != inStreamObjsList.end(); ++it)
		delete *it;
	inStreamObjsList.clear();
	for (std::map<std::string, OutputBase*>::iterator it=outStreamObjs.begin(); it != outStreamObjs.end(); ++it){
		delete it->second;
	}
	outStreamObjs.clear();
}


/**
 * Add commandline flag definition
 *
 * e.g. -h --help etc.
 *
 * NOTE: multiple single flags can be merged. e.g. -ht is same as -h -t
 *
 * @param name, a unique name for this option
 * @param flag, a 1 letter flag, '\0' for no flag
 * @param longflag, a multi-letter flag, NULL for no flag.
 * @param minoccurs, the minimum number of times the user needs to provide this option
 * @param maxoccurs, the maximum number of times the user can provide this option (0=unlimited)
 * @param def, the default value for the option if user doesn't provide one, NULL means no default.
 * @return false on error (usually duplicate name, flag or longflag)
 */
bool Configurator::addFlag(const std::string name, char flag, std::string *longflag, unsigned short minoccurs,
		unsigned short maxoccurs, std::string docstring) {

	// check name, flag and longflag are unique
	if (names.find(name) == names.end() &&
			flags.find(flag) == flags.end() &&
			(longflag == NULL || longflags.find(*longflag) == longflags.end())) {
		Flag *f = new Flag(name, flag, longflag, minoccurs, maxoccurs, docstring);
		flagsByName[name] = f;
		names.insert(name);
		if (flag != '\0') {
			flagsByFlag[flag] = f;
			flags.insert(flag);
		}
		if (longflag != NULL) {
			flagsByLongFlag[*longflag] = f;
			longflags.insert(*longflag);
		}

		return true;
	}

#ifdef DEBUG
	std::cerr << "Error: name, flag or longflag already exists" << std::endl;
#endif

	return false;
}

/**
 * Add commandline option definition
 *
 * e.g. -f <file> --infile=<file>
 * NOTE: multiple single flags can be merged. e.g. -ht <val> is same as -h -t <val>
 *
 * @param name, a unique name for this option
 * @param flag, a 1 letter flag, '\0' for no flag
 * @param longflag, a multi-letter flag, NULL for no flag.
 * @param minoccurs, the minimum number of times the user needs to provide this option
 * @param maxoccurs, the maximum number of times the user can provide this option (0=unlimited)
 * @param def, the default value for the option if user doesn't provide one, NULL means no default.
 * @return false on error (usually duplicate name, flag or longflag)
 */
bool Configurator::addOption(const std::string name, char flag, std::string *longflag, unsigned short minoccurs,
		unsigned short maxoccurs, std::string *def, std::string docstring) {

	// check name, flag and longflag are unique
	if (names.find(name) == names.end() &&
			flags.find(flag) == flags.end() &&
			(longflag == NULL || longflags.find(*longflag) == longflags.end())) {

		Option *o = new Option(name, flag, longflag, minoccurs, maxoccurs, def, docstring);
		optionsByName[name] = o;
		names.insert(name);
		if (flag != '\0') {
			optionsByFlag[flag] = o;
			flags.insert(flag);
		}
		if (longflag != NULL) {
			optionsByLongFlag[*longflag] = o;
			longflags.insert(*longflag);
		}

		return true;
	}

#ifdef DEBUG
	std::cerr << "Error: name, flag or longflag already exists" << std::endl;
#endif

	return false;
}

/**
 * Adds an option that is not proceeded by a flag.  Add multiple groups of
 * anonymous options in the order that user will need to type them.  You
 * can only add one anonymous option with a variable amount of occurrences.
 *
 * NOTE: If name matches an option/stream name then it becomes an alternate
 * method of providing value for that option.
 *
 * e.g.  <file> [<file> ...]
 *
 * @param name, a unique name for this option
 * @param minoccurs, the minimum number of times the user needs to provide this option
 * @param maxoccurs, the maximum number of times the user can provide this option (0=unlimited)
 * @param def, the default value for the option if user doesn't provide one, NULL means no default.
 * @param docstring, the explanation of flag displayed on help screen
 */
bool Configurator::addAnonymousOption(const std::string name, unsigned short minoccurs,
		unsigned short maxoccurs, std::string *def, std::string docstring) {

	// check for multiple variable lengths
	if (minoccurs == 0 || maxoccurs == 0) {
		if (variableAnonymousArgs) {
#ifdef DEBUG
			std::cerr << "Error: multiple variable length anonymous args specified" << std::endl;
#endif
			return false;
		}

		variableAnonymousArgs = true;
	}

	// store
	AnonymousOption *opt = new AnonymousOption(name, minoccurs, maxoccurs, def, docstring);
	anonymousArgsByName[name] = opt;
	anonymousArgsOrder.push_back(opt);
	names.insert(name);
	minAnonymousArgs += minoccurs;
	maxAnonymousArgs += maxoccurs;

	// check for option/stream with same name
	std::map<std::string, Option*>::iterator optit = optionsByName.find(name);
	if (optit != optionsByName.end()) {
		opt->optionLink = optit->second;
		optit->second->anonymousOptionLink = opt;
	}
	std::map<std::string, Stream*>::iterator strit = streamsByName.find(name);
	if (strit != streamsByName.end()) {
		opt->streamLink = strit->second;
		strit->second->anonymousOptionLink = opt;
	}

	return true;
}

/**
 * Add an input stream
 *
 * e.g. longflag="db" => --db=XXXX
 *
 * @param name, a unique name to identify this stream
 * @param flag, the 1 letter flag used to specify input file/stdin, '\0' for no flag
 * @param longflag, the multi-letter flag used to specify input file/stdin, NULL for no flag.
 * @param minoccurs, the minimum number of times the user needs to provide this option
 * @param maxoccurs, the maximum number of times the user can provide this option (0=unlimited)
 * @param def, the default value for this stream. "-"=stdin
 * @return false on error (usually duplicate name, flag or longflag)
 */
bool Configurator::addInStream(const std::string name, char flag, std::string *longflag, unsigned short minoccurs,
		unsigned short maxoccurs, std::string *def, std::string docstring) {

	if (name.size() > 0 && names.find(name) == names.end() &&
			flags.find(flag) == flags.end() &&
			(longflag == NULL || longflags.find(*longflag) == longflags.end()) &&
			((longflag != NULL && longflags.find(*longflag + CONFIGURATOR_COMPRESSION_FLAG) == longflags.end()) ||
					(longflag == NULL && longflags.find(name + CONFIGURATOR_COMPRESSION_FLAG) == longflags.end()))) {

		Stream *s = new Stream(name, flag, longflag, minoccurs, maxoccurs, def, docstring, IN);
		CompressStreamOption *o = new CompressStreamOption(s);
		s->compressOptionLink = o;

		streamsByName[name] = s;
		names.insert(name);
		if (flag != '\0') {
			streamsByFlag[flag] = s;
			flags.insert(flag);
		}
		if (longflag != NULL) {
			streamsByLongFlag[*longflag] = s;
			longflags.insert(*longflag);
			optionsByLongFlag[*o->longoption] = o;
			longflags.insert(*o->longoption);
		}
		else {
			optionsByLongFlag[*o->longoption] = o;
			longflags.insert(*o->longoption);
		}

		if (defaultInput == NULL)
			defaultInput = s;

		return true;
	}

#ifdef DEBUG
	std::cerr << "Error: name, flag or longflag already exists" << std::endl;
#endif

	return false;
}

/**
 * Add an output stream
 *
 * e.g. longflag="db" => --db=XXXX
 *
 * @param name, a unique name to identify this stream
 * @param flag, the 1 letter flag used to specify input file/stdin, '\0' for no flag
 * @param longflag, the multi-letter flag used to specify input file/stdin, NULL for no flag.
 * @param minoccurs, the minimum number of times the user needs to provide this option
 * @param maxoccurs, the maximum number of times the user can provide this option (0=unlimited)
 * @param def, the default value for this stream. "-"=stdout, "="=stderr
 * @return false on error (usually duplicate name, flag or longflag)
 */
bool Configurator::addOutStream(const std::string name, char flag, std::string *longflag, unsigned short minoccurs,
		unsigned short maxoccurs, std::string *def, std::string docstring) {

	if (names.find(name) == names.end() &&
			flags.find(flag) == flags.end() &&
			(longflag == NULL || longflags.find(*longflag) == longflags.end()) &&
			((longflag != NULL && longflags.find(*longflag + CONFIGURATOR_COMPRESSION_FLAG) == longflags.end()) ||
					(longflag == NULL && longflags.find(name + CONFIGURATOR_COMPRESSION_FLAG) == longflags.end()))) {

		Stream *s = new Stream(name, flag, longflag, minoccurs, maxoccurs, def, docstring, OUT);
		CompressStreamOption *o = new CompressStreamOption(s);
		s->compressOptionLink = o;

		streamsByName[name] = s;
		names.insert(name);
		if (flag != '\0') {
			streamsByFlag[flag] = s;
			flags.insert(flag);
		}
		if (longflag != NULL) {
			streamsByLongFlag[*longflag] = s;
			longflags.insert(*longflag);
			optionsByLongFlag[*o->longoption] = o;
			longflags.insert(*o->longoption);
		}
		else {
			optionsByLongFlag[*o->longoption] = o;
			longflags.insert(*o->longoption);
		}

		if (defaultOutput == NULL)
			defaultOutput = s;

		return true;
	}

#ifdef DEBUG
	std::cerr << "Error: name, flag or longflag already exists" << std::endl;
#endif

	return false;
}

/**
 * Add an log stream (an output stream intended for human readable log/error messages
 *
 * e.g. longflag="db" => --db=XXXX
 *
 * @param name, a unique name to identify this stream
 * @param flag, the 1 letter flag used to specify input file/stdin, '\0' for no flag
 * @param longflag, the multi-letter flag used to specify input file/stdin, NULL for no flag.
 * @param minoccurs, the minimum number of times the user needs to provide this option
 * @param maxoccurs, the maximum number of times the user can provide this option (0=unlimited)
 * @param def, the default value for this stream. "-"=stdout, "="=stderr
 * @return false on error (usually duplicate name, flag or longflag)
 */
bool Configurator::addLogStream(const std::string name, char flag, std::string *longflag, unsigned short minoccurs,
		unsigned short maxoccurs, std::string *def, std::string docstring) {

	if (names.find(name) == names.end() &&
			flags.find(flag) == flags.end() &&
			(longflag == NULL || longflags.find(*longflag) == longflags.end()) &&
			((longflag != NULL && longflags.find(*longflag + CONFIGURATOR_COMPRESSION_FLAG) == longflags.end()) ||
					(longflag == NULL && longflags.find(name + CONFIGURATOR_COMPRESSION_FLAG) == longflags.end()))) {

		Stream *s = new Stream(name, flag, longflag, minoccurs, maxoccurs, def, docstring, LOG);
		CompressStreamOption *o = new CompressStreamOption(s);
		s->compressOptionLink = o;

		streamsByName[name] = s;
		names.insert(name);
		if (flag != '\0') {
			streamsByFlag[flag] = s;
			flags.insert(flag);
		}
		if (longflag != NULL) {
			streamsByLongFlag[*longflag] = s;
			longflags.insert(*longflag);
			optionsByLongFlag[*o->longoption] = o;
			longflags.insert(*o->longoption);
		}
		else {
			optionsByLongFlag[*o->longoption] = o;
			longflags.insert(*o->longoption);
		}

		if (defaultLog == NULL)
			defaultLog = s;

		return true;
	}

#ifdef DEBUG
	std::cerr << "Error: name, flag or longflag already exists" << std::endl;
#endif

	return false;
}

/**
 * Adds text on help screen before stream/option/flags sections (after "Usage: ... line)
 *
 * @param doc, the text to add
 */
void Configurator::setPreamble(std::string doc) {
	this->preamble = doc;
}

/**
 * Adds text at end of help screen
 *
 * @param doc, the text to add
 */
void Configurator::setPostamble(std::string doc) {
	this->postamble = doc;
}

/**
 * Adds a new section to the help screen (after stream/option/flags sections
 */
void Configurator::addSection(std::string name, std::string doc) {
	this->sections.push_back(new Section(name, doc));
}

/**
 * Parses, assigns and validates options.  If help is requested then help
 * message is displayed
 *
 * @return unsigned short, 0= success, 1= help was displayed, 2= parse error, 3=assign/validation error
 */
unsigned short Configurator::processesOptions(OutputBase* log) {

	if (!parseOptions()) {
		printUsageSummary(log);
		printErrors(log);
		return 2;
	}
	if (isHelp()) {
		printHelp(log);
		return 1;
	}
	if (isFlagSet("version")) {
		if (log == NULL)
			log = getDefaultLogStream();
		if (major < 0) {
			*log << "Version: Unknown (not provided by tool developer)" << std::endl;
		} else {
			*log << "Version: v" << major << "." << minor << "." << patch;
			if (versionDate.size() > 0)
				*log << " (" << versionDate << ")";
			else
				*log << " (Unknown release date)";
			*log << std::endl;
		}
		return 1;
	}
	if (!assignAndValidateOptions()) {
		printUsageSummary(log);
		printErrors(log);
		return 3;
	}
	return 0;
}

/**
 * Processes the argements passed in via the contrustor against the options
 * and streams defined with the addXXX methods.
 *
 * @return: bool, false on error
 */
bool Configurator::parseOptions() {

#define NEWOPT		1
#define FLAGVAL		2

	int mode = NEWOPT;
	bool noflags = false; ///< have we seen a '--' arg (i.e. all remaining args are anonymous)
	char flagname = '\0';
	std::string longflagname = "";

	Option* currentOption = NULL;
	Stream* currentStream = NULL;

	// get command name
	if (argc >= 1)
		cmdname = std::string(argv[0]);

	// process the args
	for (int i = 1; i < argc; i++) {
		char *arg = argv[i];
		if (mode == NEWOPT) {
			if (noflags) { //i.e. already received '--'
				anonymousArgs.push_back(std::string(arg));
			}
			else if (arg[0] == '-') { // start of flag
				if (arg[1] == '-') { // start of longflag
					if (arg[2] == '\0') { // '--' means no more flags
						noflags = true;
					} else {
						std::string longflagname = std::string(&arg[2]);
						std::map<std::string, Flag*>::iterator longflagit = flagsByLongFlag.find(longflagname);
						std::map<std::string, Option*>::iterator longoptit = optionsByLongFlag.find(longflagname);
						std::map<std::string, Stream*>::iterator longstrit = streamsByLongFlag.find(longflagname);
						if (longflagit != flagsByLongFlag.end()) { // flag
							mode = NEWOPT;
							longflagit->second->occurrences++;
						} else if (longoptit != optionsByLongFlag.end()) { // option
							mode = FLAGVAL;
							currentOption = longoptit->second;
						} else if (longstrit != streamsByLongFlag.end()) { // stream
							mode = FLAGVAL;
							currentStream = longstrit->second;
						} else {
							mode = NEWOPT;
							size_t equalspos = longflagname.find("=");
							if (equalspos != std::string::npos) { // --flag=val varient
								std::string longpairflagname = longflagname.substr(0,equalspos);

								longoptit = optionsByLongFlag.find(longpairflagname);
								longstrit = streamsByLongFlag.find(longpairflagname);
								if (longoptit != optionsByLongFlag.end()) { // option
									mode = NEWOPT;
									longoptit->second->values.push_back(longflagname.substr(equalspos+1));
								} else if (longstrit != streamsByLongFlag.end()) { // stream
									mode = NEWOPT;
									longstrit->second->values.push_back(longflagname.substr(equalspos+1));
								} else {
									errors << "Error: Unknown long flag '--" << longpairflagname << "'" << std::endl;
								}

							} else { // error
								errors << "Error: Unknown long flag '" << arg << "'" << std::endl;
							}
						}
					}
				}
				else { // short flag
					if (arg[1] == '\0'){
						anonymousArgs.push_back(std::string(arg));
					} else {
						int ci = 1;
						while (arg[ci] != '\0') {
							flagname = arg[ci];
							std::map<char, Flag*>::iterator flagit = flagsByFlag.find(flagname);
							std::map<char, Option*>::iterator optit = optionsByFlag.find(flagname);
							std::map<char, Stream*>::iterator strit = streamsByFlag.find(flagname);
							if (flagit != flagsByFlag.end()) { // flag
								mode = NEWOPT;
								flagit->second->occurrences++;
							} else if (optit != optionsByFlag.end()) { // option
								mode = FLAGVAL;
								currentOption = optit->second;
							} else if (strit != streamsByFlag.end()) { // stream
								mode = FLAGVAL;
								currentStream = strit->second;
							} else { // error
								errors << "Error: Unknown short flag '-" << flagname << "'" << std::endl;
							}
							++ci;
						}
					}
				}
			} else { // anonymous arg
				anonymousArgs.push_back(std::string(arg));
			}
		} else if (mode == FLAGVAL) { // option value
			mode = NEWOPT;
			if (currentOption != NULL) {
				currentOption->values.push_back(std::string(arg));
				currentOption = NULL;
			} else if (currentStream != NULL) {
				currentStream->values.push_back(std::string(arg));
				currentStream = NULL;
			} else {
				errors << "Error: Un-expected option" << std::endl;
			}
		} else { // ERROR
			errors << "Error: Unknown state during option parsing" << std::endl;
		}
	}

	return errors.str().size() == 0;
}

/**
 * Maps the anonymous options and checks occurrences of each option/flag
 *
 * @return bool, false on error
 */
bool Configurator::assignAndValidateOptions() {

	// validate anonymous occurrences
	if (anonymousArgsByName.size() == 0 && anonymousArgs.size() > 0) { // not expected but some given
		errors << "Error: No anonymous options expected, " << anonymousArgs.size() << " provided" << std::endl;
	} else if (maxAnonymousArgs < CONFIGURATOR_UNLIMITED) { // 0 = unlimited
		if (anonymousArgs.size() > maxAnonymousArgs) {
			errors << "Error: Too many anonymous options, " << anonymousArgs.size() << " provided (expects "
					  << minAnonymousArgs << " to " << maxAnonymousArgs << ")" << std::endl;
		}
	}
	if (anonymousArgs.size() < minAnonymousArgs) {
		errors << "Error: Not enough anonymous options provided, expected at least " << minAnonymousArgs << " but only " << anonymousArgs.size() << " provided" << std::endl;
	}

	// map anonymous arguments
	int ai = 0;
	for (std::vector<AnonymousOption*>::iterator fit = anonymousArgsOrder.begin(); fit != anonymousArgsOrder.end(); ++fit) {
		AnonymousOption* aopt = *fit;
		if (aopt->minOccurs == aopt->maxOccurs) {
			aopt->mapped = true;

			// copy in values
			for (int c = 0; c < aopt->minOccurs && ai < anonymousArgs.size(); ++c, ++ai) {
				std::string val = anonymousArgs[ai];
				if (aopt->streamLink != NULL) {
					aopt->streamLink->values.push_back(val);
				} else if (aopt->optionLink != NULL) {
					aopt->optionLink->values.push_back(val);
				} else
					aopt->values.push_back(val);
			}
		} else
			break; // stop because we found the unlimited one
	}
	int ari = anonymousArgs.size() - 1;
	for (std::vector<AnonymousOption*>::reverse_iterator rit = anonymousArgsOrder.rbegin(); rit != anonymousArgsOrder.rend(); ++rit) {
		AnonymousOption* aopt = *rit;
		if (aopt->mapped)
			break;

		// copy values
		for (int i = aopt->maxOccurs; i > 0 && ari >= ai; --i, --ari) {
			std::string val = anonymousArgs[ari];
			if (aopt->streamLink != NULL) {
				aopt->streamLink->values.insert(aopt->streamLink->values.begin(),val);
			} else if (aopt->optionLink != NULL) {
				aopt->optionLink->values.insert(aopt->optionLink->values.begin(),val);
			} else
				aopt->values.insert(aopt->values.begin(),val);
		}
	}

	/// check flag/option/stream occurrences ///
	// flags
	for (std::map<std::string, Flag*>::iterator fit = flagsByName.begin(); fit != flagsByName.end(); ++fit) {
		if (fit->second->occurrences < fit->second->minOccurs) {
			if (fit->second->longflag != NULL)
				errors << "Error: too few occurrences of flag --" << *fit->second->longflag << " (min: " << fit->second->minOccurs << ")" << std::endl;
			else if (fit->second->flag != '\0')
				errors << "Error: too few occurrences of flag -" << fit->second->flag << " (min: " << fit->second->minOccurs << ")" << std::endl;
		}
		if (fit->second->occurrences > fit->second->maxOccurs && fit->second->maxOccurs < CONFIGURATOR_UNLIMITED) {
			if (fit->second->longflag != NULL)
				errors << "Error: too many occurrences of flag --" << *fit->second->longflag << " (max: " << fit->second->maxOccurs << ")" << std::endl;
			else if (fit->second->flag != '\0')
				errors << "Error: too many occurrences of flag -" << fit->second->flag << " (max: " << fit->second->maxOccurs << ")" << std::endl;
		}
	}
	// options
	for (std::map<std::string, Option*>::iterator oit = optionsByName.begin(); oit != optionsByName.end(); ++oit) {
		if (oit->second->values.size() < oit->second->minOccurs) {
			if (oit->second->longoption != NULL)
				errors << "Error: too few occurrences of option --" << *oit->second->longoption << " (min: " << oit->second->minOccurs << ")" << std::endl;
			else if (oit->second->option != '\0')
				errors << "Error: too few occurrences of option -" << oit->second->option << " (min: " << oit->second->minOccurs << ")" << std::endl;
		}
		if (oit->second->values.size() > oit->second->maxOccurs && oit->second->maxOccurs < CONFIGURATOR_UNLIMITED) {
			if (oit->second->longoption != NULL)
				errors << "Error: too many occurrences of option --" << *oit->second->longoption << " (max: " << oit->second->maxOccurs << ")" << std::endl;
			else if (oit->second->option != '\0')
				errors << "Error: too many occurrences of option -" << oit->second->option << " (max: " << oit->second->maxOccurs << ")" << std::endl;
		}
	}
	// streams
	//TODO: streams needs to support multiple values first

	return errors.str().size() == 0;
}

/**
 * Prints option parsing, assignment and validation errors.
 */
void Configurator::printErrors(OutputBase *log) {

	if (log == NULL)
		log = getDefaultLogStream();
	if (log != NULL && log->open())
		*log << errors.str();
	else
		std::cerr << errors.str();
}

/**
 * Checks if user asked for help.
 */
bool Configurator::isHelp() {
	return isFlagSet("help");
}

/**
 * Attempts to load and connect all streams.  It starts with the default
 * log stream and then all others.  If a stream fails then a suitable
 * message is placed on default log stream (or stderr if log fails).
 *
 * @param types, the types of streams to open (default IN|OUT|LOG)
 * @return 0= success, 1= failed to open stream
 */
int Configurator::connectStreams(int types) {

	// connect default log stream
	OutputBase *logs = this->getDefaultLogStream();
	if (logs == NULL || !logs->open()) {
		if (defaultLog != NULL && defaultLog->values.size() > 0)
			std::cerr << "Error: failed to open log stream (" << *(defaultLog->values.rbegin()) << ")" << std::endl;
		else
			std::cerr << "Error: failed to open log stream" << std::endl;
		return 1;
	}

	// open other streams
	InputBase *is = NULL;
	OutputBase *os = NULL;
	std::vector<InputBase*> iss;
	std::vector<OutputBase*> oss;
	for (std::map<std::string, Stream*>::iterator it = streamsByName.begin(); it != streamsByName.end(); ++it) {
		if (it->second != defaultLog && (types & it->second->streamType) > 0) {
			switch (it->second->streamType) {
			case IN:
				iss = getInStreams(it->second->name);
				for (std::vector<InputBase*>::iterator iit=iss.begin(); iit != iss.end(); ++iit) {
					is = *iit;
					if (is == NULL || !is->open()) {
						if (it->second->values.size() > 0 && is != NULL)
							*logs << "Error: failed to open input stream (" << *(it->second->values.rbegin()) << ": " << is->getFilename() << ")" << std::endl;
						else
							*logs << "Error: failed to open input stream" << std::endl;
						return 1;
					}
				}
				break;
			case OUT:
				oss = getOutStreams(it->second->name);
				for (std::vector<OutputBase*>::iterator oit=oss.begin(); oit != oss.end(); ++oit) {
					os = *oit;
					if (os == NULL || !os->open()) {
						if (it->second->values.size() > 0 && os != NULL)
							*logs << "Error: failed to open output stream (" << *(it->second->values.rbegin()) << ": " << os->getFilename() << ")" << std::endl;
						else
							*logs << "Error: failed to open output stream" << std::endl;
						return 1;
					}
				}
				break;
			case LOG:
				oss = getOutStreams(it->second->name);
				for (std::vector<OutputBase*>::iterator lit=oss.begin(); lit != oss.end(); ++lit) {
					os = *lit;
					if (os == NULL || !os->open()) {
						if (it->second->values.size() > 0 && os != NULL)
							*logs << "Error: failed to open log stream (" << *(it->second->values.rbegin()) << ": " << os->getFilename() << ")" << std::endl;
						else
							*logs << "Error: failed to open log stream" << std::endl;
						return 1;
					}
				}
				break;
			}
		}
	}

	return 0;
}

/**
 * Prints a short usage summary
 *
 * @param fullHelpMsg, causes an extra "see --help for details" to by printed as well
 */
void Configurator::printUsageSummary(OutputBase* log, bool fullHelpMsg) {

	if (log == NULL)
		log = getDefaultLogStream();

	bool dellog = false;
	if (log == NULL || !log->open()) {
		log = new PlainOutput("=");
		log->open();
		dellog = true;
	}

	std::stringstream linebuffer;
	std::stringstream argbuffer;
#define LINELEN 70
#define INDENT "          "

	linebuffer << std::endl << " Usage: " << cmdname;

	// print flags
	for (std::map<std::string, Flag*>::iterator fit = flagsByName.begin(); fit != flagsByName.end(); ++fit) {
		if (fit->second->minOccurs == 0)
			argbuffer << "[";
		if (fit->second->flag != '\0')
			argbuffer << "-" << fit->second->flag;
		if (fit->second->flag != '\0' && fit->second->longflag != NULL)
			argbuffer << " | ";
		if (fit->second->longflag != NULL)
			argbuffer << "--" << *fit->second->longflag;
		if (fit->second->minOccurs == 0)
			argbuffer << "]";

		// flush buffer
		if (linebuffer.str().size() + argbuffer.str().size() > LINELEN) {
			*log << INDENT << linebuffer.str() << std::endl;
			linebuffer.str("");
			linebuffer.clear();
		}
		else
			linebuffer << " ";
		linebuffer << argbuffer.str();
		argbuffer.str("");
		argbuffer.clear();
	}

	// print options
	for (std::map<std::string, Option*>::iterator oit = optionsByName.begin(); oit != optionsByName.end(); ++oit) {
		if (oit->second->minOccurs == 0)
			argbuffer << "[";
		if (oit->second->option != '\0')
			argbuffer << "-" << oit->second->option << " " << "value";
		if (oit->second->option != '\0' && oit->second->longoption != NULL)
			argbuffer << " | ";
		if (oit->second->longoption != NULL)
			argbuffer << "--" << *oit->second->longoption << "=" << "value";
		if (oit->second->maxOccurs > 1)
			argbuffer << " [...]";
		if (oit->second->minOccurs == 0)
			argbuffer << "]";

		// flush buffer
		if (linebuffer.str().size() + argbuffer.str().size() > LINELEN) {
			*log << INDENT << linebuffer.str() << std::endl;
			linebuffer.str("");
			linebuffer.clear();
		}
		else
			linebuffer << " ";
		linebuffer << argbuffer.str();
		argbuffer.str("");
		argbuffer.clear();
	}

	// print streams
	for (std::map<std::string, Stream*>::iterator sit = streamsByName.begin(); sit != streamsByName.end(); ++sit) {
		if (sit->second->option != '\0' || sit->second->longoption != NULL) {
			if (sit->second->minOccurs == 0)
				argbuffer << "[";
			if (sit->second->option != '\0') {
				argbuffer << "-" << sit->second->option << " ";
				if (sit->second->streamType == IN)
					argbuffer << "infile";
				else if (sit->second->streamType == OUT)
					argbuffer << "outfile";
				else
					argbuffer << "logfile";
			}
			if (sit->second->option != '\0' && sit->second->longoption != NULL)
				argbuffer << " | ";
			if (sit->second->longoption != NULL) {
				argbuffer << "--" << *sit->second->longoption;
				if (sit->second->streamType == IN)
					argbuffer << "=infile";
				else if (sit->second->streamType == OUT)
					argbuffer << "=outfile";
				else
					argbuffer << "=logfile";
			}
			if (sit->second->maxOccurs > 1)
				argbuffer << " [...]";
			if (sit->second->minOccurs == 0)
				argbuffer << "]";

			// flush buffer
			if (linebuffer.str().size() + argbuffer.str().size() > LINELEN) {
				*log << INDENT << linebuffer.str() << std::endl;
				linebuffer.str("");
				linebuffer.clear();
			}
			else
				linebuffer << " ";
			linebuffer << argbuffer.str();
			argbuffer.str("");
			argbuffer.clear();
		}

		//compress option
		if (sit->second->compressOptionLink != NULL) {
			argbuffer << "[";
			if (sit->second->compressOptionLink->longoption != NULL) {
				if (sit->second->streamType == IN)
					argbuffer << "--" << *sit->second->compressOptionLink->longoption << "=" << "AUTO|PLAIN";
				else
					argbuffer << "--" << *sit->second->compressOptionLink->longoption << "=" << "PLAIN";
#ifdef LBS_ZLIB_FOUND
				argbuffer << ("|GZ");
#endif
#ifdef LBS_BZLIB_FOUND
				argbuffer << ("|BZ2");
#endif
#ifdef LBS_LZMA_FOUND
				argbuffer << ("|XZ");
#endif
			}
			argbuffer << "]";

			// flush buffer
			if (linebuffer.str().size() + argbuffer.str().size() > LINELEN) {
				*log << INDENT << linebuffer.str() << std::endl;
				linebuffer.str("");
				linebuffer.clear();
			}
			else
				linebuffer << " ";
			linebuffer << argbuffer.str();
			argbuffer.str("");
			argbuffer.clear();
		}
	}

	// print anonymous options
	for (std::vector<AnonymousOption*>::iterator ait = anonymousArgsOrder.begin(); ait != anonymousArgsOrder.end(); ++ait) {
		std::string name = (*ait)->name;
//		unsigned short ni = 1;

		// print required values
		if ((*ait)->minOccurs > 0) {
			if ((*ait)->maxOccurs == 1)
				argbuffer << name;
			else
				argbuffer << name << 1;
			if ((*ait)->minOccurs >= 3)
				argbuffer << " ...";
			if ((*ait)->minOccurs >= 2)
				argbuffer << " " << name << (*ait)->minOccurs;
		}

		// print optional values
		if ((*ait)->maxOccurs > (*ait)->minOccurs) {
			argbuffer << " [" << name << ((*ait)->minOccurs + 1);

			if ((*ait)->maxOccurs < CONFIGURATOR_UNLIMITED)
				argbuffer << " [... " << name << (*ait)->maxOccurs << "]";
			else
				argbuffer << " ...";
			argbuffer << "]";
		}

		// flush buffer
		if (linebuffer.str().size() + argbuffer.str().size() > LINELEN) {
			*log << INDENT << linebuffer.str() << std::endl;
			linebuffer.str("");
			linebuffer.clear();
		}
		else
			linebuffer << "  ";

		linebuffer << argbuffer.str();
		argbuffer.str("");
		argbuffer.clear();
	}

	*log << INDENT << linebuffer.str() << std::endl;

	// print extra help
	if (fullHelpMsg)
		*log << std::endl << "  Use --help option for full help information" << std::endl;

	*log << std::endl;

	if (dellog)
		delete log;
}

/**
 * Prints the help message to the screen
 */
void Configurator::printHelp(OutputBase* log) {

	if (log == NULL)
		log = getDefaultLogStream();

	bool dellog = false;
	if (log == NULL || !log->open()) {
		log = new PlainOutput("=");
		log->open();
		dellog = true;
	}

	// how much to indent the doc strings
#define DOCSTRINGINDENT 20

	std::string indent = "\n";
	for (int i = 0; i <= DOCSTRINGINDENT; ++i)
		indent += " ";

	printUsageSummary(log, false);

	if (this->preamble.length() > 0) {

		// indent
		size_t start_pos = 0;
		std::string temp = this->preamble;
		while ((start_pos = temp.find("\n", start_pos)) != std::string::npos) {
			temp.replace(start_pos, 1, "\n  ");
			start_pos += 3;
		}

		// print
		*log << "  " << temp << std::endl << std::endl;
	}

	*log << " Streams:" << std::endl;
	for (std::map<std::string, Stream*>::iterator it = streamsByName.begin(); it != streamsByName.end(); ++it) {
		bool both = false;
		int optionslen = 4;

		// print short flag
		if (it->second->option != '\0') {
			*log << "  -" << it->second->option;
			both = true;
			optionslen += 4;
		} else if (it->second->longoption != NULL) {
			*log << "    ";
			optionslen += 4;
		} else {
			*log << "  ";
			optionslen += 2;
		}

		// print long flag
		if (it->second->longoption != NULL) {
			if (both)
				*log << ", ";
			else
				*log << "  ";
			*log << "--" << *it->second->longoption;
			optionslen += it->second->longoption->length();
		}

		// print anonymous option
		if (it->second->anonymousOptionLink != NULL) {
			std::string name = it->second->anonymousOptionLink->name;
			std::stringstream s;

			if (it->second->option != '\0' || it->second->longoption != NULL) {
				s << ", ";
				optionslen += 2;
			}

			// print required values
			if (it->second->anonymousOptionLink->minOccurs > 0) {
				if (it->second->anonymousOptionLink->maxOccurs == 1)
					s << name;
				else
					s << name << 1;
				if (it->second->anonymousOptionLink->minOccurs >= 3)
					s << " ...";
				if (it->second->anonymousOptionLink->minOccurs >= 2)
					s << " " << name << it->second->anonymousOptionLink->minOccurs;
			}

			// print optional values
			if (it->second->anonymousOptionLink->maxOccurs > it->second->anonymousOptionLink->minOccurs) {
				s << " [" << name << (it->second->anonymousOptionLink->minOccurs + 1);

				if (it->second->anonymousOptionLink->maxOccurs < CONFIGURATOR_UNLIMITED)
					s << " [... " << name << it->second->anonymousOptionLink->maxOccurs << "]";
				else
					s << " ...";
				s << "]";
			}

			*log << s.str();
			optionslen += s.str().size();
		}

		// wrap if needed
		bool wrapped = false;
		if (optionslen > DOCSTRINGINDENT) {
			*log << std::endl;
			optionslen = 0;
			wrapped = true;
		}

		// Pad to required indentation
		for (int i = optionslen; i < DOCSTRINGINDENT; ++i)
			*log << " ";

		// indent newlines within doc string
		size_t start_pos = 0;
		std::string temp = it->second->docstring;
		while ((start_pos = temp.find("\n", start_pos)) != std::string::npos) {
			temp.replace(start_pos, 1, indent);
			start_pos += DOCSTRINGINDENT + 1;
			wrapped = true;
		}

		// print doc string
		switch (it->second->streamType) {
		case IN:
			*log << " [IN]  " << temp << std::endl;
			break;
		case OUT:
			*log << " [OUT] " << temp << std::endl;
			break;
		case LOG:
			*log << " [LOG] " << temp << std::endl;
			break;
		default:
			*log << " [UNK] " << temp << std::endl;
			break;
		}

		// print compress option
		if (it->second->compressOptionLink != NULL)
			printOption(log, it->second->compressOptionLink, indent);
	}

	*log << std::endl << " Options:" << std::endl;
	for (std::map<std::string, Option*>::iterator it = optionsByName.begin(); it != optionsByName.end(); ++it) {
		printOption(log, it->second, indent);
	}

	// and anonymous options
	for (std::vector<AnonymousOption*>::iterator ait = anonymousArgsOrder.begin(); ait != anonymousArgsOrder.end(); ++ait) {

		if ((*ait)->streamLink == NULL && (*ait)->optionLink == NULL) {
			std::string name = (*ait)->name;
			int optionslen = 2 + name.size();

			*log << "  " << name;

			// wrap if needed
			bool wrapped = false;
			if (optionslen > DOCSTRINGINDENT) {
				*log << std::endl;
				optionslen = 0;
				wrapped = true;
			}

			// Pad to required indentation
			for (int i = optionslen; i < DOCSTRINGINDENT; ++i)
				*log << ' ';

			// indent newlines within doc string
			size_t start_pos = 0;
			std::string temp = (*ait)->docstring;
			while ((start_pos = temp.find("\n", start_pos)) != std::string::npos) {
				temp.replace(start_pos, 1, indent);
				start_pos += DOCSTRINGINDENT + 1;
				wrapped = true;
			}
			*log << ' ' << temp << std::endl;
			if (wrapped)
				*log << std::endl;
		}
	}

	*log << std::endl << " Flags:" << std::endl;
	for (std::map<std::string, Flag*>::iterator it = flagsByName.begin(); it != flagsByName.end(); ++it) {
		bool both = false;
		int flagslen = 4;

		// print short flag
		if (it->second->flag != '\0') {
			*log << "  -" << it->second->flag;
			both = true;
		} else
			*log << "    ";

		// print long flag
		if (it->second->longflag != NULL) {
			if (both)
				*log << ", ";
			else
				*log << "  ";
			*log << "--" << *it->second->longflag;
			flagslen += 4 + it->second->longflag->length();
		}

		// wrap if needed
		bool wrapped = false;
		if (flagslen > DOCSTRINGINDENT) {
			*log << std::endl;
			flagslen = 0;
			wrapped = true;
		}

		// Pad to required indentation
		for (int i = flagslen; i < DOCSTRINGINDENT; ++i)
			*log << ' ';

		// indent newlines within doc string
		size_t start_pos = 0;
		std::string temp = it->second->docstring;
		while ((start_pos = temp.find("\n", start_pos)) != std::string::npos) {
			temp.replace(start_pos, 1, indent);
			start_pos += DOCSTRINGINDENT + 1;
			wrapped = true;
		}
		*log << ' ' << temp << std::endl;
		if (wrapped)
			*log << std::endl;
	}

	*log << std::endl;

	*log << std::endl << " Build options:" << std::endl;

#ifdef LIB_BIO_STREAMS_VERSION
	*log << "  - LibBioStreams: Yes (v" << QUOTE(LIB_BIO_STREAMS_VERSION) << ")" << std::endl;
#else
	*log << "  - LibBioStreams: Yes (unknown version)" << std::endl;
#endif
#ifdef LBS_ZLIB_FOUND
	*log << "  - ZLib (.gz):    Yes (v" << QUOTE(LBS_ZLIB_VERSION_STRING) << ")" << std::endl;
#else
	*log << "  - ZLib (.gz):    No" << std::endl;
#endif
#ifdef LBS_BZLIB_FOUND
	*log << "  - BZLib2 (.bz2): Yes (v" << QUOTE(LBS_BZLIB_VERSION_STRING) << ")" << std::endl;
#else
	*log << "  - BZLib2 (.bz2): No" << std::endl;
#endif
#ifdef LBS_LZMA_FOUND
	*log << "  - LZMA (.xz):    Yes (v" << QUOTE(LBS_LZMA_VERSION_STRING) << ")" << std::endl;
#else
	*log << "  - LZMA (.xz):    No" << std::endl;
#endif
	*log << std::endl;

	// display sections
	for (std::vector<Section*>::iterator it = sections.begin(); it != sections.end(); ++it) {

		// print header
		*log << " " << (*it)->name << ":" << std::endl;

		// indent text
		size_t start_pos = 0;
		std::string temp = (*it)->doc;
		while ((start_pos = temp.find("\n", start_pos)) != std::string::npos) {
			temp.replace(start_pos, 1, "\n  ");
			start_pos += 3;
		}

		// print text
		*log << "  " << temp << std::endl << std::endl;
	}

	// display tool version
	*log << std::endl;
	if (major < 0) {
		*log << " Version: Unknown (not provided by tool developer)" << std::endl;
	} else {
		*log << " Version: " << major << "." << minor << "." << patch;
		if (versionDate.size() > 0)
			*log << " (" << versionDate << ")";
		else
			*log << " (Unknown date)";
		*log << std::endl;
	}
	*log << std::endl;

	if (this->postamble.length() > 0) {

		// indent
		size_t start_pos = 0;
		std::string temp = this->postamble;
		while ((start_pos = temp.find("\n", start_pos)) != std::string::npos) {
			temp.replace(start_pos, 1, "\n  ");
			start_pos += 3;
		}

		// print
		*log << " -----" << std::endl << "  " << temp << std::endl;
	}

	if (dellog)
		delete log;
}

/**
 * Prints an option description
 *
 * @param log, OutputBase* class to write too
 * @param opt, Option* class to describe
 * @param indent, a string containing the amount of spaces to indent wrapped lines with
 */
void Configurator::printOption(OutputBase* log, Option* opt, std::string &indent) {
	bool both = false;
	int optionslen = 4;

	// print short flag
	if (opt->option != '\0') {
		*log << "  -" << opt->option;
		both = true;
		optionslen += 4;
	} else if (opt->longoption != NULL) {
		*log << "    ";
		optionslen += 4;
	} else { // just anonymous
		*log << "  ";
		optionslen += 2;
	}

	// print long flag
	if (opt->longoption != NULL) {
		if (both)
			*log << ", ";
		else
			*log << "  ";
		*log << "--" << *opt->longoption;
		optionslen += opt->longoption->length();
	}

	// print anonymous option
	if (opt->anonymousOptionLink != NULL) {
		std::string name = opt->anonymousOptionLink->name;
		std::stringstream s;

		if (opt->option != '\0' || opt->longoption != NULL) {
			s << ", ";
			optionslen += 2;
		}

		// print required values
		if (opt->anonymousOptionLink->minOccurs > 0) {
			if (opt->anonymousOptionLink->maxOccurs == 1)
				s << name;
			else
				s << name << 1;
			if (opt->anonymousOptionLink->minOccurs >= 3)
				s << " ...";
			if (opt->anonymousOptionLink->minOccurs >= 2)
				s << " " << name << opt->anonymousOptionLink->minOccurs;
		}

		// print optional values
		if (opt->anonymousOptionLink->maxOccurs > opt->anonymousOptionLink->minOccurs) {
			s << " [" << name << (opt->anonymousOptionLink->minOccurs + 1);

			if (opt->anonymousOptionLink->maxOccurs < CONFIGURATOR_UNLIMITED)
				s << " [... " << name << opt->anonymousOptionLink->maxOccurs << "]";
			else
				s << " ...";
			s << "]";
		}

		*log << s.str();
		optionslen += s.str().size();
	}

	// wrap if needed
	bool wrapped = false;
	if (optionslen > DOCSTRINGINDENT) {
		*log << std::endl;
		optionslen = 0;
		wrapped = true;
	}

	// Pad to required indentation
	for (int i = optionslen; i < DOCSTRINGINDENT; ++i)
		*log << ' ';

	// indent newlines within doc string
	size_t start_pos = 0;
	std::string temp = opt->docstring;
	while ((start_pos = temp.find("\n", start_pos)) != std::string::npos) {
		temp.replace(start_pos, 1, indent);
		start_pos += DOCSTRINGINDENT + 1;
		wrapped = true;
	}
	*log << ' ' << temp << std::endl;
	if (wrapped)
		*log << std::endl;
}



/**
 * Prints the configuration used (including the options using default values)
 *
 * @param outs, the stream to write to
 */
void Configurator::printConfig(OutputBase *outs) {
	*outs << "Effective command:" << std::endl;

	// print command name
	*outs << "  " << cmdname;

	// print flags
	for (std::map<std::string, Flag*>::iterator it = flagsByName.begin(); it != flagsByName.end(); ++it) {
		if (it->second->occurrences > 0) {
			if (it->second->flag != '\0') {
				*outs << " -" << it->second->flag;
			} else if (it->second->longflag != NULL) {
				*outs << " --" << it->second->longflag;
			} else {
				*outs << " [ERROR: Unknown flag set]";
			}
		}
	}

	// print streams
	for (std::map<std::string, Stream*>::iterator it = streamsByName.begin(); it != streamsByName.end(); ++it) {
		for (std::vector<std::string>::iterator vit = it->second->values.begin(); vit != it->second->values.end(); ++it) {
			std::string value = *vit;
			if (value.compare("") == 0 && it->second->def != NULL)
				value = *it->second->def;

			if (it->second->option != '\0') {
				*outs << " -" << it->second->option << " " << value;
			} else if (it->second->longoption != NULL) {
				*outs << " --" << it->second->longoption << "=" << value;
			} else {
				*outs << " [ERROR: Unknown stream set]";
			}
		}
		// print the stream's compress option
		if (it->second->compressOptionLink != NULL) {
			if (it->second->compressOptionLink->values.size() == 0) {
				if (it->second->def != NULL)
					*outs << " --" << it->second->longoption << "=" << it->second->compressOptionLink->def;
				else
					*outs << " --" << it->second->longoption << "=";
			} else {
				for (std::vector<std::string>::iterator valit = it->second->compressOptionLink->values.begin(); valit != it->second->compressOptionLink->values.end(); ++valit) {
					std::string value = *(valit);
					if (it->second->compressOptionLink->longoption != NULL) {
						*outs << " --" << it->second->compressOptionLink->longoption << "=" << value;
					} else {
						*outs << " [ERROR: Unknown option set]";
					}
				}
			}
		}
	}

	// print options
	for (std::map<std::string, Option*>::iterator it = optionsByName.begin(); it != optionsByName.end(); ++it) {
		for (std::vector<std::string>::iterator valit = it->second->values.begin(); valit != it->second->values.end(); ++valit) {
			std::string value = *(valit);
			if (it->second->option != '\0') {
				*outs << " -" << it->second->option << " " << value;
			} else if (it->second->longoption != NULL) {
				*outs << " --" << it->second->longoption << "=" << value;
			} else {
				*outs << " [ERROR: Unknown option set]";
			}
		}
		if (it->second->values.size() == 0) {
			if (it->second->option != '\0') {
				if (it->second->def != NULL)
					*outs << " -" << it->second->option << " " << it->second->def;
			} else if (it->second->longoption != NULL) {
				if (it->second->def != NULL)
					*outs << " --" << it->second->longoption << "=" << it->second->def;
			} else {
				*outs << " [ERROR: Unknown stream set]";
			}
		}
	}

	// print anonymous options
	for (std::vector<std::string>::iterator it = anonymousArgs.begin(); it != anonymousArgs.end(); ++it) {
		*outs << " " << *(it);
	}
	*outs << std::endl;

	*outs << std::endl;
	if (major < 0) {
		*outs << "Version: Unknown (not provided by tool developer)" << std::endl;
	} else {
		*outs << "Version: " << major << "." << minor << "." << patch;
		if (versionDate.size() > 0)
			*outs << " (" << versionDate << ")";
		else
			*outs << " (Unknown date)";
		*outs << std::endl;
	}
}

/**
 * Returns true if flag was set
 *
 * @param name, the name flag of interest
 * @return bool, true if flag was set
 */
bool Configurator::isFlagSet(const std::string &name) {

	std::map<std::string, Flag*>::iterator it = flagsByName.find(name);
	if (it == flagsByName.end()) {
		return false;
	}

	return it->second->occurrences > 0;
}

/**
 * Returns the number of occurrences of flag
 *
 * @param name, the name flag of interest
 * @return int, number of occurrences, -1 on flag not defined
 */
int Configurator::getFlagCount(const std::string &name) {
	std::map<std::string, Flag*>::iterator it = flagsByName.find(name);
	if (it == flagsByName.end())
		return -1;

	return it->second->occurrences;
}

/**
 * Returns true if flag was set
 *
 * @param flag, the flag of interest
 * @return bool, true if flag was set
 * @deprecated use the name version
 */
bool Configurator::isFlagSet(char flag) {

	std::map<char, Flag*>::iterator it = flagsByFlag.find(flag);
	if (it == flagsByFlag.end()) {
		return false;
	}

	return it->second->occurrences > 0;
}

/**
 * Returns the number of occurrences of flag
 *
 * @param flag, the flag of interest
 * @return int, number of occurrences, -1 on flag not defined
 * @deprecated use the name version
 */
int Configurator::getFlagCount(char flag) {
	std::map<char, Flag*>::iterator it = flagsByFlag.find(flag);
	if (it == flagsByFlag.end())
		return -1;

	return it->second->occurrences;
}

/**
 * Get the named option's value as an int
 */
int Configurator::getOptionInt(const std::string &name) {
	std::string value = getOptionString(name);
	return atoi(value.c_str());
}

/**
 * Get the named option's value as a string.  If the user provided multiple
 * values then it will return the last such value.
 */
std::string Configurator::getOptionString(const std::string &name) {

	std::map<std::string, Option*>::iterator it = this->optionsByName.find(name);

	if (it == optionsByName.end())
		return "";

	if (it->second->values.size() > 0) {
		std::string last = it->second->values.back();
		it->second->values.pop_back();
		return last;
	}

	if (it->second->def != NULL)
		return *(it->second->def);

	return "";
}

/**
 * Get the named option's values as a vector of strings.
 */
const std::vector<std::string> Configurator::getOptionStrings(const std::string &name) {

	std::map<std::string, Option*>::iterator it = this->optionsByName.find(name);

	if (it == optionsByName.end())
		return emptyvalues;

	if (it->second->values.size() > 0) {
		return it->second->values;
	}

	if (it->second->def != NULL)
		return it->second->deflist;

	return emptyvalues;
}

/**
 * Get the named anonymous option's value as a string
 */
std::string Configurator::getAnonymousOptionString(const std::string &name) {

	std::map<std::string, AnonymousOption*>::iterator it = anonymousArgsByName.find(name);

	if (it == anonymousArgsByName.end())
		return "";

	if (it->second->values.size() > 0) {
		std::string last = it->second->values.back();
		it->second->values.pop_back();
		return last;
	}

	if (it->second->def != NULL)
		return *(it->second->def);

	return "";
}

/**
 * Get the all named anonymous option's values as a vector of strings
 */
const std::vector<std::string> &Configurator::getAnonymousOptionStrings(const std::string &name) {

	std::map<std::string, AnonymousOption*>::iterator it = anonymousArgsByName.find(name);

	if (it == anonymousArgsByName.end())
		return emptyvalues;

	if (it->second->values.size() > 0) {
		return it->second->values;
	}

	if (it->second->def != NULL) {
		if (it->second->defaultvalues.size() == 0)
			it->second->defaultvalues.push_back(*it->second->def);
		return it->second->defaultvalues;
	}

	return emptyvalues;
}


/**
 * Get an input stream (IO class)
 *
 * @param name, the name of the instream to retrieve
 * @return InputBase*, a pointer to the (borrowed) stream object (or NULL on error)
 */
InputBase *Configurator::getInStream(const std::string &name) {

	InputBase *ins = NULL;

	// get stream filename
	std::map<std::string, Stream*>::iterator it = this->streamsByName.find(name);
	if (it == streamsByName.end()) {
		std::cerr << "Failed to locate in-stream '" << name << "'" << std::endl;
		return NULL;
	}
	if (it->second->streamType != IN) {
		std::cerr << "Error: Stream '" << name << "' is not an In Stream" << std::endl;
		return NULL;
	}
	std::string filename = "";
	if (it->second->values.size() > 0)
		filename = *it->second->values.rbegin();
	else if (it->second->def != NULL) {
		filename = *it->second->def;
	}

	ins = getInStreamObject(filename, it->second);

	return ins;
}

/**
 * Loads an InputBase object representing the filename (reuses value is already used).
 */
InputBase* Configurator::getInStreamObject(std::string filename, Stream* stream) {
	// magic numbers
	// 0x1f 0x8b						ZLIB (.gz)
	// 0x42 0x5a 0x68 ('BZh')			BZIP2 (.bz2)
	// 0xfd 0x37 0x7a 0x58 0x5a 0x00	LZMA (.xz)

	// the number of bytes to read in-order to inspect stream filetype (i.e. gzip, bzip2 etc.)
#define INSPECT_SIZE 6

	InputBase *ins = NULL;

	// consult cache
	if (cacheInStream || filename == "-") {
		std::map<std::string, InputBase*>::iterator isit = inStreamObjs.find(filename);
		if (isit != inStreamObjs.end())
			return isit->second;
	}

	// Check type is acceptable
	std::string compression = "";
	if (stream->compressOptionLink->values.size() > 0)
		compression = *stream->compressOptionLink->values.begin();
	if (compression.size() == 0) {
		if (stream->compressOptionLink->def != NULL)
			compression = *stream->compressOptionLink->def; //TODO: should constrain the default value to supported ones (fallback plain)
		else
			compression = "PLAIN";
	} else if ((compression != "AUTO"
			&& compression != "PLAIN"
#ifdef LBS_ZLIB_FOUND
			&& compression != "GZ"
#endif
#ifdef LBS_BZLIB_FOUND
			&& compression != "BZ2"
#endif
#ifdef LBS_LZMA_FOUND
			&& compression != "XZ"
#endif
			)) {
		std::cerr << "Error: Unsupported compression algorithm (" << compression << ")" << std::endl;
		compression = "ERROR";
	}

	// create object
	if (compression == "PLAIN")
		ins = new PlainInput(filename, (const unsigned char*) "", 0);
#ifdef LBS_ZLIB_FOUND
	else if (compression == "GZ")
		ins = new ZLibInput(filename, (const unsigned char*) "", 0);
#endif
#ifdef LBS_BZLIB_FOUND
	else if (compression == "BZ2")
		ins = new BZLibInput(filename, (const unsigned char*) "", 0);
#endif
#ifdef LBS_LZMA_FOUND
	else if (compression == "XZ")
		ins = new LZMAInput(filename, (const unsigned char*) "", 0);
#endif
	else if (compression == "AUTO") { // by inspection

		unsigned char inspectBuffer[INSPECT_SIZE+1];
		inspectBuffer[0] = '\0';
		inspectBuffer[INSPECT_SIZE] = '\0';

		// check for stdin special case
		FILE *in;
		if (filename.compare("-") == 0) {
			in = stdin;
		} else { // regular file (or named pipe)
			in = fopen(filename.c_str(), "rb");
			if (in == NULL) {
				return new OpenErrorInput(filename, (const unsigned char*) "", 0);
			}
		}

		// check for compression header
		int bytesRead = fread(inspectBuffer, 1, INSPECT_SIZE, in);
		if (bytesRead == 0) {
//			std::cerr << "Plain file format (empty)" << std::endl;
			return new EmptyInput(filename, (const unsigned char*) "", 0);
		}
		if (bytesRead >=2 && inspectBuffer[0] == 0x1f && inspectBuffer[1] == 0x8b) { // looks like it might be gzip (but could be plain)
#ifdef LBS_ZLIB_FOUND
			ins = new ZLibInput(filename, inspectBuffer, bytesRead);
#else
			std::cerr << "Error: ZLib (.gz) support not built into this application" << std::endl;
			ins = new UnsupportedInput(filename, (const unsigned char*) "", 0);
#endif
		} else if (bytesRead >=3 && inspectBuffer[0] == 0x42 && inspectBuffer[1] == 0x5a && inspectBuffer[2] == 0x68) { // looks like it might be bzip2 (but could be plain)
#ifdef LBS_BZLIB_FOUND
			ins = new BZLibInput(filename, inspectBuffer, bytesRead);
#else
			std::cerr << "Error: BZip2 (.bz2) support not built into this application" << std::endl;
			ins = new UnsupportedInput(filename, (const unsigned char*) "", 0);

#endif
		} else if (bytesRead >=6 && inspectBuffer[0] == 0xfd && inspectBuffer[1] == 0x37 && inspectBuffer[2] == 0x7a
				 && inspectBuffer[3] == 0x58 && inspectBuffer[4] == 0x5a && inspectBuffer[5] == 0x00) { // looks like it might be LZMA
#ifdef LBS_LZMA_FOUND
			ins = new LZMAInput(filename, inspectBuffer, bytesRead);
#else
			std::cerr << "Error: LZMA (.xz) support not built into this application" << std::endl;
			ins = new UnsupportedInput(filename, (const unsigned char*) "", 0);
#endif
		} else { // not compressed
			ins = new PlainInput(filename, inspectBuffer, bytesRead);
		}
	} else { // ERROR
		ins = new UnsupportedInput(filename, (const unsigned char*) "", 0);
	}

	// populate cache
	if (ins != NULL) {
		if (cacheInStream || filename == "-")
			inStreamObjs[filename] = ins;
		else
			inStreamObjsList.push_back(ins);
	}

	return ins;
}

/**
 * Get an output stream (IO class)
 *
 * @param name, the name of the outstream to retrieve
 * @return OutputBase*, a pointer to the (borrowed) stream object (or NULL on error)
 */
OutputBase *Configurator::getOutStream(const std::string &name) {

	OutputBase *out = NULL;

	// get stream filename
	std::map<std::string, Stream*>::iterator it = this->streamsByName.find(name);
	if (it == streamsByName.end()) {
		std::cerr << "Failed to locate out-stream '" << name << "'" << std::endl;
		return NULL;
	}
	if (it->second->streamType != OUT) {
		std::cerr << "Error: Stream '" << name << "' is not a Out Stream" << std::endl;
		return NULL;
	}
	std::string filename = "";
	if (it->second->values.size() > 0)
		filename = *it->second->values.rbegin();
	else if (it->second->def != NULL) {
		filename = *it->second->def;
	}

	out = getOutStreamObject(filename, it->second);

	return out;
}

/**
 * Loads an InputBase object representing the filename (reuses value is already used).
 */
OutputBase* Configurator::getOutStreamObject(std::string filename, Stream *stream) {

	OutputBase *out = NULL;

	// consult cache
	std::map<std::string, OutputBase*>::iterator osit = this->outStreamObjs.find(filename);
	if (osit != outStreamObjs.end())
		return osit->second;

	// Check type is acceptable
	std::string compression = "";
	if (stream->compressOptionLink->values.size() > 0)
		compression = *stream->compressOptionLink->values.begin();
	if (compression.size() == 0) {
		if (stream->compressOptionLink->def != NULL)
			compression = *stream->compressOptionLink->def; //TODO: should constrain the default value to supported ones (fallback plain)
		else
			compression = "PLAIN";
	} else if (compression != "PLAIN"
#ifdef LBS_ZLIB_FOUND
			&& compression != "GZ"
#endif
#ifdef LBS_BZLIB_FOUND
			&& compression != "BZ2"
#endif
#ifdef LBS_LZMA_FOUND
			&& compression != "XZ"
#endif
			) {
		std::cerr << "Error: Unsupported compression algorithm (" << compression << ")" << std::endl;
		compression = "ERROR";
	}

	// make the stream object
	if (compression == "PLAIN")
		out = new PlainOutput(filename);
#ifdef LBS_ZLIB_FOUND
	else if (compression == "GZ")
		out = new ZLibOutput(filename);
#endif
#ifdef LBS_BZLIB_FOUND
	else if (compression == "BZ2")
		out = new BZLibOutput(filename);
#endif
#ifdef LBS_LZMA_FOUND
	else if (compression == "XZ")
		out = new LZMAOutput(filename);
#endif

	if (out != NULL)
		outStreamObjs[filename] = out;

	return out;
}

/**
 * Get an log stream (IO class)
 *
 * @param name, the name of the logstream to retrieve
 * @return OutputBase*, a pointer to the (borrowed) stream object (or NULL on error)
 */
OutputBase *Configurator::getLogStream(const std::string &name) {

	OutputBase *out = NULL;

	// get stream filename
	std::map<std::string, Stream*>::iterator it = this->streamsByName.find(name);
	if (it == streamsByName.end()) {
		std::cerr << "Failed to locate log-stream '" << name << "'" << std::endl;
		return NULL;
	}
	if (it->second->streamType != LOG) {
		std::cerr << "Error: Stream '" << name << "' is not a Log Stream" << std::endl;
		return NULL;
	}
	std::string filename = "";
	if (it->second->values.size() > 0)
		filename = *it->second->values.rbegin();
	else if (it->second->def != NULL) {
		filename = *it->second->def;
	}

	out = getOutStreamObject(filename, it->second);

	return out;
}

/**
 * Gets the default (first defined) input stream.
 *
 * @return, the input stream, NULL if none defined or error
 */
InputBase *Configurator::getDefaultInStream() {
	InputBase *result = NULL;
	if (defaultInput != NULL)
		result = getInStream(defaultInput->name);
	return result;
}

/**
 * Gets the default (first defined) output stream.
 *
 * @return, the output stream, NULL if none defined or error
 */
OutputBase *Configurator::getDefaultOutStream() {
	OutputBase *result = NULL;
	if (defaultOutput != NULL)
		result = getOutStream(defaultOutput->name);
	return result;
}

/**
 * Gets the default (first defined) log stream.
 *
 * @return, the log stream, NULL if none defined or error
 */
OutputBase *Configurator::getDefaultLogStream() {
	OutputBase *result = NULL;
	if (defaultLog != NULL)
		result = getLogStream(defaultLog->name);
	return result;
}

/**
 * Get a list input streams (IO class).
 *
 * @param name, the name of the instream to retrieve
 * @return InputBase*, a pointer to the (borrowed) stream object (or NULL on error)
 */
std::vector<InputBase*> Configurator::getInStreams(const std::string &name) {
	std::vector<InputBase*> result;
	InputBase* obj = NULL;

	// get stream filename
	std::map<std::string, Stream*>::iterator it = this->streamsByName.find(name);
	if (it == streamsByName.end()) {
		std::cerr << "Failed to locate in-stream '" << name << "'" << std::endl;
		return result;
	}
	if (it->second->streamType != IN) {
		std::cerr << "Error: Stream '" << name << "' is not an In Stream" << std::endl;
		return result;
	}

	if (it->second->values.size() == 0) {
		if (it->second->def != NULL) {
			obj = getInStreamObject(*it->second->def, it->second);
			if (obj != NULL)
				result.push_back(obj);
		}
	} else {
		for (std::vector<std::string>::iterator vit=it->second->values.begin(); vit != it->second->values.end(); ++vit) {
			obj = getInStreamObject(*vit, it->second);
			if (obj != NULL)
				result.push_back(obj);
		}
	}

	return result;
}

/**
 * Get a list output streams (IO class)
 *
 * @param name, the name of the outstream to retrieve
 * @return OutputBase*, a pointer to the (borrowed) stream object (or NULL on error)
 */
std::vector<OutputBase*> Configurator::getOutStreams(const std::string &name) {
	std::vector<OutputBase*> result;
	OutputBase* obj = NULL;

	// get stream filename
	std::map<std::string, Stream*>::iterator it = this->streamsByName.find(name);
	if (it == streamsByName.end()) {
		std::cerr << "Failed to locate out-stream '" << name << "'" << std::endl;
		return result;
	}
	if (it->second->streamType != OUT) {
		std::cerr << "Error: Stream '" << name << "' is not a Out Stream" << std::endl;
		return result;
	}
	std::string filename = "";
	if (it->second->values.size() > 0)
		filename = *it->second->values.rbegin();
	else if (it->second->def != NULL) {
		filename = *it->second->def;
	}

	// get the default value as outstream
	if (it->second->values.size() == 0) {
		if (it->second->def != NULL) {
			obj = getOutStreamObject(*it->second->def, it->second);
			if (obj != NULL)
				result.push_back(obj);
		}
	} else { // get each provided value as outstream
		for (std::vector<std::string>::iterator vit=it->second->values.begin(); vit != it->second->values.end(); ++vit) {
			obj = getOutStreamObject(*vit, it->second);
			if (obj != NULL)
				result.push_back(obj);
		}
	}

	return result;
}

/**
 * Get a list log streams (IO class)
 *
 * @param name, the name of the logstream to retrieve
 * @return OutputBase*, a pointer to the (borrowed) stream object (or NULL on error)
 */
std::vector<OutputBase*> Configurator::getLogStreams(const std::string &name) {
	std::vector<OutputBase*> result;
	OutputBase* obj = NULL;

	// get stream filename
	std::map<std::string, Stream*>::iterator it = this->streamsByName.find(name);
	if (it == streamsByName.end()) {
		std::cerr << "Failed to locate log-stream '" << name << "'" << std::endl;
		return result;
	}
	if (it->second->streamType != LOG) {
		std::cerr << "Error: Stream '" << name << "' is not a Log Stream" << std::endl;
		return result;
	}
	std::string filename = "";
	if (it->second->values.size() > 0)
		filename = *it->second->values.rbegin();
	else if (it->second->def != NULL) {
		filename = *it->second->def;
	}

	// get the default value as outstream
	if (it->second->values.size() == 0) {
		if (it->second->def != NULL) {
			obj = getOutStreamObject(*it->second->def, it->second);
			if (obj != NULL)
				result.push_back(obj);
		}
	} else { // get each provided value as outstream
		for (std::vector<std::string>::iterator vit=it->second->values.begin(); vit != it->second->values.end(); ++vit) {
			obj = getOutStreamObject(*vit, it->second);
			if (obj != NULL)
				result.push_back(obj);
		}
	}

	return result;
}

/**
 * Set the 3 components of the version string for this tool.  Note: this
 * will be printed in the help text
 *
 * @param major short, the major version number
 * @param minor short, the minor version number
 * @param patch short, the patch version number
 * @param versionDate std::string, the date (YYYY-MM-DD format) when the version was created
 */
void Configurator::setVersionNumber(short major, short minor, short patch, std::string versionDate) {
	this->major = major;
	this->minor = minor;
	this->patch = patch;
	this->versionDate = versionDate;
}













