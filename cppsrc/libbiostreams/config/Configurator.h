/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * Configurator.h
 *
 *  Created on: 05/09/2014
 *      Author: arobinson
 */

#ifndef CONFIGURATOR_H_
#define CONFIGURATOR_H_

#include <cstdlib>
#include <map>
#include <set>
#include <sstream>
#include <vector>

#include <libbiostreams/io/InputBase.h>
#include <libbiostreams/io/OutputBase.h>

#define CONFIGURATOR_UNLIMITED 8191
#define CONFIGURATOR_COMPRESSION_FLAG "-compress"

class Flag;
class Option;
class Section;
class Stream;
class CompressStreamOption;
class AnonymousOption;
class Configurator;

class Flag {
public:
	Flag(const std::string name,
			char flag,
			std::string *longflag,
			unsigned short minoccurs=0,
			unsigned short maxoccurs=CONFIGURATOR_UNLIMITED,
			const std::string &docstring="") {

		this->name = name;
		this->flag = flag;
		this->longflag = longflag;
		this->minOccurs = minoccurs;
		this->maxOccurs = maxoccurs;
		this->docstring = docstring;

		this->occurrences = 0;
	}
	~Flag() {
		if (longflag != NULL)
			delete longflag;
		longflag = NULL;
	}

	std::string name;
	char flag;
	std::string *longflag;
	unsigned short minOccurs;
	unsigned short maxOccurs;
	std::string docstring;

	int occurrences;
};


class Option {
public:
	Option(const std::string name,
			char option,
			std::string *longoption,
			unsigned short minoccurs=0,
			unsigned short maxoccurs=1,
			std::string *def=NULL,
			const std::string &docstring="") {

		this->name = name;
		this->option = option;
		this->longoption = longoption;
		this->minOccurs = minoccurs;
		this->maxOccurs = maxoccurs;
		this->def = def;
		this->docstring = docstring;
		this->anonymousOptionLink = NULL;
		if (def != NULL)
			this->deflist.push_back(*def);
	}
	~Option() {
		if (longoption != NULL)
			delete longoption;
		longoption = NULL;
		if (def != NULL)
			delete def;
		def = NULL;
	}

	std::string name;
	char option;
	std::string *longoption;
	unsigned short minOccurs;
	unsigned short maxOccurs;
	std::string *def;
	std::vector<std::string> deflist;
	std::string docstring;
	AnonymousOption *anonymousOptionLink;

	std::vector<std::string> values;
};

class Section {
public:
	Section(const std::string name, const std::string doc) {
		this->name = name;
		this->doc = doc;
	}

	std::string name;
	std::string doc;
};

enum StreamType {
	IN=1,
	OUT=2,
	LOG=4
};


class Stream {
public:
	Stream(const std::string name,
			char option,
			std::string *longoption,
			unsigned short minoccurs=0,
			unsigned short maxoccurs=1,
			std::string *def=NULL,
			const std::string &docstring="",
			StreamType streamType=OUT) {

		this->name = name;
		this->option = option;
		this->longoption = longoption;
		this->minOccurs = minoccurs;
		this->maxOccurs = maxoccurs;
		this->def = def;
		this->docstring = docstring;
		this->streamType = streamType;
		this->compressOptionLink = NULL;
		this->anonymousOptionLink = NULL;

		this->inStream = NULL;
		this->outStream = NULL;
	}
	~Stream() {
		if (longoption != NULL)
			delete longoption;
		longoption = NULL;
		if (def != NULL)
			delete def;
		def = NULL;
	}

	std::string name;
	char option;
	std::string *longoption;
	unsigned short minOccurs;
	unsigned short maxOccurs;
	std::string *def;
	std::string docstring;
	StreamType streamType;
	CompressStreamOption *compressOptionLink;
	AnonymousOption *anonymousOptionLink;

	// cache stream objects
	InputBase* inStream;
	OutputBase* outStream;

//	std::string value;

	std::vector<std::string> values;
};

class CompressStreamOption : public Option {
public:
	CompressStreamOption(Stream *s) :
		Option(s->name+CONFIGURATOR_COMPRESSION_FLAG, '\0', s->longoption, s->minOccurs, s->maxOccurs, NULL, s->docstring)
	{
		this->s = s;

		// correct longoption and docstring
		if (s->longoption == NULL) {
			this->longoption = new std::string(name);
//			this->longoption->append(CONFIGURATOR_COMPRESSION_FLAG);
			if (s->option != '\0')
				this->docstring = "Set compression for '" + s->name + "' (-" + s->option + ").";
			else
				this->docstring = "Set compression for '" + s->name + "'.";
		} else {
			this->longoption = new std::string(*longoption);
			this->longoption->append(CONFIGURATOR_COMPRESSION_FLAG);
			if (s->option != '\0')
				this->docstring = "Set compression for '" + s->name + "' (-" + s->option + " OR --" + *s->longoption + ").";
			else
				this->docstring = "Set compression for '" + s->name + "' (--" + *s->longoption + ").";
		}

		if (s->streamType == IN) {
			this->def = new std::string("AUTO");
			this->docstring.append("\nAccepts: AUTO*, PLAIN");
		} else {
			this->def = new std::string("PLAIN");
			this->docstring.append("\nAccepts: PLAIN*");
		}
#ifdef LBS_ZLIB_FOUND
		this->docstring.append(", GZ");
#endif
#ifdef LBS_BZLIB_FOUND
		this->docstring.append(", BZ2");
#endif
#ifdef LBS_LZMA_FOUND
		this->docstring.append(", XZ");
#endif
	}

	Stream *s;
};

class AnonymousOption {
public:
	AnonymousOption(const std::string name,
			unsigned short minoccurs=0,
			unsigned short maxoccurs=1,
			std::string *def=NULL,
			std::string docstring="") {

		this->name = name;
		this->minOccurs = minoccurs;
		this->maxOccurs = maxoccurs;
		this->def = def;
		this->docstring = docstring;

		optionLink = NULL;
		streamLink = NULL;

		mapped = false;
	}
	~AnonymousOption() {

	}

	std::string name;
	unsigned short minOccurs;
	unsigned short maxOccurs;
	std::string *def;
	std::string docstring;

	Option *optionLink; ///< if provided, this is a synonym for this option
	Stream *streamLink; ///< if provided, this is a synonym for this stream
	std::vector<std::string> values;
	std::vector<std::string> defaultvalues; ///< don't alter, used for returning a default list

	bool mapped; ///< used to check if values were mapped to this argument during mapping.
};

/**
 * A class to manage argument parsing and creation of io streams for the application
 */
class Configurator {

protected:
	int argc;
	char ** argv;
	bool cacheInStream;

	std::map<std::string, Flag*> flagsByName;
	std::map<char, Flag*> flagsByFlag;
	std::map<std::string, Flag*> flagsByLongFlag;

	std::map<std::string, Option*> optionsByName;
	std::map<char, Option*> optionsByFlag;
	std::map<std::string, Option*> optionsByLongFlag;

	std::map<std::string, Stream*> streamsByName;
	std::map<char, Stream*> streamsByFlag;
	std::map<std::string, Stream*> streamsByLongFlag;

	// defaults
	Stream* defaultInput;
	Stream* defaultOutput;
	Stream* defaultLog;

	// sets for checking uniqueness
	std::set<std::string> names;
	std::set<char> flags;
	std::set<std::string> longflags;

	// stream object caches
	std::map<std::string, InputBase*> inStreamObjs;
	std::vector<InputBase*> inStreamObjsList;
	std::map<std::string, OutputBase*> outStreamObjs;

	// anonymous options
	std::vector<std::string> anonymousArgs;
	std::vector<AnonymousOption*> anonymousArgsOrder;
	std::map<std::string, AnonymousOption*> anonymousArgsByName;
	unsigned short minAnonymousArgs;
	unsigned short maxAnonymousArgs;
	bool variableAnonymousArgs;

	// help documentation
	std::string cmdname;
	std::string preamble;
	std::string postamble;
	std::vector<Section*> sections;
	short major;
	short minor;
	short patch;
	std::string versionDate;

	std::vector<std::string> emptyvalues; ///< don't alter, used for returning a default list

	std::stringstream errors; ///< parsing, assignment and validation error messages

	/**
	 * Loads an InputBase object representing the filename (reuses value is already used).
	 */
	InputBase* getInStreamObject(std::string filename, Stream* stream);

	/**
	 * Loads an OutputBase object representing the filename (reuses value is already used).
	 */
	OutputBase* getOutStreamObject(std::string filename, Stream *stream);

	/**
	 * Prints an option description
	 *
	 * @param log, OutputBase* class to write too
	 * @param opt, Option* class to describe
	 * @param indent, a string containing the amount of spaces to indent wrapped lines with
	 */
	void printOption(OutputBase* log, Option* opt, std::string &indent);

public:
	Configurator(int argc, char ** argv);
	virtual ~Configurator();

	/**
	 * Add commandline flag definition
	 *
	 * e.g. -h --help etc.
	 *
	 * @param name, a unique name for this option
	 * @param flag, a 1 letter flag, '\0' for no flag
	 * @param longflag, a multi-letter flag, NULL for no flag.
	 * @param minoccurs, the minimum number of times the user needs to provide this option
	 * @param maxoccurs, the maximum number of times the user can provide this option (0=unlimited)
	 * @param docstring, the explanation of flag displayed on help screen
	 * @return false on error (usually duplicate name, flag or longflag)
	 */
	bool addFlag(const std::string name, char flag, std::string *longflag, unsigned short minoccurs=0,
			unsigned short maxoccurs=1, std::string docstring="");

	/**
	 * Add commandline option definition
	 *
	 * e.g. -f <file> -f<file> --infile=<file>
	 *
	 * @param name, a unique name for this option
	 * @param flag, a 1 letter flag, '\0' for no flag
	 * @param longflag, a multi-letter flag, NULL for no flag.
	 * @param minoccurs, the minimum number of times the user needs to provide this option
	 * @param maxoccurs, the maximum number of times the user can provide this option (0=unlimited)
	 * @param def, the default value for the option if user doesn't provide one, NULL means no default.
	 * @param docstring, the explanation of flag displayed on help screen
	 * @return false on error (usually duplicate name, flag or longflag)
	 */
	bool addOption(const std::string name, char flag, std::string *longflag, unsigned short minoccurs=0,
			unsigned short maxoccurs=1, std::string *def=NULL, std::string docstring="");

	/**
	 * Adds an option that is not proceeded by a flag.  Add multiple groups of
	 * anonymous options in the order that user will need to type them.  You
	 * can only add one anonymous option with a variable amount of occurrences.
	 *
	 * NOTE: If name matches an option/stream name then it becomes an alternate
	 * method of providing value for that option.
	 *
	 * e.g.  <file> [<file> ...]
	 *
	 * @param name, a unique name for this option
	 * @param minoccurs, the minimum number of times the user needs to provide this option
	 * @param maxoccurs, the maximum number of times the user can provide this option (0=unlimited)
	 * @param def, the default value for the option if user doesn't provide one, NULL means no default.
	 * @param docstring, the explanation of flag displayed on help screen
	 */
	bool addAnonymousOption(const std::string name, unsigned short minoccurs=0,
			unsigned short maxoccurs=1, std::string *def=NULL, std::string docstring="");

	/**
	 * Add an input stream
	 *
	 * e.g. longflag="db" => --db=XXXX
	 *
	 * @param name, a unique name to identify this stream
	 * @param flag, the 1 letter flag used to specify input file/stdin, '\0' for no flag
	 * @param longflag, the multi-letter flag used to specify input file/stdin, NULL for no flag.
	 * @param minoccurs, the minimum number of times the user needs to provide this option
	 * @param maxoccurs, the maximum number of times the user can provide this option (0=unlimited)
	 * @param def, the default value for this stream. "-"=stdin
	 * @param docstring, the explanation of flag displayed on help screen
	 * @return false on error (usually duplicate name, flag or longflag)
	 */
	bool addInStream(const std::string name, char flag='i', std::string *longflag=NULL, unsigned short minoccurs=0,
			unsigned short maxoccurs=1,std::string *def=new std::string("-"), std::string docstring="");

	/**
	 * Add an output stream
	 *
	 * e.g. longflag="db" => --db=XXXX
	 *
	 * @param name, a unique name to identify this stream
	 * @param flag, the 1 letter flag used to specify input file/stdin, '\0' for no flag
	 * @param longflag, the multi-letter flag used to specify input file/stdin, NULL for no flag.
	 * @param minoccurs, the minimum number of times the user needs to provide this option
	 * @param maxoccurs, the maximum number of times the user can provide this option (0=unlimited)
	 * @param def, the default value for this stream. "-"=stdout, "="=stderr
	 * @param docstring, the explanation of flag displayed on help screen
	 * @return false on error (usually duplicate name, flag or longflag)
	 */
	bool addOutStream(const std::string name, char flag='o', std::string *longflag=NULL, unsigned short minoccurs=0,
			unsigned short maxoccurs=1,std::string *def=new std::string("-"), std::string docstring="");

	/**
	 * Add an log stream (an output stream intended for human readable log/error messages
	 *
	 * e.g. longflag="db" => --db=XXXX
	 *
	 * @param name, a unique name to identify this stream
	 * @param flag, the 1 letter flag used to specify input file/stdin, '\0' for no flag
	 * @param longflag, the multi-letter flag used to specify input file/stdin, NULL for no flag.
	 * @param minoccurs, the minimum number of times the user needs to provide this option
	 * @param maxoccurs, the maximum number of times the user can provide this option (0=unlimited)
	 * @param def, the default value for this stream. "-"=stdout, "="=stderr
	 * @param docstring, the explanation of flag displayed on help screen
	 * @return false on error (usually duplicate name, flag or longflag)
	 */
	bool addLogStream(const std::string name, char flag='e', std::string *longflag=NULL, unsigned short minoccurs=0,
			unsigned short maxoccurs=1,std::string *def=new std::string("="), std::string docstring="");

	/**
	 * Adds text on help screen before stream/option/flags sections (after "Usage: ... line)
	 *
	 * @param doc, the text to add
	 */
	void setPreamble(std::string doc);

	/**
	 * Adds text at end of help screen
	 *
	 * @param doc, the text to add
	 */
	void setPostamble(std::string doc);

	/**
	 * Adds a new section to the help screen (after stream/option/flags sections
	 */
	void addSection(std::string name, std::string doc);

	/**
	 * Parses, assigns and validates options.  If help is requested then help
	 * message is displayed
	 *
	 * @return unsigned short, 0= success, 1= help was displayed, 2= parse error, 3=assign/validation error
	 */
	unsigned short processesOptions(OutputBase* log = NULL);

	/**
	 * Processes the argements passed in via the contrustor against the options
	 * and streams defined with the addXXX methods.
	 *
	 * @return: bool, false on error
	 */
	bool parseOptions();

	/**
	 * Maps the anonymous options and checks occurrences of each option/flag
	 *
	 * @return bool, false on error
	 */
	bool assignAndValidateOptions();

	/**
	 * Prints option parsing, assignment and validation errors.
	 */
	void printErrors(OutputBase *log = NULL);

	/**
	 * Checks if user asked for help.
	 */
	bool isHelp();

	/**
	 * Attempts to load and connect all streams.  It starts with the default
	 * log stream and then all others.  If a stream fails then a suitable
	 * message is placed on default log stream (or stderr if log fails).
	 *
	 * @param types, the types of streams to open (default IN|OUT|LOG)
	 * @return 0= success, 2= unsupported compression, 3= failed to open stream
	 */
	int connectStreams(int types=IN|OUT|LOG);

	/**
	 * Prints a short usage summary
	 *
	 * @param fullHelpMsg, causes an extra "see --help for details" to by printed as well
	 */
	void printUsageSummary(OutputBase* log = NULL, bool fullHelpMsg=true);

	/**
	 * Prints the help message to the screen
	 */
	void printHelp(OutputBase* log=NULL);

	/**
	 * Prints the configuration used (including the options using default values)
	 *
	 * @param outs, the stream to write to
	 */
	void printConfig(OutputBase *outs);

	/**
	 * Returns true if flag was set
	 *
	 * @param name, the name flag of interest
	 * @return bool, true if flag was set
	 */
	bool isFlagSet(const std::string &name);

	/**
	 * Returns the number of occurrences of flag
	 *
	 * @param name, the name flag of interest
	 * @return int, number of occurrences, -1 on flag not defined
	 */
	int getFlagCount(const std::string &name);

	/**
	 * Returns true if flag was set
	 *
	 * @param flag, the flag of interest
	 * @return bool, true if flag was set
	 * @deprecated use the name version
	 */
	bool isFlagSet(char flag);

	/**
	 * Returns the number of occurrences of flag
	 *
	 * @param flag, the flag of interest
	 * @return int, number of occurrences
	 * @deprecated use the name version
	 */
	int getFlagCount(char flag);

	/**
	 * Get the named option's value as an int
	 */
	int getOptionInt(const std::string &name);

	/**
	 * Get the named option's value as a string
	 */
	std::string getOptionString(const std::string &name);

	/**
	 * Get the named option's values as a vector of strings
	 */
	const std::vector<std::string> getOptionStrings(const std::string &name);

	/**
	 * Get the named anonymous option's value as a string
	 */
	std::string getAnonymousOptionString(const std::string &name);

	/**
	 * Get the all named anonymous option's values as a vector of strings
	 */
	const std::vector<std::string> &getAnonymousOptionStrings(const std::string &name);

	/**
	 * Get an input stream (IO class)
	 *
	 * @param name, the name of the instream to retrieve
	 * @return InputBase*, a pointer to the (borrowed) stream object (or NULL on error)
	 */
	InputBase *getInStream(const std::string &name);

	/**
	 * Get an output stream (IO class)
	 *
	 * @param name, the name of the outstream to retrieve
	 * @return OutputBase*, a pointer to the (borrowed) stream object (or NULL on error)
	 */
	OutputBase *getOutStream(const std::string &name);

	/**
	 * Get an log stream (IO class)
	 *
	 * @param name, the name of the logstream to retrieve
	 * @return OutputBase*, a pointer to the (borrowed) stream object (or NULL on error)
	 */
	OutputBase *getLogStream(const std::string &name);

	/**
	 * Gets the default (first defined) input stream.
	 *
	 * @return, the input stream, NULL if none defined or error
	 */
	InputBase *getDefaultInStream();

	/**
	 * Gets the default (first defined) output stream.
	 *
	 * @return, the output stream, NULL if none defined or error
	 */
	OutputBase *getDefaultOutStream();

	/**
	 * Gets the default (first defined) log stream.
	 *
	 * @return, the log stream, NULL if none defined or error
	 */
	OutputBase *getDefaultLogStream();

	/**
	 * Get a list input streams (IO class).
	 *
	 * @param name, the name of the instream to retrieve
	 * @return InputBase*, a pointer to the (borrowed) stream object (or NULL on error)
	 */
	std::vector<InputBase*> getInStreams(const std::string &name);

	/**
	 * Get a list output streams (IO class)
	 *
	 * @param name, the name of the outstream to retrieve
	 * @return OutputBase*, a pointer to the (borrowed) stream object (or NULL on error)
	 */
	std::vector<OutputBase*> getOutStreams(const std::string &name);

	/**
	 * Get a list log streams (IO class)
	 *
	 * @param name, the name of the logstream to retrieve
	 * @return OutputBase*, a pointer to the (borrowed) stream object (or NULL on error)
	 */
	std::vector<OutputBase*> getLogStreams(const std::string &name);

	/**
	 * Instruct getInStream*() functions to return unique objects for each file
	 * (and thus open unique file handles).
	 */
	void unsetCacheInStreams() {
		this->cacheInStream = false;
	}

	/**
	 * Set the 3 components of the version string for this tool.  Note: this
	 * will be printed in the help text
	 *
	 * @param major short, the major version number
	 * @param minor short, the minor version number
	 * @param patch short, the patch version number
	 * @param versionDate std::string, the date (YYYY-MM-DD format) when the version was created
	 */
	void setVersionNumber(short major, short minor, short patch, std::string versionDate);
};

#endif /* CONFIGURATOR_H_ */
