/*
 * ZLibOutput_unittest.cpp
 *
 *  Created on: 27 Oct 2014
 *      Author: aroboutson
 */


#include <string>
#include <fstream>
#include <sstream>

#include "gtest/gtest.h"

#include <libbiostreams/io/ZLibOutput.h>

#define OUTFILE "/tmp/ZLibOutput_tmpfile"
#define OUTFILEGZ "/tmp/ZLibOutput_tmpfile.gz"

std::stringstream buffer;
std::string b2;

const char *readGZFile(std::string filename) {

	buffer.str("");

	// decompress file
	char cmd[1000];
	sprintf(cmd, "gunzip -c %s > %s", filename.c_str(), OUTFILE);
	system(cmd);

//	// pre-allocate space
	std::ifstream t(OUTFILE);
//	t.seekg(0, std::ios::end);
//	buffer.reserve(t.tellg());
//	t.seekg(0, std::ios::beg);

	// read
	buffer << t.rdbuf();
	b2 = buffer.str();

	return b2.c_str();
}

TEST(ZLibOutput, OpenFileStdOut) {
	ZLibOutput *out = new ZLibOutput("-");

	EXPECT_EQ(out->open(), true);

	delete out;
}

TEST(ZLibOutput, OpenFileStdErr) {
	ZLibOutput *out = new ZLibOutput("=");

	EXPECT_EQ(out->open(), true);

	delete out;
}

TEST(ZLibOutput, OpenFile) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	EXPECT_EQ(out->open(), true);

	delete out;
}

TEST(ZLibOutput, WriteCString) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	EXPECT_EQ(out->write("hello"), true);

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(read, "hello");
}

TEST(ZLibOutput, WriteChar) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	EXPECT_EQ(out->write('h'), true);
	EXPECT_EQ(out->write('e'), true);

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(read, "he");
}

TEST(ZLibOutput, WriteStdString) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	std::string write = "hello";
	EXPECT_EQ(out->write(write), true);

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(read, write.c_str());
}

TEST(ZLibOutput, WriteLnStdString) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	std::string write = "hello";
	EXPECT_EQ(out->writeln(write), true);

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(read, "hello\n");
}

TEST(ZLibOutput, WriteUnsignedLong) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	unsigned long write = 0xFFFFFFFFFFFFFFFF;
	EXPECT_EQ(out->write(write), true);

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ(read, "18446744073709551615");
}

TEST(ZLibOutput, WriteLong) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	long write = 2147483647;
	EXPECT_EQ(out->write(write), true);

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ(read, "2147483647");
}

TEST(ZLibOutput, WriteLongNeg) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	long write = -2147483647;
	EXPECT_EQ(out->write(write), true);

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ(read, "-2147483647");
}

TEST(ZLibOutput, WriteEndl) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	EXPECT_EQ(out->endl(), true);

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(read, "\n");
}



TEST(ZLibOutput, StreamCString) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	const char *write = "hello";
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(read, write);
}

TEST(ZLibOutput, StreamChar) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	*out << 'h' << 'e';

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(read, "he");
}

TEST(ZLibOutput, StreamStdString) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	std::string write = "hello";
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(read, write.c_str());
}

TEST(ZLibOutput, StreamUnsignedLong) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	unsigned long write = 0xFFFFFFFFFFFFFFFF;
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ(read, "18446744073709551615");
}

TEST(ZLibOutput, StreamInt) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	int write = 2147483647;
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ(read, "2147483647");
}

TEST(ZLibOutput, StreamStdEndl) {
	ZLibOutput *out = new ZLibOutput(OUTFILEGZ);

	out->open();

	*out << std::endl;

	delete out; // this closes and flushes file

	const char *read = readGZFile(OUTFILEGZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(read, "\n");
}

