/*
 * BZLibOutput_unittest.cpp
 *
 *  Created on: 27 Oct 2014
 *      Author: aroboutson
 */


#include <string>
#include <fstream>
#include <sstream>

#include "gtest/gtest.h"

#include <libbiostreams/io/BZLibOutput.h>

#define OUTFILE "/tmp/BZLibOutput_tmpfile"
#define OUTFILEBZ "/tmp/BZLibOutput_tmpfile.bz2"

std::stringstream buffer;
std::string b2;

const char *readBZFile(std::string filename) {

	buffer.str("");

	// decompress file
	char cmd[1000];
	sprintf(cmd, "bunzip2 -c %s > %s", filename.c_str(), OUTFILE);
	system(cmd);

//	// pre-allocate space
	std::ifstream t(OUTFILE);
//	t.seekg(0, std::ios::end);
//	buffer.reserve(t.tellg());
//	t.seekg(0, std::ios::beg);

	// read
	buffer << t.rdbuf();
	b2 = buffer.str();

	return b2.c_str();
}

TEST(BZLibOutput, OpenFileStdOut) {
	BZLibOutput *out = new BZLibOutput("-");

	EXPECT_EQ(true, out->open());

	delete out;
}

TEST(BZLibOutput, OpenFileStdErr) {
	BZLibOutput *out = new BZLibOutput("=");

	EXPECT_EQ(true, out->open());

	delete out;
}

TEST(BZLibOutput, OpenFile) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	EXPECT_EQ(true, out->open());

	delete out;
}

TEST(BZLibOutput, WriteCString) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	EXPECT_EQ(true, out->write("hello"));

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("hello", read);
}

TEST(BZLibOutput, WriteChar) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	EXPECT_EQ(true, out->write('h'));
	EXPECT_EQ(true, out->write('e'));

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("he", read);
}

TEST(BZLibOutput, WriteStdString) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	std::string write = "hello";
	EXPECT_EQ(true, out->write(write));

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(write.c_str(), read);
}

TEST(BZLibOutput, WriteLnStdString) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	std::string write = "hello";
	EXPECT_EQ(true, out->writeln(write));

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("hello\n", read);
}

TEST(BZLibOutput, WriteUnsignedLong) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	unsigned long write = 0xFFFFFFFFFFFFFFFF;
	EXPECT_EQ(true, out->write(write));

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("18446744073709551615", read);
}

TEST(BZLibOutput, WriteInt) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	int write = 2147483647;
	EXPECT_EQ(true, out->write((long)write));

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("2147483647", read);
}

TEST(BZLibOutput, WriteFloat) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	float write = 3.141592;
	EXPECT_EQ(true, out->write(write));

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("3.141592", read);
}

TEST(BZLibOutput, WriteDouble) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	double write = 3.141592;
	EXPECT_EQ(true, out->write(write));

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("3.141592", read);
}

TEST(BZLibOutput, WriteEndl) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	EXPECT_EQ(true, out->endl());

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("\n", read);
}



TEST(BZLibOutput, StreamCString) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	const char *write = "hello";
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(write, read);
}

TEST(BZLibOutput, StreamChar) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	*out << 'h' << 'e';

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("he", read);
}

TEST(BZLibOutput, StreamStdString) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	std::string write = "hello";
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(write.c_str(), read);
}

TEST(BZLibOutput, StreamUnsignedLong) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	unsigned long write = 0xFFFFFFFFFFFFFFFF;
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("18446744073709551615", read);
}

TEST(BZLibOutput, StreamLong) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	long write = 2147483647;
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("2147483647", read);
}

TEST(BZLibOutput, StreamFloat) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	float write = 3.141592;
	(*out) << write;

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("3.141592", read);
}

TEST(BZLibOutput, StreamDouble) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	double write = 3.141592;
	(*out) << write;

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("3.141592", read);
}

TEST(BZLibOutput, StreamStdEndl) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	*out << std::endl;

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("\n", read);
}

TEST(BZLibOutput, StreamStdEndlEndl) {
	BZLibOutput *out = new BZLibOutput(OUTFILEBZ);

	out->open();

	*out << std::endl;
	*out << std::endl;

	delete out; // this closes and flushes file

	const char *read = readBZFile(OUTFILEBZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("\n\n", read);
}

