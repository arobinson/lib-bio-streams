/*
 * BLZMAInput_unittest.cpp
 *
 *  Created on: 17 Oct 2014
 *      Author: arobinson
 */

#include <libbiostreams/io/LZMAInput.h>
#include "gtest/gtest.h"

// data/test.fa.xz
#define FILEHEX "\xfd\x37\x7a\x58\x5a\x00\x00\x04\xe6\xd6"\
				"\xb4\x46\x02\x00\x21\x01\x16\x00\x00\x00"\
				"\x74\x2f\xe5\xa3\xe0\x00\x84\x00\x33\x5d"\
				"\x00\x1f\x1a\x08\xa6\xf7\x66\x05\x5c\x32"\
				"\x09\x6d\x58\x11\x45\xd2\x42\x05\x51\x33"\
				"\x07\x1a\x80\x6b\x3c\x5a\x4a\x8a\xeb\x81"\
				"\x1f\xb3\x84\x5c\xbe\x0a\x7e\xd5\xa0\x3a"\
				"\xe7\xca\xb7\x68\x0e\x7b\xda\xbd\x36\x9c"\
				"\x57\x00\x00\x00\xea\xa4\xb0\x24\x37\xdc"\
				"\x65\x19\x00\x01\x4f\x85\x01\x00\x00\x00"\
				"\xe3\x34\xe1\xfc\xb1\xc4\x67\xfb\x02\x00"\
				"\x00\x00\x00\x04\x59\x5a"

#define FILEHEX_SIZE 116


//TEST(LZMAInput, OpenStdIn) {
//	LZMAInput *in = new LZMAInput("-", (const unsigned char*)"", 0);
//
//	EXPECT_EQ(in->open(), true);
//
//	delete in;
//}

TEST(LZMAInput, OpenFile) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)"", 0);

	EXPECT_EQ(in->open(), true);

	delete in;
}

TEST(LZMAInput, OpenFileNotExists) {
	LZMAInput *in = new LZMAInput("data/non-existent-file.xz", (const unsigned char*)"", 0);

	EXPECT_EQ(in->open(), false);

	delete in;
}

TEST(LZMAInput, ReadLine1) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)"", 0);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(LZMAInput, ReadLine2) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	delete in;
}

TEST(LZMAInput, ReadLineAll) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare(">hello2 ") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare(">hello3") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("") == 0);

	EXPECT_TRUE(in->eof());

	delete in;
}

TEST(LZMAInput, ReadLineTooFar) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine(); // header 1
	line = in->readLine();	// dna 1
	line = in->readLine();	// header 2
	line = in->readLine();	// dna 2
	line = in->readLine();	// header 3
	line = in->readLine();	// dna 3
	line = in->readLine();	// blank after EOL

	line = in->readLine();	// too far
	EXPECT_TRUE(line.compare("") == 0);

	EXPECT_TRUE(in->eof());

	delete in;
}

TEST(LZMAInput, ReadLineWhile) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)"", 0);
	in->open();

	std::string line;
	short linecount = 0;

	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	}
	EXPECT_TRUE(linecount == 6);

	delete in;
}

TEST(LZMAInput, ReadLineWhileBlank) {
	LZMAInput *in = new LZMAInput("data/blank.xz", (const unsigned char*)"", 0);
	in->open();

	std::string line;
	short linecount = 0;

	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	}
	EXPECT_TRUE(linecount == 0);

	delete in;
}

TEST(LZMAInput, ReadLineWhileNoNewline) {
	LZMAInput *in = new LZMAInput("data/nonewline.fa.xz", (const unsigned char*)"", 0);
	in->open();

	std::string line;
	short linecount = 0;

	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	}
	EXPECT_TRUE(linecount == 6);

	delete in;
}

TEST(LZMAInput, ReadLineHeaderPartialFile2) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)FILEHEX, 2);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(LZMAInput, ReadLineHeaderPartialFile4) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)FILEHEX, 4);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(LZMAInput, ReadLineHeaderPartialFile6) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)FILEHEX, 6);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(LZMAInput, ReadLineHeaderPartialFile8) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)FILEHEX, 8);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(LZMAInput, ReadLineHeaderPartialFile16) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)FILEHEX, 16);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(LZMAInput, ReadLineHeaderPartialFile32) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)FILEHEX, 32);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(LZMAInput, ReadLineHeaderPartialFile36) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)FILEHEX, 36);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(LZMAInput, ReadLineHeaderPartialFile40) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)FILEHEX, 40);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(LZMAInput, ReadLineHeaderPartialFile48) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)FILEHEX, 48);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(LZMAInput, ReadLineHeaderPartialFile64) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)FILEHEX, 64);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(LZMAInput, ReadLineHeaderFullFile) {
	LZMAInput *in = new LZMAInput("data/test.fa.xz", (const unsigned char*)FILEHEX, FILEHEX_SIZE);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(LZMAInput, ReadLineHeaderTooLong) {
	LZMAInput *in = new LZMAInput("data/rec1.fa.xz", (const unsigned char*)FILEHEX, FILEHEX_SIZE);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	}

	EXPECT_TRUE(linecount == 6);

	delete in;
}

TEST(LZMAInput, DualStream) {
	LZMAInput *in = new LZMAInput("data/cat2.fa.xz", (const unsigned char*)"", 0);
	in->open();
	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">hello1 world") == 0);
	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);
	line = in->readLine();
	EXPECT_TRUE(line.compare(">hello1 world") == 0);
	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	delete in;
}

TEST(LZMAInput, DualStreamNoNewLine) {
	LZMAInput *in = new LZMAInput("data/cat2nonewline.fa.xz", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine();
	line = in->readLine();
	line = in->readLine();
	line = in->readLine();
	line = in->readLine();

	line = in->readLine();	// last line of first stream and first line of second stream
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC>hello1 world") == 0);

	line = in->readLine();	// 2nd line of second stream
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	delete in;
}

// Buffer size tests //
#define INBUF_SEQCOUNT 100
#define INBUF_SEQSIZE  268
TEST(LZMAInput, InputBufferSizeMinus8) {
	LZMAInput *in = new LZMAInput("data/8184-100x268.fa.xz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}

TEST(LZMAInput, InputBufferSizeMinus4) {
	LZMAInput *in = new LZMAInput("data/8188-100x268.fa.xz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}

TEST(LZMAInput, InputBufferSize) {
	LZMAInput *in = new LZMAInput("data/8192-100x268.fa.xz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}
TEST(LZMAInput, InputBufferSizePlus4) {
	LZMAInput *in = new LZMAInput("data/8196-100x268.fa.xz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}
TEST(LZMAInput, InputBufferSizePlus8) {
	LZMAInput *in = new LZMAInput("data/8200-100x268.fa.xz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}

//////////////////////////////////////////
#define OUTBUF_SEQCOUNT 64
#define OUTBUF_SEQSIZE  1016
#define OUTBUF_LASTSEQSIZE  952
TEST(LZMAInput, OutputBufferSizeMinus2) {
	LZMAInput *in = new LZMAInput("data/65534-64x1016.fa.xz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE - 2)) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE - 2);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << " " << linecount << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}

TEST(LZMAInput, OutputBufferSizeMinus1) {
	LZMAInput *in = new LZMAInput("data/65535-64x1016.fa.xz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE - 1)) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE - 1);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}

TEST(LZMAInput, OutputBufferSize) {
	LZMAInput *in = new LZMAInput("data/65536-64x1016.fa.xz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE )) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}
TEST(LZMAInput, OutputBufferSizePlus1) {
	LZMAInput *in = new LZMAInput("data/65537-64x1016.fa.xz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE + 1)) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE + 1);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}
TEST(LZMAInput, OutputBufferSizePlus2) {
	LZMAInput *in = new LZMAInput("data/65538-64x1016.fa.xz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE + 2)) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE + 2);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}
