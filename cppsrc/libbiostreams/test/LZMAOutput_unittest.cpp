/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * LZMAOutput_unittest.cpp
 *
 *  Created on: 28 Oct 2014
 *      Author: arobinson
 */

#include <string>
#include <fstream>
#include <sstream>

#include "gtest/gtest.h"

#include <libbiostreams/io/LZMAOutput.h>

#define OUTFILE "/tmp/LZMAOutput_tmpfile"
#define OUTFILEXZ "/tmp/LZMAOutput_tmpfile.xz"

std::stringstream buffer;
std::string b2;

const char *readXZFile(std::string filename) {

	buffer.str("");

	// decompress file
	char cmd[1000];
	sprintf(cmd, "xz -dc %s > %s", filename.c_str(), OUTFILE);
	system(cmd);
//	std::cerr << "cmd: " << cmd << std::endl;

//	// pre-allocate space
	std::ifstream t(OUTFILE);
//	t.seekg(0, std::ios::end);
//	buffer.reserve(t.tellg());
//	t.seekg(0, std::ios::beg);

	// read
	buffer << t.rdbuf();
	b2 = buffer.str();

	return b2.c_str();
}

TEST(LZMAOutput, OpenFileStdOut) {
	LZMAOutput *out = new LZMAOutput("-");

	EXPECT_EQ(true, out->open());

	delete out;
}

TEST(LZMAOutput, OpenFileStdErr) {
	LZMAOutput *out = new LZMAOutput("=");

	EXPECT_EQ(true, out->open());

	delete out;
}

TEST(LZMAOutput, OpenFile) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	EXPECT_EQ(true, out->open());

	delete out;
}

TEST(LZMAOutput, WriteCString) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	EXPECT_EQ(true, out->write("hello"));

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("hello", read);
}

TEST(LZMAOutput, WriteChar) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	EXPECT_EQ(true, out->write('h'));
	EXPECT_EQ(true, out->write('e'));

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("he", read);
}

TEST(LZMAOutput, WriteStdString) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	std::string write = "hello";
	EXPECT_EQ(true, out->write(write));

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(write.c_str(), read);
}

TEST(LZMAOutput, WriteLnStdString) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	std::string write = "hello";
	EXPECT_EQ(true, out->writeln(write));

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("hello\n", read);
}

TEST(LZMAOutput, WriteUnsignedLong) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	unsigned long write = 0xFFFFFFFFFFFFFFFF;
	EXPECT_EQ(true, out->write(write));

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("18446744073709551615", read);
}

TEST(LZMAOutput, WriteShort) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	short write = 32000;
	EXPECT_EQ(true, out->write((long)write));

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("32000", read);
}

TEST(LZMAOutput, WriteInt) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	int write = 2147483647;
	EXPECT_EQ(true, out->write((long)write));

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("2147483647", read);
}

TEST(LZMAOutput, WriteLong) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	long write = 2147483647;
	EXPECT_EQ(true, out->write(write));

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("2147483647", read);
}

TEST(LZMAOutput, WriteFloat) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	float write = 3.141592;
	EXPECT_EQ(true, out->write(write));

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("3.141592", read);
}

TEST(LZMAOutput, WriteDouble) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	double write = 3.141592;
	EXPECT_EQ(true, out->write(write));

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("3.141592", read);
}

TEST(LZMAOutput, WriteEndl) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	EXPECT_EQ(true, out->endl());

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("\n", read);
}



TEST(LZMAOutput, StreamCString) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	const char *write = "hello";
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(write, read);
}

TEST(LZMAOutput, StreamChar) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	*out << 'h' << 'e';

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("he", read);
}

TEST(LZMAOutput, StreamStdString) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	std::string write = "hello";
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(write.c_str(), read);
}

TEST(LZMAOutput, StreamUnsignedShort) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	unsigned short write = 65000;
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("65000", read);
}

TEST(LZMAOutput, StreamUnsignedInt) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	unsigned int write = 3147483647;
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("3147483647", read);
}

TEST(LZMAOutput, StreamUnsignedLong) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	unsigned long write = 0xFFFFFFFFFFFFFFFF;
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("18446744073709551615", read);
}

TEST(LZMAOutput, StreamShort) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	short write = 32000;
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("32000", read);
}

TEST(LZMAOutput, StreamInt) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	int write = 2147483647;
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("2147483647", read);
}

TEST(LZMAOutput, StreamLong) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	long write = 2147483647;
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("2147483647", read);
}

TEST(LZMAOutput, StreamFloat) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	float write = 3.141592;
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("3.141592", read);
}

TEST(LZMAOutput, StreamDouble) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	double write = 3.141592;
	*out << write;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << write << std::endl;
	EXPECT_STREQ("3.141592", read);
}

TEST(LZMAOutput, StreamStdEndl) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	*out << std::endl;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("\n", read);
}

TEST(LZMAOutput, StreamStdEndlEndl) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	*out << std::endl;
	*out << std::endl;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ("\n\n", read);
}

////// BUFFER Overflows //////

TEST(LZMAOutput, StreamInBufferMinus2) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	char buf[LZMAOUTPUT_INCHUNK-2];
	for (size_t i = 0; i < LZMAOUTPUT_INCHUNK-2; ++i)
		buf[i] = 'a'+i%26;
	buf[LZMAOUTPUT_INCHUNK-2 -1] = '\0';

	*out << buf;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(buf, read);
}

TEST(LZMAOutput, StreamInBufferMinus1) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	char buf[LZMAOUTPUT_INCHUNK-1];
	for (size_t i = 0; i < LZMAOUTPUT_INCHUNK-1; ++i)
		buf[i] = 'a'+i%26;
	buf[LZMAOUTPUT_INCHUNK-1 -1] = '\0';

	*out << buf;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(buf, read);
}

TEST(LZMAOutput, StreamInBuffer) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	char buf[LZMAOUTPUT_INCHUNK];
	for (size_t i = 0; i < LZMAOUTPUT_INCHUNK; ++i)
		buf[i] = 'a'+i%26;
	buf[LZMAOUTPUT_INCHUNK -1] = '\0';

	*out << buf;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(buf, read);
}

TEST(LZMAOutput, StreamInBufferPlus1) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	char buf[LZMAOUTPUT_INCHUNK+1];
	for (size_t i = 0; i < LZMAOUTPUT_INCHUNK+1; ++i)
		buf[i] = 'a'+i%26;
	buf[LZMAOUTPUT_INCHUNK+1 -1] = '\0';

	*out << buf;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(buf, read);
}

TEST(LZMAOutput, StreamInBufferPlus2) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	char buf[LZMAOUTPUT_INCHUNK+2];
	for (size_t i = 0; i < LZMAOUTPUT_INCHUNK+2; ++i)
		buf[i] = 'a'+i%26;
	buf[LZMAOUTPUT_INCHUNK+2 -1] = '\0';

	*out << buf;

	delete out; // this closes and flushes file

	const char *read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;
	EXPECT_STREQ(buf, read);
}

TEST(LZMAOutput, StreamOutBufferMinus4) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	// read sample (randomly generated) data
	char buf[LZMAOUTPUT_INCHUNK*2];
	const char *read = readXZFile("data/1020-100x18.fa.xz");
	size_t i;
	for (i = 0; i < LZMAOUTPUT_INCHUNK*2 && read[i] != '\0'; ++i)
		buf[i] = read[i];
	buf[i] = '\0';

	// write and flush
	*out << buf;
	delete out; // this closes and flushes file

	// read compressed data
	read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;

	// check they match
	EXPECT_STREQ(buf, read);
}

TEST(LZMAOutput, StreamOutBuffer) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	// read sample (randomly generated) data
	char buf[LZMAOUTPUT_INCHUNK*2];
	const char *read = readXZFile("data/1024-100x18.fa.xz");
	size_t i;
	for (i = 0; i < LZMAOUTPUT_INCHUNK*2 && read[i] != '\0'; ++i)
		buf[i] = read[i];
	buf[i] = '\0';

	// write and flush
	*out << buf;
	delete out; // this closes and flushes file

	// read compressed data
	read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;

	// check they match
	EXPECT_STREQ(buf, read);
}

TEST(LZMAOutput, StreamOutBufferPlus4) {
	LZMAOutput *out = new LZMAOutput(OUTFILEXZ);

	out->open();

	// read sample (randomly generated) data
	char buf[LZMAOUTPUT_INCHUNK*2];
	const char *read = readXZFile("data/1028-100x18.fa.xz");
	size_t i;
	for (i = 0; i < LZMAOUTPUT_INCHUNK*2 && read[i] != '\0'; ++i)
		buf[i] = read[i];
	buf[i] = '\0';

	// write and flush
	*out << buf;
	delete out; // this closes and flushes file

	// read compressed data
	read = readXZFile(OUTFILEXZ);
//	std::cout << "'" << read << "'" << std::endl;

	// check they match
	EXPECT_STREQ(buf, read);
}


