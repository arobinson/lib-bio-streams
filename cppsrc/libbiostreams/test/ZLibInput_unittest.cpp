/*
 * ZLibInput_unittest.cpp
 *
 *  Created on: 16 Oct 2014
 *      Author: arobinson
 */

#include "gtest/gtest.h"

#include <libbiostreams/io/ZLibInput.h>

// data/test.fa.xz
#define FILEHEX "\x1f\x8b\x08\x08\x1e\x49\x2a\x54\x00\x03\x74\x65\x73\x74\x2e\x66"\
				"\x61\x00\xb3\xcb\x48\xcd\xc9\xc9\x37\x54\x28\xcf\x2f\xca\x49\xe1"\
				"\x72\x74\x76\x74\x76\x77\x0e\x09\x71\x76\x77\x74\x77\x0c\x01\xd3"\
				"\x40\x86\x73\x88\x33\x88\x01\xe4\x39\x73\xd9\x81\xd5\x1b\x29\x10"\
				"\xad\xd4\x98\x08\x95\x00\x87\x11\xb7\xfe\x85\x00\x00\x00"

#define FILEHEX_SIZE 78


//TEST(ZLibInput, OpenStdIn) {
//	ZLibInput *in = new ZLibInput("-", (const unsigned char*)"", 0);
//
//	EXPECT_EQ(in->open(), true);
//
//	delete in;
//}

TEST(ZLibInput, OpenFile) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)"", 0);

	EXPECT_EQ(in->open(), true);

	delete in;
}

TEST(ZLibInput, OpenFileNotExists) {
	ZLibInput *in = new ZLibInput("data/non-existent-file.gz", (const unsigned char*)"", 0);

	EXPECT_EQ(in->open(), false);

	delete in;
}

TEST(ZLibInput, ReadLine1) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)"", 0);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(ZLibInput, ReadLine2) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	delete in;
}

TEST(ZLibInput, ReadLineAll) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare(">hello2 ") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare(">hello3") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("") == 0);

	EXPECT_TRUE(in->eof());

	delete in;
}

TEST(ZLibInput, ReadLineTooFar) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine(); // header 1
	line = in->readLine();	// dna 1
	line = in->readLine();	// header 2
	line = in->readLine();	// dna 2
	line = in->readLine();	// header 3
	line = in->readLine();	// dna 3
	line = in->readLine();	// blank after EOL

	line = in->readLine();	// too far
	EXPECT_TRUE(line.compare("") == 0);

	EXPECT_TRUE(in->eof());

	delete in;
}

TEST(ZLibInput, ReadLineWhile) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)"", 0);
	in->open();

	std::string line;
	short linecount = 0;

	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	}
	EXPECT_TRUE(linecount == 6);

	delete in;
}

TEST(ZLibInput, ReadLineWhileBlank) {
	ZLibInput *in = new ZLibInput("data/blank.gz", (const unsigned char*)"", 0);
	in->open();

	std::string line;
	short linecount = 0;

	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	}
	EXPECT_TRUE(linecount == 0);

	delete in;
}

TEST(ZLibInput, ReadLineWhileNoNewline) {
	ZLibInput *in = new ZLibInput("data/nonewline.fa.gz", (const unsigned char*)"", 0);
	in->open();

	std::string line;
	short linecount = 0;

	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	}
	EXPECT_TRUE(linecount == 6);

	delete in;
}

TEST(ZLibInput, ReadLineHeaderPartialFile2) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)FILEHEX, 2);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(ZLibInput, ReadLineHeaderPartialFile4) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)FILEHEX, 4);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(ZLibInput, ReadLineHeaderPartialFile6) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)FILEHEX, 6);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(ZLibInput, ReadLineHeaderPartialFile8) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)FILEHEX, 8);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(ZLibInput, ReadLineHeaderPartialFile16) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)FILEHEX, 16);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(ZLibInput, ReadLineHeaderPartialFile32) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)FILEHEX, 32);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(ZLibInput, ReadLineHeaderPartialFile36) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)FILEHEX, 36);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(ZLibInput, ReadLineHeaderPartialFile40) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)FILEHEX, 40);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(ZLibInput, ReadLineHeaderPartialFile48) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)FILEHEX, 48);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(ZLibInput, ReadLineHeaderPartialFile64) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)FILEHEX, 64);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(ZLibInput, ReadLineHeaderFullFile) {
	ZLibInput *in = new ZLibInput("data/test.fa.gz", (const unsigned char*)FILEHEX, FILEHEX_SIZE);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(ZLibInput, ReadLineHeaderTooLong) {
	ZLibInput *in = new ZLibInput("data/rec1.fa.gz", (const unsigned char*)FILEHEX, FILEHEX_SIZE);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	}

	EXPECT_TRUE(linecount == 6);

	delete in;
}

TEST(ZLibInput, DualStream) {
	ZLibInput *in = new ZLibInput("data/cat2.fa.gz", (const unsigned char*)"", 0);
	in->open();
	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">hello1 world") == 0);
	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);
	line = in->readLine();
	EXPECT_TRUE(line.compare(">hello1 world") == 0);
	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	delete in;
}

TEST(ZLibInput, DualStreamNoNewLine) {
	ZLibInput *in = new ZLibInput("data/cat2nonewline.fa.gz", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine();
	line = in->readLine();
	line = in->readLine();
	line = in->readLine();
	line = in->readLine();

	line = in->readLine();	// last line of first stream and first line of second stream
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC>hello1 world") == 0);

	line = in->readLine();	// 2nd line of second stream
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	delete in;
}

// Buffer size tests //
#define INBUF_SEQCOUNT 100
#define INBUF_SEQSIZE  251
TEST(ZLibInput, InputBufferSizeMinus2) {
	ZLibInput *in = new ZLibInput("data/8190-100x251.fa.gz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}

TEST(ZLibInput, InputBufferSizeMinus1) {
	ZLibInput *in = new ZLibInput("data/8191-100x251.fa.gz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}

TEST(ZLibInput, InputBufferSize) {
	ZLibInput *in = new ZLibInput("data/8192-100x251.fa.gz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}
TEST(ZLibInput, InputBufferSizePlus1) {
	ZLibInput *in = new ZLibInput("data/8193-100x251.fa.gz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}
TEST(ZLibInput, InputBufferSizePlus2) {
	ZLibInput *in = new ZLibInput("data/8194-100x251.fa.gz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}

//////////////////////////////////////////
#define OUTBUF_SEQCOUNT 64
#define OUTBUF_SEQSIZE  1016
#define OUTBUF_LASTSEQSIZE  952
TEST(ZLibInput, OutputBufferSizeMinus2) {
	ZLibInput *in = new ZLibInput("data/65534-64x1016.fa.gz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE - 2)) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE - 2);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << " " << linecount << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}

TEST(ZLibInput, OutputBufferSizeMinus1) {
	ZLibInput *in = new ZLibInput("data/65535-64x1016.fa.gz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE - 1)) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE - 1);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}

TEST(ZLibInput, OutputBufferSize) {
	ZLibInput *in = new ZLibInput("data/65536-64x1016.fa.gz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE )) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}
TEST(ZLibInput, OutputBufferSizePlus1) {
	ZLibInput *in = new ZLibInput("data/65537-64x1016.fa.gz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE + 1)) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE + 1);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}
TEST(ZLibInput, OutputBufferSizePlus2) {
	ZLibInput *in = new ZLibInput("data/65538-64x1016.fa.gz", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE + 2)) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE + 2);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}

