/*
 * PlainInput_unittest.cpp
 *
 *  Created on: 13 Oct 2014
 *      Author: arobinson
 */

#include <libbiostreams/io/PlainInput.h>
#include "gtest/gtest.h"


TEST(PlainInput, OpenStdIn) {
	PlainInput *in = new PlainInput("-", (const unsigned char*)"", 0);

	EXPECT_EQ(in->open(), true);

	delete in;
}

TEST(PlainInput, OpenFile) {
	PlainInput *in = new PlainInput("data/test.fa", (const unsigned char*)"", 0);

	EXPECT_EQ(in->open(), true);

	delete in;
}

TEST(PlainInput, OpenFileReadLine) {
	PlainInput *in = new PlainInput("data/test.fa", (const unsigned char*)"", 0);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(PlainInput, OpenFileReadLineHeader0) {
	PlainInput *in = new PlainInput("data/test.fa", (const unsigned char*)">FILE", 5);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">FILEo1 world") == 0);

	delete in;
}

TEST(PlainInput, OpenFileReadLineHeader1a) {
	PlainInput *in = new PlainInput("data/test.fa", (const unsigned char*)">F\nA", 4);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">F") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("Alo1 world") == 0);

	delete in;
}

TEST(PlainInput, OpenFileReadLineHeader1b) {
	PlainInput *in = new PlainInput("data/test.fa", (const unsigned char*)">F\n\n", 4);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">F") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("lo1 world") == 0);

	delete in;
}

TEST(PlainInput, OpenFileReadLineHeader2a) {
	PlainInput *in = new PlainInput("data/test.fa", (const unsigned char*)">F\nA\n", 5);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">F") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("A") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("o1 world") == 0);

	delete in;
}

TEST(PlainInput, OpenFileReadLineHeader2b) {
	PlainInput *in = new PlainInput("data/test.fa", (const unsigned char*)">F\nA\n>2", 7);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">F") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("A") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare(">2 world") == 0);

	delete in;
}

TEST(PlainInput, OpenFileReadLineEmplyFile) {
	PlainInput *in = new PlainInput("data/blank", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare("") == 0);

	EXPECT_TRUE(in->eof());

	delete in;
}

TEST(PlainInput, OpenFileReadLineHeaderLongerThanFile1) {
	PlainInput *in = new PlainInput("data/blank", (const unsigned char*)">File1", 6);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">File1") == 0);

	line = in->readLine(); // triggers eof
	EXPECT_TRUE(line.compare("") == 0); // not valid but should be empty

	EXPECT_TRUE(in->eof());

	delete in;
}

TEST(PlainInput, OpenFileReadLineHeaderLongerThanFile2) {
	PlainInput *in = new PlainInput("data/blank", (const unsigned char*)">File1\n", 7);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">File1") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("") == 0);

	EXPECT_TRUE(in->eof());

	delete in;
}

TEST(PlainInput, ReadLineTooFar) {
	PlainInput *in = new PlainInput("data/test.fa", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine(); // header 1
	line = in->readLine();	// dna 1
	line = in->readLine();	// header 2
	line = in->readLine();	// dna 2
	line = in->readLine();	// header 3
	line = in->readLine();	// dna 3
	line = in->readLine();	// blank after EOL

	line = in->readLine();	// too far
	EXPECT_TRUE(line.compare("") == 0);

	EXPECT_TRUE(in->eof());

	delete in;
}

TEST(PlainInput, ReadLineWhile) {
	PlainInput *in = new PlainInput("data/test.fa", (const unsigned char*)"", 0);
	in->open();

	std::string line;
	short linecount = 0;

	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
//		std::cerr << "Line " << linecount << ": '" << line << "'" << std::endl;
	}

//	std::cerr << "Linecount: " << linecount << std::endl;
	EXPECT_TRUE(linecount == 6);

	delete in;
}

TEST(PlainInput, ReadLineWhileNoNewline) {
	PlainInput *in = new PlainInput("data/nonewline.fa", (const unsigned char*)"", 0);
	in->open();

	std::string line;
	short linecount = 0;

	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
//		std::cerr << "Line " << linecount << ": '" << line << "'" << std::endl;
	}

//	std::cerr << "Linecount: " << linecount << std::endl;
	EXPECT_TRUE(linecount == 6);

	delete in;
}
