/*
 * BZLibInput_unittest.cpp
 *
 *  Created on: 17 Oct 2014
 *      Author: arobinson
 */

#include <libbiostreams/io/BZLibInput.h>
#include "gtest/gtest.h"

// data/test.fa.bz2
#define FILEHEX "\x42\x5a\x68\x39\x31\x41\x59\x26\x53\x59"\
				"\x00\xfd\x61\xe0\x00\x00\x05\xdf\x80\x00"\
				"\x10\x40\x00\x38\x01\x28\x80\x04\x00\x06"\
				"\x44\x90\x80\x20\x00\x40\xda\x44\x99\x19"\
				"\x36\x90\x50\xd3\x4c\x00\x60\xeb\x36\xbd"\
				"\xe8\xc1\x94\x33\xb2\x0c\x8c\x63\x43\x34"\
				"\x89\x1a\x21\x12\x4a\x20\x63\x18\x94\xa2"\
				"\x06\x41\x05\x51\xb3\x9c\x2c\x8f\xb5\x5f"\
				"\x8b\xb9\x22\x9c\x28\x48\x00\x7e\xb0\xf0"\
				"\x00"




//TEST(BZLibInput, OpenStdIn) {
//	BZLibInput *in = new BZLibInput("-", (const unsigned char*)"", 0);
//
//	EXPECT_EQ(in->open(), true);
//
//	delete in;
//}

TEST(BZLibInput, OpenFile) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)"", 0);

	EXPECT_EQ(in->open(), true);

	delete in;
}

TEST(BZLibInput, OpenFileNotExists) {
	BZLibInput *in = new BZLibInput("data/non-existent-file.bz2", (const unsigned char*)"", 0);

	EXPECT_EQ(in->open(), false);

	delete in;
}

TEST(BZLibInput, ReadLine1) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)"", 0);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(BZLibInput, ReadLine2) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	delete in;
}

TEST(BZLibInput, ReadLineAll) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare(">hello2 ") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare(">hello3") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	line = in->readLine();
	EXPECT_TRUE(line.compare("") == 0);

	EXPECT_TRUE(in->eof());

	delete in;
}

TEST(BZLibInput, ReadLineTooFar) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine(); // header 1
	line = in->readLine();	// dna 1
	line = in->readLine();	// header 2
	line = in->readLine();	// dna 2
	line = in->readLine();	// header 3
	line = in->readLine();	// dna 3
	line = in->readLine();	// blank after EOL

	line = in->readLine();	// too far
	EXPECT_TRUE(line.compare("") == 0);

	EXPECT_TRUE(in->eof());

	delete in;
}

TEST(BZLibInput, ReadLineWhile) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)"", 0);
	in->open();

	std::string line;
	short linecount = 0;

	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	}
	EXPECT_TRUE(linecount == 6);

	delete in;
}

TEST(BZLibInput, ReadLineWhileBlank) {
	BZLibInput *in = new BZLibInput("data/blank.bz2", (const unsigned char*)"", 0);
	in->open();

	std::string line;
	short linecount = 0;

	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	}
	EXPECT_TRUE(linecount == 0);

	delete in;
}

TEST(BZLibInput, ReadLineWhileNoNewline) {
	BZLibInput *in = new BZLibInput("data/nonewline.fa.bz2", (const unsigned char*)"", 0);
	in->open();

	std::string line;
	short linecount = 0;

	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	}

	EXPECT_TRUE(linecount == 6);

	delete in;
}

TEST(BZLibInput, ReadLineHeaderPartialFile2) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)FILEHEX, 2);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(BZLibInput, ReadLineHeaderPartialFile4) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)FILEHEX, 4);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(BZLibInput, ReadLineHeaderPartialFile6) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)FILEHEX, 6);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(BZLibInput, ReadLineHeaderPartialFile8) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)FILEHEX, 8);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(BZLibInput, ReadLineHeaderPartialFile16) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)FILEHEX, 16);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(BZLibInput, ReadLineHeaderPartialFile32) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)FILEHEX, 32);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(BZLibInput, ReadLineHeaderPartialFile36) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)FILEHEX, 36);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(BZLibInput, ReadLineHeaderPartialFile40) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)FILEHEX, 40);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(BZLibInput, ReadLineHeaderPartialFile48) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)FILEHEX, 48);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(BZLibInput, ReadLineHeaderPartialFile64) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)FILEHEX, 64);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(BZLibInput, ReadLineHeaderFullFile) {
	BZLibInput *in = new BZLibInput("data/test.fa.bz2", (const unsigned char*)FILEHEX, 91);
	in->open();
	std::string line = in->readLine();

	EXPECT_TRUE(line.compare(">hello1 world") == 0);

	delete in;
}

TEST(BZLibInput, ReadLineHeaderTooLong) {
	BZLibInput *in = new BZLibInput("data/rec1.fa.bz2", (const unsigned char*)FILEHEX, 91);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	}

	EXPECT_TRUE(linecount == 6);

	delete in;
}

TEST(BZLibInput, DualStream) {
	BZLibInput *in = new BZLibInput("data/cat2.fa.bz2", (const unsigned char*)"", 0);
	in->open();
	std::string line = in->readLine();
	EXPECT_TRUE(line.compare(">hello1 world") == 0);
	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);
	line = in->readLine();
	EXPECT_TRUE(line.compare(">hello1 world") == 0);
	line = in->readLine();
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	delete in;
}

TEST(BZLibInput, DualStreamNoNewLine) {
	BZLibInput *in = new BZLibInput("data/cat2nonewline.fa.bz2", (const unsigned char*)"", 0);
	in->open();

	std::string line = in->readLine();
	line = in->readLine();
	line = in->readLine();
	line = in->readLine();
	line = in->readLine();

	line = in->readLine();	// last line of first stream and first line of second stream
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC>hello1 world") == 0);

	line = in->readLine();	// 2nd line of second stream
	EXPECT_TRUE(line.compare("ACACGCTTCGAGATTTCGAAGACTCCGAATCGC") == 0);

	delete in;
}

// Buffer size tests //
#define INBUF_SEQCOUNT 100
#define INBUF_SEQSIZE  284
TEST(BZLibInput, InputBufferSizeMinus2) {
	BZLibInput *in = new BZLibInput("data/8190-100x284.fa.bz2", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}

TEST(BZLibInput, InputBufferSizeMinus1) {
	BZLibInput *in = new BZLibInput("data/8191-100x284.fa.bz2", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}

TEST(BZLibInput, InputBufferSize) {
	BZLibInput *in = new BZLibInput("data/8192-100x284.fa.bz2", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}
TEST(BZLibInput, InputBufferSizePlus1) {
	BZLibInput *in = new BZLibInput("data/8193-100x284.fa.bz2", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}
TEST(BZLibInput, InputBufferSizePlus2) {
	BZLibInput *in = new BZLibInput("data/8194-100x284.fa.bz2", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount % 2 == 0 && line.size() != INBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == INBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == INBUF_SEQCOUNT*2);

	delete in;
}

//////////////////////////////////////////
#define OUTBUF_SEQCOUNT 64
#define OUTBUF_SEQSIZE  1016
#define OUTBUF_LASTSEQSIZE  952
TEST(BZLibInput, OutputBufferSizeMinus2) {
	BZLibInput *in = new BZLibInput("data/65534-64x1016.fa.bz2", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE - 2)) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE - 2);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << " " << linecount << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}

TEST(BZLibInput, OutputBufferSizeMinus1) {
	BZLibInput *in = new BZLibInput("data/65535-64x1016.fa.bz2", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines (6)
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE - 1)) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE - 1);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}

TEST(BZLibInput, OutputBufferSize) {
	BZLibInput *in = new BZLibInput("data/65536-64x1016.fa.bz2", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE )) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}
TEST(BZLibInput, OutputBufferSizePlus1) {
	BZLibInput *in = new BZLibInput("data/65537-64x1016.fa.bz2", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE + 1)) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE + 1);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}
TEST(BZLibInput, OutputBufferSizePlus2) {
	BZLibInput *in = new BZLibInput("data/65538-64x1016.fa.bz2", (const unsigned char*) "", 0);
	in->open();

	// count the number of lines
	std::string line;
	short linecount = 0;
	while (true) {
		line = in->readLine();
	    if (in->eof())
	    	break;
	    linecount++;
	    if (linecount == (OUTBUF_SEQCOUNT*2)) {
	    	if (line.size() != (OUTBUF_LASTSEQSIZE + 2)) {
				std::cerr << "line.size(): " << line.size() << std::endl;
				EXPECT_TRUE(line.size() == OUTBUF_LASTSEQSIZE + 2);
				break;
	    	}
	    } else if (linecount % 2 == 0 && line.size() != OUTBUF_SEQSIZE) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == OUTBUF_SEQSIZE);
	    	break;
	    } else if (linecount % 2 == 1 && line.size() != 7) {
	    	std::cerr << "line.size(): " << line.size() << std::endl;
	    	EXPECT_TRUE(line.size() == 7);
	    	break;
	    }
	}

	EXPECT_TRUE(linecount == OUTBUF_SEQCOUNT*2);

	delete in;
}
