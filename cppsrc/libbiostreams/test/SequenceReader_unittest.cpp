/*
 * SequenceReader_unittest.cpp
 *
 *  Created on: 06 Sept 2016
 *      Author: arobinson
 */

#include <libbiostreams/io/PlainInput.h>
#include <libbiostreams/readers/SequenceReader.h>
#include "gtest/gtest.h"


TEST(PlainInput, FastaNormal1) {
	PlainInput *in = new PlainInput("data/test/sequencereader/normal.fa", (const unsigned char*)"", 0);
	SequenceReader *sr = new SequenceReader(in, FASTA);
	Sequence* seq = (Sequence*)sr->next();

	ASSERT_TRUE(seq != NULL);
	EXPECT_TRUE(seq->id.compare("seq1") == 0);
	EXPECT_TRUE(seq->seq.compare("ACTGACTGAG") == 0);

	if (seq != NULL)
		delete seq;
	delete sr, in;
}

TEST(PlainInput, FastaNormal2) {
	PlainInput *in = new PlainInput("data/test/sequencereader/normal.fa", (const unsigned char*)"", 0);
	SequenceReader *sr = new SequenceReader(in, FASTA);
	Sequence* seq = (Sequence*)sr->next();
	if (seq != NULL)
		delete seq;
	seq = (Sequence*)sr->next();
	ASSERT_TRUE(seq != NULL);
	EXPECT_TRUE(seq->id.compare("seq2") == 0);
	EXPECT_TRUE(seq->seq.compare("ACTG") == 0);

	if (seq != NULL)
		delete seq;
	delete sr, in;
}

TEST(PlainInput, FastaNormal3) {
	PlainInput *in = new PlainInput("data/test/sequencereader/normal.fa", (const unsigned char*)"", 0);
	SequenceReader *sr = new SequenceReader(in, FASTA);
	Sequence* seq = (Sequence*)sr->next();
	if (seq != NULL)
		delete seq;
	seq = (Sequence*)sr->next();

	ASSERT_TRUE(seq != NULL);
	EXPECT_TRUE(sr->next() == NULL);

	if (seq != NULL)
		delete seq;
	delete sr, in;
}

TEST(PlainInput, FastaMulti1) {
	PlainInput *in = new PlainInput("data/test/sequencereader/multiline.fa", (const unsigned char*)"", 0);
	SequenceReader *sr = new SequenceReader(in, FASTA);
	Sequence* seq = (Sequence*)sr->next();

	ASSERT_TRUE(seq != NULL);
	EXPECT_TRUE(seq->id.compare("seq1") == 0);
	EXPECT_TRUE(seq->seq.compare("ACTGACTGAG") == 0);

	if (seq != NULL)
		delete seq;
	seq = (Sequence*)sr->next();

	ASSERT_TRUE(seq != NULL);
	EXPECT_TRUE(seq->id.compare("seq2") == 0);
	EXPECT_TRUE(seq->seq.compare("ACTGA") == 0);

	EXPECT_TRUE(sr->next() == NULL);

	if (seq != NULL)
		delete seq;
	delete sr, in;
}

TEST(PlainInput, FastaEmpty1) {
	PlainInput *in = new PlainInput("data/test/sequencereader/emptyseq.fa", (const unsigned char*)"", 0);
	SequenceReader *sr = new SequenceReader(in, FASTA);
	Sequence* seq = (Sequence*)sr->next();

	ASSERT_TRUE(seq != NULL);
	EXPECT_TRUE(seq->id.compare("seq1") == 0);
	EXPECT_TRUE(seq->seq.compare("") == 0);

	if (seq != NULL)
		delete seq;
	seq = (Sequence*)sr->next();

	ASSERT_TRUE(seq != NULL);
	EXPECT_TRUE(seq->id.compare("seq2") == 0);
	EXPECT_TRUE(seq->seq.compare("") == 0);

	EXPECT_TRUE(sr->next() == NULL);

	if (seq != NULL)
		delete seq;
	delete sr, in;
}

TEST(PlainInput, FastaEmpty2) {
	PlainInput *in = new PlainInput("data/test/sequencereader/empty.txt", (const unsigned char*)"", 0);
	SequenceReader *sr = new SequenceReader(in, FASTA);

	EXPECT_TRUE(sr->next() == NULL);

	delete sr, in;
}

TEST(PlainInput, FastqEmpty2) {
	PlainInput *in = new PlainInput("data/test/sequencereader/empty.txt", (const unsigned char*)"", 0);
	SequenceReader *sr = new SequenceReader(in, FASTQ);

	EXPECT_TRUE(sr->next() == NULL);

	delete sr, in;
}

TEST(PlainInput, RawEmpty2) {
	PlainInput *in = new PlainInput("data/test/sequencereader/empty.txt", (const unsigned char*)"", 0);
	SequenceReader *sr = new SequenceReader(in, RAW);

	EXPECT_TRUE(sr->next() == NULL);

	delete sr, in;
}

TEST(PlainInput, RawMEmpty2) {
	PlainInput *in = new PlainInput("data/test/sequencereader/empty.txt", (const unsigned char*)"", 0);
	SequenceReader *sr = new SequenceReader(in, RAWM);
	Sequence* seq = (Sequence*)sr->next();

	ASSERT_TRUE(seq != NULL);
	EXPECT_TRUE(seq->id.compare("raw1") == 0);
	EXPECT_TRUE(seq->seq.compare("") == 0);

	if (seq != NULL)
		delete seq;

	EXPECT_TRUE(sr->next() == NULL);

	delete sr, in;
}

TEST(PlainInput, AutoFasta) {
	PlainInput *in = new PlainInput("data/test/sequencereader/normal.fa", (const unsigned char*)"", 0);
	SequenceReader *sr = new SequenceReader(in, AUTO);
	Sequence* seq = (Sequence*)sr->next();

	ASSERT_TRUE(seq != NULL);
	EXPECT_TRUE(seq->id.compare("seq1") == 0);
	EXPECT_TRUE(seq->seq.compare("ACTGACTGAG") == 0);
	EXPECT_TRUE(seq->qual.compare("") == 0);

	if (seq != NULL)
		delete seq;
	delete sr, in;
}

TEST(PlainInput, AutoFastq) {
	PlainInput *in = new PlainInput("data/test/sequencereader/normal.fq", (const unsigned char*)"", 0);
	SequenceReader *sr = new SequenceReader(in, AUTO);
	Sequence* seq = (Sequence*)sr->next();

	ASSERT_TRUE(seq != NULL);
	EXPECT_TRUE(seq->id.compare("seq1") == 0);
	EXPECT_TRUE(seq->seq.compare("ACGTGCGT") == 0);
	EXPECT_TRUE(seq->qual.compare("GGGGGGGG") == 0);

	if (seq != NULL)
		delete seq;
	delete sr, in;
}

TEST(PlainInput, AutoRaw) {
	PlainInput *in = new PlainInput("data/test/sequencereader/normal.raw", (const unsigned char*)"", 0);
	SequenceReader *sr = new SequenceReader(in, AUTO);
	Sequence* seq = (Sequence*)sr->next();

	ASSERT_TRUE(seq != NULL);
	EXPECT_TRUE(seq->id.compare("raw1") == 0);
	EXPECT_TRUE(seq->seq.compare("ACGTGCGT") == 0);
	EXPECT_TRUE(seq->qual.compare("") == 0);

	if (seq != NULL)
		delete seq;
	delete sr, in;
}


