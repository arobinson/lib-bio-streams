/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 03/09/2014
 *      Author: arobinson
 */

#include <cstdlib>
#include <cstdio>
//#include <csignal>

#include <iostream>
#include <string>

#include "PlainInput.h"

PlainInput::PlainInput(std::string filename, const unsigned char* headerbytes, unsigned int headersize)
    :InputBase(filename, headerbytes, headersize)
{
    ihandle = NULL;
    headerConsumed = headersize == 0;
    pos = 0;
    header = "";
    header.append((char*)headerbytes, headersize);
    rEOF = false;
}

PlainInput::~PlainInput() {
//	raise(SIGINT); // breakpoint
    if (ifhandle.is_open())
        ifhandle.close();
//    std::cerr << "" << std::endl;
}


/**
 * Try to open the io source
 * @return bool, true if successful
 */
bool PlainInput::open() {

	if (ihandle != NULL)
		return true;

    if (filename == "-") {
        ihandle = &std::cin;
        return true;
    }
    else {
        ifhandle.open(filename.c_str());
        if (ifhandle.is_open()) {
        	ifhandle.seekg(headersize, std::ios_base::beg);
            ihandle = &ifhandle;
            return true;
        }
    }

    return false;
}

/**
 * Try to reset io source pointer to beginning (if makes sense)
 * @return bool, true if successful
 */
bool PlainInput::reset() {
	if (ifhandle.is_open()) {
		ifhandle.close();
		ifhandle.open(filename.c_str());
		ifhandle.seekg(0, std::ios_base::beg); //TODO: check if this is ok when header exists (i.e. add unittests)
		rEOF = false;
		headerConsumed = true;
		return true;
	}
	return false;
}

/**
 * Reads a line from the Input source
 * @return std::string, the line of text
 */
std::string PlainInput::readLine() {
    std::string line;
    if (ihandle != NULL) {
        if (!headerConsumed) {
        	size_t eolpos = header.find("\n", pos);
        	if (eolpos == std::string::npos) {
        		headerConsumed = true;
            	std::getline(*ihandle, line);
        		line = header.substr(pos) + line;
        	} else {
        		line = header.substr(pos, eolpos - pos);
        		pos = eolpos + 1;
        		if (pos > header.size())
        			headerConsumed = true;
        	}
    	} else
        	std::getline(*ihandle, line);
        rEOF = (ihandle->eof() && line.size() == 0);
    }
    return line;
}

/**
 * Indicates if the end of file has been reached
 */
bool PlainInput::eof() {
    return headerConsumed && (ihandle == NULL || rEOF);
}
