/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * LZMAInput.cpp
 *
 *  Created on: 22 Oct 2014
 *      Author: arobinson
 */

#include <iostream>
#include <stdio.h>

#include "LZMAInput.h"

LZMAInput::LZMAInput(std::string filename, const unsigned char* headerbytes, unsigned int headersize)
	:InputBase(filename, headerbytes, headersize)
{
	handle = NULL;
	fhandle = NULL;
	outProcessedPos = 0;

	state = LZI_ERROR;
	action = LZMA_RUN;
	lzma_stream tmp = LZMA_STREAM_INIT;
	strm = tmp;
}

LZMAInput::~LZMAInput() {

	lzma_end(&strm);

	// close file
	if (fhandle != NULL)
		fclose(fhandle);
}

/**
 * Copies len characters from src to dest arrays.  You must ensure that both
 * src and dest arrays are of suitable length.
 */
char* LZMAInput::unsignedToSigned(char* dest, const unsigned char* src, size_t len) {
	for (size_t i = 0; i < len; ++i)
		dest[i] = src[i];
	return dest;
}

/**
 * Try to open the io source
 * @return bool, true if successful
 */
bool LZMAInput::open() {

	if (handle != NULL)
		return true;

	// open the file handle
	if (filename == "-") {
		handle = stdin;
	} else {
		fhandle = fopen(filename.c_str(), "r");
		if (fhandle == NULL) {
			std::cerr << "Error: failed to open input file '" << filename.c_str() << "'" << std::endl;
			return false;
		}
		fseek(fhandle, headersize, SEEK_SET);
		handle = fhandle;
	}

	// init the decompression structures
//	strm = lzma_stream(LZMA_STREAM_INIT);
	lzma_ret ret = lzma_stream_decoder(&strm, UINT64_MAX, LZMA_CONCATENATED);

	if (ret != LZMA_OK) {
		const char *msg;
		switch (ret) {
		case LZMA_MEM_ERROR:
			msg = "Memory allocation failed";
			break;
		case LZMA_OPTIONS_ERROR:
			msg = "Unsupported decompressor flags";
			break;
		default:
			msg = "Unknown error, possibly a bug";
			break;
		}
		fprintf(stderr, "Error: failed to initialise lzma (xz) decoder: %s (%u)\n", msg, ret);
		return false;
	}

	// connect buffers
	strm.avail_in = headersize;
	strm.next_in = in;
	strm.avail_out = LZMAINPUT_OUTCHUNK;
	strm.next_out = out;

	// load in the header if needed
	if (headersize > 0) {
		memcpy(in, headerbytes, headersize);
		state = LZI_INFLATE;
	}
	else
	    state = LZI_READ;

	return true;
}

/**
 * Try to reset io source pointer to beginning (if makes sense)
 * @return bool, true if successful
 */
bool LZMAInput::reset() {
	//TODO: implement reset function
	return false;
}

/**
 * Searches plain buffer for new line (and appends to current line with bytes
 * up to EOL (or end of buffer)).
 *
 * @param line, (byref) std::string to at add bytes too
 * @return short, 0 = all bytes consumed (no EOL), 1 = EOL found
 */
short LZMAInput::consumePlainBuffer(std::string &line) {

	bool foundEOL = false;
	char buf[LZMAINPUT_OUTCHUNK+1];
	unsigned start = outProcessedPos;
	unsigned have = LZMAINPUT_OUTCHUNK - strm.avail_out;

	// search for EOL
	for (; outProcessedPos < have; ++outProcessedPos) {
		if (out[outProcessedPos] == '\n') {
			foundEOL = true;
			break;
		}
	}

	// append the required bytes
	if (foundEOL) {
		unsignedToSigned((char*)&buf, &out[start], outProcessedPos - start);
		buf[outProcessedPos - start] = '\0';
		outProcessedPos++; // consume EOL
	} else {
		unsignedToSigned((char*)&buf, &out[start], have - start);
		buf[outProcessedPos - start] = '\0';
	}
	line.append((char*)&buf);

	return foundEOL? 1: 0;
}

/**
 * Inflates raw buffer
 *
 * @return short, -1 = error, 0 = no output, 1 = output
 */
short LZMAInput::inflateBuffer() {
	lzma_ret ret;

	// clear output buffer
	strm.avail_out = LZMAINPUT_OUTCHUNK;
	strm.next_out = out;
	outProcessedPos = 0;

	// inflate data
	ret = lzma_code(&strm, action);

	// check for more lzma data streams
//	if (ret == BZ_STREAM_END) {
//		if (strm.avail_in > 0) {
//#ifdef DEBUG
//			std::cerr << "inflateBuffer(): Re-initialising as more bytes found after libbz2 stream end" << std::endl;
//#endif
//
//			// store buffer for old buffer
//			unsigned av_in = strm.avail_in;
//			char* nx_in = strm.next_in;
//
//			// initialise
//			strm.bzalloc = NULL;
//			strm.bzfree = NULL;
//			strm.opaque = NULL;
//			strm.avail_in = 0;
//			strm.next_in = NULL;
//			ret = BZ2_bzDecompressInit(&strm, BZVERBOSE, 0);
//			if (ret != BZ_OK) {
//				std::cerr << "Error: failed to initialise libbz2 (bz2) data structure (for 2nd usage)" << std::endl;
//				return -1;
//			}
//
//			// re-connect old buffers
//			strm.avail_in = av_in;
//			strm.next_in = nx_in;
//		}
//	}

	// catch error states
	//else
	if (ret != LZMA_OK && ret != LZMA_STREAM_END) {

		// cleanup
		lzma_end(&strm);

		// make error message
		const char *msg;
		switch (ret) {
		case LZMA_MEM_ERROR:
			msg = "Memory allocation failed";
			break;

		case LZMA_FORMAT_ERROR:
			msg = "The input is not in the .xz format";
			break;

		case LZMA_OPTIONS_ERROR:
			msg = "Unsupported compression options";
			break;

		case LZMA_DATA_ERROR:
			msg = "Compressed file is corrupt";
			break;

		case LZMA_BUF_ERROR:
			msg = "Compressed file is truncated or otherwise corrupt";
			break;

		default:
			msg = "Unknown error, possibly a bug";
			break;
		}

		std::cerr << "Error: [" << ret << "] LZMA says '" << msg << "'" << std::endl;
		return -1;
	}

	return (LZMAINPUT_OUTCHUNK - strm.avail_out)==0? 0: 1;
}

/**
 * Reads a buffer full (max) of data from compressed instream and connects it
 * to the zlib data structure ready for inflation.
 *
 * @return short, -1=error, 0=exhausted, 1=data read
 */
short LZMAInput::readRawData() {

	strm.avail_in = fread(in, 1, LZMAINPUT_INCHUNK, handle);
	if (ferror(handle)) {
		lzma_end(&strm);
		return -1;
	}
	if (strm.avail_in == 0) { //XXX: maybe this should be feof(handle) instead
		return 0;
	}
	strm.next_in = in;

	return 1;
}

/**
 * Reads a line from the Input source.
 *
 * Uses a state-machine which is described in the following document:
 * @see https://docs.google.com/document/d/1JLhEHQY_jB2Gbm_DmNgPiuwOkUk9_DGlAE5ABIhPC68#heading=h.8z5i4f4je1qw
 *
 * @return std::string, the line of text
 */
std::string LZMAInput::readLine() {
    std::string line;
	short ret;

    // State machine main loop
    while (true) {
		switch (state) {
		case LZI_READ:
			ret = readRawData();
			if (ret < 0) {
				state = LZI_ERROR;
			} else if (ret == 1)
				state = LZI_INFLATE;
			else { // IO Exhausted
				if (line.length() > 0)
					state = LZI_EXHAUST;
				else
					state = LZI_FINISH;
				return line;
			}
			break;
		case LZI_INFLATE:
			if (strm.avail_in == 0)
				state = LZI_READ;
			else {
				ret = inflateBuffer();
				if (ret < 0) {
					state = LZI_ERROR;
				} else if (ret == 1) // more output
					state = LZI_CONSUME;
				//else stay here
			}
			break;
		case LZI_CONSUME:
			ret = consumePlainBuffer(line);
			if (ret < 0)
				state = LZI_ERROR;
			else if (ret == 1)	// found a line
				return line;
			else				// nothing left
				state = LZI_INFLATE;
			break;
		case LZI_EXHAUST:
			state=LZI_FINISH;
			return "";
		case LZI_FINISH:
			state=LZI_ERROR;
			return "";
		default: // ERROR (sink state)
	    	std::cerr << "Error: Instream not open, already exhausted OR in error state" << std::endl;
			return "";
		}
    }

    return "";
}

/**
 * Indicates if the end of file has been reached
 */
bool LZMAInput::eof() {
	return state == LZI_FINISH || state == LZI_ERROR;
}













