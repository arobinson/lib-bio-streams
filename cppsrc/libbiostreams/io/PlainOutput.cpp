/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * PlainOutput.cpp
 *
 *  Created on: 26/09/2014
 *      Author: arobinson
 */

#include <iostream>

#include "PlainOutput.h"

PlainOutput::PlainOutput(std::string filename)
		: OutputBase(filename)
{
	ohandle = NULL;
}

PlainOutput::~PlainOutput() {
    if (ofhandle.is_open())
        ofhandle.close();
}

/**
 * Try to open the io source
 * @return bool, true if successful
 */
bool PlainOutput::open() {

	if (ohandle != NULL)
		return true;

    if (filename == "-") {
        ohandle = &std::cout;
        return true;
    } else if (filename == "=") {
        ohandle = &std::cerr;
        return true;
    } else {
        ofhandle.open(filename.c_str());
        if (ofhandle.is_open()) {
            ohandle = &ofhandle;
            return true;
        }
    }
    return false;
}

/**
 * Write a null-term character array to output stream
 */
bool PlainOutput::write(const char *c) {
	if (ohandle !=  NULL) {
		*ohandle << c;
		return true;
	}

	return false;
}

/**
 * Write character to output stream
 */
bool PlainOutput::write(const char &c) {
	if (ohandle !=  NULL) {
		*ohandle << c;
		return true;
	}

	return false;
}

/**
 * Write c++ string to output stream
 */
bool PlainOutput::write(const std::string buf) {
	if (ohandle !=  NULL) {
		*ohandle << buf;
		return true;
	}

	return false;
}

/**
 * Write c++ string to output stream and follow with a std::endl;
 */
bool PlainOutput::writeln(const std::string buf) {
	if (ohandle !=  NULL) {
		*ohandle << buf << std::endl;
		return true;
	}
	return false;
}

/**
 * Write unsigned long int to output stream
 */
bool PlainOutput::write(const unsigned long &ul) {
	if (ohandle !=  NULL) {
		*ohandle << ul;
		return true;
	}
	return false;
}

/**
 * Write long to output stream
 */
bool PlainOutput::write(const long &l) {
	if (ohandle !=  NULL) {
		*ohandle << l;
		return true;
	}
	return false;
}

/**
 * Write double to output stream
 */
bool PlainOutput::write(const double &d) {
	if (ohandle !=  NULL) {
		*ohandle << d;
		return true;
	}
	return false;
}

/**
 * Writes a newline and flushes buffer
 */
bool PlainOutput::endl() {
	if (ohandle !=  NULL) {
		*ohandle << std::endl;
		return true;
	}
	return false;
}

