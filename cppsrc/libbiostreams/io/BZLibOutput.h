/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * BZLibOutput.h
 *
 *  Created on: 27 Oct 2014
 *      Author: arobinson
 */

#ifndef LIB_BIO_STREAMS_CPPSRC_IO_BZLIBOUTPUT_H_
#define LIB_BIO_STREAMS_CPPSRC_IO_BZLIBOUTPUT_H_

#include <bzlib.h>

#include "OutputBase.h"

#define BZLIBOUTPUT_OUTCHUNK 8*1024

class BZLibOutput: public OutputBase {
protected:

	FILE *handle;					///< copy of a file handle to read from (i.e. don't close it as it could be stdin)
	FILE *fhandle;					///< file handle if a file was opened (i.e. close if not null as its a file we opened)
	BZFILE *bzFile;					///< bzlib state structure
	char out[BZLIBOUTPUT_OUTCHUNK];	///< output buffer for bzlib deflation

	char eol[3];					///< the end of line character(s) to use.
	unsigned short eollen;			///< the length of EOL characters
public:
	BZLibOutput(std::string filename);
	virtual ~BZLibOutput();

    /**
     * Try to open the io source
     * @return bool, true if successful
     */
    virtual bool open();

    /**
     * Write null-term character array to output stream
     */
    virtual bool write(const char *c);

    /**
     * Write character to output stream
     */
    virtual bool write(const char &c);

    /**
     * Write c++ string to output stream
     */
    virtual bool write(const std::string buf);

    /**
     * Write c++ string to output stream and follow with a std::endl;
     */
    virtual bool writeln(const std::string buf);

    /**
     * Write unsigned long int to output stream
     */
    virtual bool write(const unsigned long &ul);

    /**
     * Write long to output stream
     */
    virtual bool write(const long &l);

    /**
     * Write double to output stream
     */
    virtual bool write(const double &d);

    /**
     * Writes a newline but does not flush the buffer because that can lead to poor
     * compression rates.
     */
    virtual bool endl();
};

#endif /* LIB_BIO_STREAMS_CPPSRC_IO_BZLIBOUTPUT_H_ */
