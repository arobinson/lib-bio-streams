/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 03/09/2014
 *      Author: arobinson
 */

#include <cstdlib>
#include <cstring>
#include <iostream>
#include <stdio.h>
#include <string>
#include <unistd.h>
#include <zlib.h>

#include "ZLibInput.h"

ZLibInput::ZLibInput(std::string filename, const unsigned char* headerbytes, unsigned int headersize)
    :InputBase(filename, headerbytes, headersize)
{
	handle = NULL;
	fhandle = NULL;
	outProcessedPos = 0;

    state = ZLI_ERROR;
}

ZLibInput::~ZLibInput() {

	// close (and free) zlib data structure
	(void)inflateEnd(&strm);

	// close file
	if (fhandle != NULL)
		fclose(fhandle);
}

/**
 * Try to open the io source
 * @return bool, true if successful
 */
bool ZLibInput::open() {

	int ret;

	if (handle != NULL)
		return true;

	// open the file handle
	if (filename == "-") {
		handle = stdin;
	} else {
		fhandle = fopen(filename.c_str(), "r");
		if (fhandle == NULL) {
			std::cerr << "Error: failed to open input file '" << filename.c_str() << "'" << std::endl;
			return false;
		}
		fseek(fhandle, headersize, SEEK_SET);
		handle = fhandle;
	}

	// init the decompression structures
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	strm.avail_in = 0;
	strm.next_in = Z_NULL;
	ret = inflateInit2(&strm, 15+16);
	if (ret != Z_OK) {
		std::cerr << "Error: failed to initialise zlib (gz) data structure" << std::endl;
		return false;
	}

	// connect buffers
	strm.avail_in = headersize;
	strm.next_in = in;
	strm.avail_out = ZLIBINPUT_OUTCHUNK;
	strm.next_out = out;

	// load in the header if needed
	if (headersize > 0) {
		memcpy(in, headerbytes, headersize);
		state = ZLI_INFLATE;
	}
	else
	    state = ZLI_READ;

	return true;
}

/**
 * Try to reset io source pointer to beginning (if makes sense)
 * @return bool, true if successful
 */
bool ZLibInput::reset() {
	//TODO: implement reset function
	return false;
}

/**
 * Copies len characters from src to dest arrays.  You must ensure that both
 * src and dest arrays are of suitable length.
 */
char* ZLibInput::unsignedToSigned(char* dest, const unsigned char* src, size_t len) {
	for (size_t i = 0; i < len; ++i)
		dest[i] = src[i];
	return dest;
}

/**
 * Searches plain buffer for new line (and appends to current line with bytes
 * up to EOL (or end of buffer)).
 *
 * @param line, (byref) std::string to at add bytes too
 * @return short, 0 = all bytes consumed (no EOL), 1 = EOL found
 */
short ZLibInput::consumePlainBuffer(std::string &line) {

	bool foundEOL = false;
	char buf[ZLIBINPUT_OUTCHUNK+1];
	unsigned start = outProcessedPos;
	unsigned have = ZLIBINPUT_OUTCHUNK - strm.avail_out;

	// search for EOL
	for (; outProcessedPos < have; ++outProcessedPos) {
		if (out[outProcessedPos] == '\n') {
			foundEOL = true;
			break;
		}
	}

	// append the required bytes
	if (foundEOL) {
		unsignedToSigned((char*)&buf, &out[start], outProcessedPos - start);
		buf[outProcessedPos - start] = '\0';
		outProcessedPos++; // consume EOL
	} else {
		unsignedToSigned((char*)&buf, &out[start], have - start);
		buf[outProcessedPos - start] = '\0';
	}
	line.append((char*)&buf);

	return foundEOL? 1: 0;
}

/**
 * Inflates raw buffer
 *
 * @return short, -1 = error, 0 = no output, 1 = output
 */
short ZLibInput::inflateBuffer() {
	int ret;

	// clear output buffer
	strm.avail_out = ZLIBINPUT_OUTCHUNK;
	strm.next_out = out;
	outProcessedPos = 0;

	// inflate data
	ret = inflate(&strm, Z_NO_FLUSH);

	// check for more libz data streams
	if (ret == Z_STREAM_END) {
		if (strm.avail_in > 0) {
#ifdef DEBUG
			std::cerr << "inflateBuffer(): Re-initialising as more bytes found after libz stream end" << std::endl;
#endif

			// store buffer for old buffer
			unsigned av_in = strm.avail_in;
			unsigned char* nx_in = strm.next_in;

			// initialise
			strm.zalloc = Z_NULL;
			strm.zfree = Z_NULL;
			strm.opaque = Z_NULL;
			strm.avail_in = 0;
			strm.next_in = Z_NULL;
			ret = inflateInit2(&strm, 15+16);
			if (ret != Z_OK) {
				std::cerr << "Error: failed to initialise zlib (gz) data structure (for 2nd usage)" << std::endl;
				return -1;
			}

			// re-connect old buffers
			strm.avail_in = av_in;
			strm.next_in = nx_in;
		}
	}

	// catch error states
	if(ret == Z_STREAM_ERROR) { /* state clobbered */
		std::cerr << "inflateBuffer(): state clobbered" << std::endl;
		return -1;
	}
	switch (ret) {
	case Z_NEED_DICT:
		ret = Z_DATA_ERROR;     /* and fall through */
	case Z_DATA_ERROR:
	case Z_MEM_ERROR:
		std::cerr << "Error: ZLIB says '" << strm.msg << "'" << std::endl;
		(void)inflateEnd(&strm);
		return -1;
	}

	return (ZLIBINPUT_OUTCHUNK - strm.avail_out)==0? 0: 1;
}

/**
 * Reads a buffer full (max) of data from compressed instream and connects it
 * to the zlib data structure ready for inflation.
 *
 * @return short, -1=error, 0=exhausted, 1=data read
 */
short ZLibInput::readRawData() {

	strm.avail_in = fread(in, 1, ZLIBINPUT_INCHUNK, handle);
	if (ferror(handle)) {
		(void)inflateEnd(&strm);
		return -1;
	}
	if (strm.avail_in == 0) {
		return 0;
	}
	strm.next_in = in;

	return 1;
}

/**
 * Reads a line from the Input source.
 *
 * Uses a state-machine which is described in the following document:
 * @see https://docs.google.com/document/d/1JLhEHQY_jB2Gbm_DmNgPiuwOkUk9_DGlAE5ABIhPC68#heading=h.8z5i4f4je1qw
 *
 * @return std::string, the line of text
 */
std::string ZLibInput::readLine() {
    std::string line;
	short ret;

    // State machine main loop
    while (true) {
		switch (state) {
		case ZLI_READ:
			ret = readRawData();
			if (ret < 0) {
				state = ZLI_ERROR;
			} else if (ret == 1)
				state = ZLI_INFLATE;
			else { // IO Exhausted
				if (line.length() > 0)
					state = ZLI_EXHAUST;
				else
					state = ZLI_FINISH;
				return line;
			}
			break;
		case ZLI_INFLATE:
			if (strm.avail_in == 0)
				state = ZLI_READ;
			else {
				ret = inflateBuffer();
				if (ret < 0) {
					state = ZLI_ERROR;
				} else if (ret == 1) // more output
					state = ZLI_CONSUME;
				//else stay here
			}
			break;
		case ZLI_CONSUME:
			ret = consumePlainBuffer(line);
			if (ret < 0)
				state = ZLI_ERROR;
			else if (ret == 1)	// found a line
				return line;
			else				// nothing left
				state = ZLI_INFLATE;
			break;
		case ZLI_EXHAUST:
			state=ZLI_FINISH;
			return "";
		case ZLI_FINISH:
			state=ZLI_ERROR;
			return "";
		default: // ERROR (sink state)
	    	std::cerr << "Error: Instream not open, already exhausted OR in error state" << std::endl;
			return "";
		}
    }

    return "";
}

/**
 * Indicates if the end of file has been reached
 */
bool ZLibInput::eof() {
	return state == ZLI_FINISH || state == ZLI_ERROR;
}

