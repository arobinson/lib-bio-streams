/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 03/09/2014
 *      Author: arobinson
 */

#ifndef ZLIBINPUT_H_
#define ZLIBINPUT_H_

#include <stdio.h>
#include <zlib.h>

#include "InputBase.h"

// new
#define ZLIBINPUT_INCHUNK 256*1024
//#define ZLIBINPUT_INCHUNK 78
#define ZLIBINPUT_OUTCHUNK ZLIBINPUT_INCHUNK*8

// old
//#define READBUFFERSIZE 2049
//#define READDATASIZE 2048
//#define HEADERBUFFERSIZE 16

enum ZLibInputStateType {
	ZLI_READ,
	ZLI_INFLATE,
	ZLI_CONSUME,
	ZLI_EXHAUST,
	ZLI_FINISH,
	ZLI_ERROR,
};

class ZLibInput: public InputBase {
protected:

	// newnew
	FILE *handle;							///< copy of a file handle to read from (i.e. don't close it as it could be stdin)
	FILE *fhandle;							///< file handle if a file was opened (i.e. close if not null as its a file we opened)
	z_stream strm;							///< zlib state structure
	unsigned char in[ZLIBINPUT_INCHUNK];	///< input buffer for zlib inflation
	unsigned char out[ZLIBINPUT_OUTCHUNK];	///< output buffer for zlib inflation
	unsigned int outProcessedPos;			///< position within 'out' that has been consumed

	ZLibInputStateType state;				///< state storage for readline state machine.

	/**
	 * Copies len characters from src to dest arrays.  You must ensure that both
	 * src and dest arrays are of suitable length.
	 */
	char* unsignedToSigned(char* dest, const unsigned char* src, size_t len);

    /**
     * Searches plain buffer for new line (and appends to current line with bytes
     * up to EOL (or end of buffer)).
     *
     * @param line, (byref) std::string to at add bytes too
     * @return short, 0 = all bytes consumed (no EOL), 1 = EOL found
     */
    short consumePlainBuffer(std::string &line);

    /**
     * Inflates raw buffer
     *
     * @return short, -1 = error, 0 = buffer full (some bytes might be available), 1 = All bytes inflated
     */
    short inflateBuffer();

    /**
     * Reads a buffer full (max) of data from compressed instream and connects it
     * to the zlib data structure read for inflation.
     *
     * @return short, -1=error, 0=exhausted, 1=data read
     */
    short readRawData();

public:
    ZLibInput(std::string filename, const unsigned char* headerbytes, unsigned int headersize);
    virtual ~ZLibInput();

    /**
     * Try to open the io source
     * @return bool, true if successful
     */
    virtual bool open();

    /**
     * Try to reset io source pointer to beginning (if makes sense)
     * @return bool, true if successful
     */
    virtual bool reset();

    /**
     * Reads a line from the Input source
	 *
	 * Uses a state-machine which is described in the following document
	 * @see https://docs.google.com/document/d/1JLhEHQY_jB2Gbm_DmNgPiuwOkUk9_DGlAE5ABIhPC68
	 *
     * @return std::string, the line of text
     */
    virtual std::string readLine();

    /**
     * Indicates if the end of file has been reached (or Error)
     */
    virtual bool eof();
};

#endif /* ZLIBINPUT_H_ */
