/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * LZMAOutput.cpp
 *
 *  Created on: 28 Oct 2014
 *      Author: arobinson
 */

#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <cstdio>

#include <sstream>

#include "LZMAOutput.h"

LZMAOutput::LZMAOutput(std::string filename) : OutputBase(filename) {
	handle = NULL;
	fhandle = NULL;
	action = LZMA_RUN;
	lzma_stream tmp = LZMA_STREAM_INIT;
	strm = tmp;

	// learn the eol char(s) for this system
	std::stringstream s;
	s << std::endl;
	const char* cs = s.str().c_str();
	for (size_t i = 0; i < 3 && cs[i] != '\0'; ++i)
		eol[i] = cs[i];
	eol[2] = '\0';
	eollen = s.str().size();

}

LZMAOutput::~LZMAOutput() {

//	std::cerr << "~LZMAOutput(char*): flushing." << std::endl;

	// flush buffer
	action = LZMA_FINISH;
	writeBuffer(in, 0);

	// cleanup stream
	lzma_end(&strm);

	// close file (if needed)
	if (fhandle != NULL) {
		fclose(fhandle);
		fhandle = NULL;
	}
}



/**
 * Try to open the io source
 * @return bool, true if successful
 */
bool LZMAOutput::open() {

	if (handle != NULL)
		return true;

	// open the file handle
	if (filename == "-") {
		handle = stdout;
	} else if (filename == "=") {
		handle = stderr;
	} else {
		fhandle = fopen(filename.c_str(), "w");
		if (fhandle == NULL) {
			std::cerr << "Error: failed to open output file '" << filename.c_str() << "'" << std::endl;
			return false;
		}
		handle = fhandle;
	}

	// init the decompression structures
	uint32_t preset = 6; // 6 = xz util default value TODO: allow user to edit the 'preset' value
	lzma_ret ret = lzma_easy_encoder(&strm, preset, LZMA_CHECK_CRC64);

	// catch errors
	if (ret != LZMA_OK) {
		const char *msg;
		switch (ret) {
		case LZMA_MEM_ERROR:
			msg = "Memory allocation failed";
			break;

		case LZMA_OPTIONS_ERROR:
			msg = "Specified preset is not supported";
			break;

		case LZMA_UNSUPPORTED_CHECK:
			msg = "Specified integrity check is not supported";
			break;

		default:
			msg = "Unknown error, possibly a bug";
			break;
		}
		std::cerr << "Error: failed to open lzma data structure.  LZMA says '" << msg << "' (" << ret << ")" << std::endl;
		return false;
	}

    strm.next_in = NULL;
    strm.avail_in = 0;
    strm.next_out = out;
    strm.avail_out = LZMAOUTPUT_OUTCHUNK;

	return true;
}

/**
 * Copy n bytes from char* array to uint8_t* array.
 * Note: len(dst) >= len(src) and both must be <= n
 *
 * @param dst uint8_t, the destination array
 * @param src const char*, the source array (bytes to be copied)
 * @param n size_t, the number of bytes to copy
 * @return uint8_t, the pointer passed as dst
 */
uint8_t *LZMAOutput::charToUint8(uint8_t* dst, const char* src, size_t n) {

	for (size_t i = 0; i < n; ++i)
		dst[i] = (uint8_t)src[i];

	return dst;
}

/**
 * Copy at most n-1 bytes (leaving space for '\0' from char* array to uint8_t*
 * array OR after reaching first '\0'.
 *
 * Note: len(dst) >= len(src) and both must be <= n
 *
 * @param dst uint8_t, the destination array
 * @param src const char*, the source array (bytes to be copied)
 * @param n size_t, the number of bytes to copy
 * @return size_t, the length of the string (exc '\0')
 */
size_t LZMAOutput::charToUint8_ncpy(uint8_t* dst, const char* src, size_t n) {

	// copy bytes
	size_t i;
	for (i = 0; i < (n-1) && src[i] != '\0'; ++i)
		dst[i] = (uint8_t)src[i];

	// null term result array
	dst[i] = '\0';

	return i;
}

/**
 * Writes the given buffer to the output filehandle in compressed form
 */
bool LZMAOutput::writeBuffer(uint8_t *buffer, size_t nbytesi) {

//	std::cerr << "writeBuffer(char*): '" /*<< buffer*/ << "' (" << nbytesi << ")" << std::endl;

    strm.next_in = buffer;
    strm.avail_in = nbytesi;

	// loop until input is consumed (or error exits early)
	while (strm.avail_in > 0 || action == LZMA_FINISH) {

		// compress the inbuffer
		lzma_ret ret = lzma_code(&strm, action);
//		std::cerr << "writeBuffer(char*):  compress return code: '" << ret << "'" << std::endl;

		// write the buffer
		if (strm.avail_out == 0 || ret == LZMA_STREAM_END) {
			size_t nbyteso = LZMAOUTPUT_OUTCHUNK - strm.avail_out;
//			std::cerr << "writeBuffer(char*):  writing '" << nbyteso << "' compressed bytes" << std::endl;
			if (fwrite(out, 1, nbyteso, handle) != nbyteso) {
				std::cerr << "Error: writing output file (" << strerror(errno) << ")" << std::endl;
				return false;
			}

			// clear output buffer
		    strm.next_out = out;
		    strm.avail_out = LZMAOUTPUT_OUTCHUNK;
		}
		if (ret == LZMA_STREAM_END)
			return true;

		if (ret != LZMA_OK) {
			const char *msg;
			switch (ret) {
			case LZMA_MEM_ERROR:
				msg = "Memory allocation failed";
				break;

			case LZMA_DATA_ERROR:
				msg = "File size limits exceeded"; // 8 EiB at time of writing
				break;

			default:
				msg = "Unknown error, possibly a bug";
				break;
			}
			std::cerr << "Error: failed to compress data.  LZMA says '" << msg << "' (" << ret << ")" << std::endl;
			return false;
		}
	}
	return true;
}

/**
 * Write a null-term c-string to output stream
 */
bool LZMAOutput::write(const char *str) {

	size_t nbytes = 0;
	size_t nconsumed = 0;

	while (true) {
		nbytes = charToUint8_ncpy(in, &str[nconsumed], LZMAOUTPUT_INCHUNK);
//		std::cerr << "write(char*): '" << in << "' (" << nbytes << ")" << std::endl;

		if (nbytes > 0) {

			// write the buffer
			if (!writeBuffer(in, nbytes))
				return false;

			nconsumed += nbytes;
		} else if (nbytes == 0)
			return true;
		else
			return false;
	}
}

/**
 * Write a character to output stream
 */
bool LZMAOutput::write(const char &c) {

	uint8_t b = c;

	return writeBuffer(&b, 1);
}

/**
 * Write c++ string to output stream
 */
bool LZMAOutput::write(const std::string buf) {

	return write(buf.c_str());
}

/**
 * Write c++ string to output stream and follow with a std::endl;
 */
bool LZMAOutput::writeln(const std::string buf) {

	if (write(buf.c_str())) {
		return endl();
	}
	return false;
}

/**
 * Write unsigned long int to output stream
 */
bool LZMAOutput::write(const unsigned long &ul) {

	char buf[50];
	int bytes = sprintf(buf, "%lu", ul);;
	if (bytes > 0) {
		return write((const char*) &buf);
	}
	return false;
}

/**
 * Write long to output stream
 */
bool LZMAOutput::write(const long &l) {

	char buf[50];
	int bytes = sprintf(buf, "%li", l);
	if (bytes > 0) {
		return write((const char*) &buf);
	}
	return false;
}

/**
 * Write double to output stream
 */
bool LZMAOutput::write(const double &d) {

	char buf[50];
	int bytes = sprintf(buf, "%f", d);
	if (bytes > 0) {
		return write((const char*) &buf);
	}
	return false;
}

/**
 * Writes a newline but does not flush the buffer because that can lead to poor
 * compression rates.
 */
bool LZMAOutput::endl() {
	return writeBuffer(eol, eollen);
}

