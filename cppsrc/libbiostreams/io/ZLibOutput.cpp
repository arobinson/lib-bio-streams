/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * ZLibOutput.cpp
 *
 *  Created on: 27 Oct 2014
 *      Author: arobinson
 */

#include <iostream>
#include <sstream>

#include <cstdio>
#include <cstring>

#include "ZLibOutput.h"

ZLibOutput::ZLibOutput(std::string filename) : OutputBase(filename) {
	handle = NULL;
	fhandle = NULL;

	// learn the eol char(s) for this system
	std::stringstream s;
	s << std::endl;
	const char* cs = s.str().c_str();
	for (size_t i = 0; i < 3 && cs[i] != '\0'; ++i)
		eol[i] = cs[i];
	eol[2] = '\0';
	eollen = s.str().size();
}

ZLibOutput::~ZLibOutput() {

	// finish off zlib stream
	int ret, have;
	do {
		strm.avail_out = ZLIBOUTPUT_OUTCHUNK;
		strm.next_out = out;
		ret = deflate(&strm, Z_FINISH);
		have = ZLIBOUTPUT_OUTCHUNK - strm.avail_out;
		if (fwrite(out, 1, have, handle) != have || ferror(handle)) {
			std::cerr << "Failed to write compressed data" << std::endl;
		}
	} while (strm.avail_out == 0);

	strm.avail_in = 0;
	strm.next_in = NULL;

	// cleanup
    (void)deflateEnd(&strm);

    // close files
	if (fhandle != NULL)
		fclose(fhandle);
	fhandle = NULL;
}

/**
 * Try to open the io source
 * @return bool, true if successful
 */
bool ZLibOutput::open() {

	int ret;
	int level = Z_DEFAULT_COMPRESSION;

	if (handle != NULL)
		return true;

	// open the file handle
	if (filename == "-") {
		handle = stdout;
	} else if (filename == "=") {
		handle = stderr;
	} else {
		fhandle = fopen(filename.c_str(), "wb");
		if (fhandle == NULL) {
			std::cerr << "Error: failed to open output file '" << filename.c_str() << "'" << std::endl;
			return false;
		}
		handle = fhandle;
	}

	// init the decompression structures
	strm.zalloc = Z_NULL;
	strm.zfree = Z_NULL;
	strm.opaque = Z_NULL;
	ret = deflateInit2(&strm, level, Z_DEFLATED, 15+16, 8, Z_DEFAULT_STRATEGY);
	if (ret != Z_OK) {
		std::cerr << "Error: failed to open zlib data structure" << std::endl;
		return false;
	}

	return true;
}

/**
 * Writes a buffer full of bytes to the compressed output stream/file
 */
bool ZLibOutput::writeBuffer(unsigned char *buffer, size_t nbytes) {
	int ret, flush = Z_NO_FLUSH;
    unsigned have;

	strm.avail_in = nbytes;
	strm.next_in = buffer;

	do {
		// compress data
		strm.avail_out = ZLIBOUTPUT_OUTCHUNK;
		strm.next_out = out;
		ret = deflate(&strm, flush);    /* no bad return value */
		if(ret == Z_STREAM_ERROR) {
			std::cerr << "Error: zlib deflation error" << std::endl;
			return false;
		}

		// write compressed data (if needed)
		have = ZLIBOUTPUT_OUTCHUNK - strm.avail_out;
		if (fwrite(out, 1, have, handle) != have || ferror(handle)) {
		    std::cerr << "Failed to write compressed data" << std::endl;
			return false;
		}
	} while (strm.avail_out == 0); // while output buffer was full

	return true;
}

/**
 * Copy n bytes from char* array to unsigned char* array.
 * Note: len(dst) >= len(src) and both must be <= n
 *
 * @param dst unsigned char*, the destination array
 * @param src const char*, the source array (bytes to be copied)
 * @param n size_t, the number of bytes to copy
 * @return unsigned char*, the pointer passed as dst
 */
unsigned char *ZLibOutput::charToUchar(unsigned char* dst, const char* src, size_t n) {

	for (size_t i = 0; i < n; ++i)
		dst[i] = (unsigned char)src[i];

	return dst;
}

/**
 * Copy at most n-1 bytes (leaving space for '\0' from char* array to unsigned char*
 * array OR after reaching first '\0'.
 *
 * Note: len(dst) >= len(src) and both must be <= n
 *
 * @param dst unsigned char, the destination array
 * @param src const char*, the source array (bytes to be copied)
 * @param n size_t, the number of bytes to copy
 * @return size_t, the length of the string (exc '\0')
 */
size_t ZLibOutput::charToUchar_ncpy(unsigned char* dst, const char* src, size_t n) {

	// copy bytes
	size_t i;
	for (i = 0; i < (n-1) && src[i] != '\0'; ++i)
		dst[i] = (unsigned char)src[i];

	// null term result array
	dst[i] = '\0';

	return i;
}

/**
 * Write a null-term c-string to output stream
 */
bool ZLibOutput::write(const char *str) {

	size_t nbytes = 0;
	size_t nconsumed = 0;

	while (true) {
		nbytes = charToUchar_ncpy(in, &str[nconsumed], ZLIBOUTPUT_INCHUNK);

		if (nbytes > 0) {

			// write the buffer
			if (!writeBuffer(in, nbytes))
				return false;

			nconsumed += nbytes;
		} else if (nbytes == 0)
			return true;
		else
			return false;
	}
}

/**
 * Write a single character to output stream
 */
bool ZLibOutput::write(const char &c) {

	unsigned char b = c;

	return writeBuffer(&b, 1);
}

/**
 * Write string to output stream
 */
bool ZLibOutput::write(const std::string buf) {

	return write(buf.c_str());
}

/**
 * Write buffer to output stream and follow with a std::endl;
 */
bool ZLibOutput::writeln(const std::string buf) {

	if (write(buf.c_str())) {
		return endl();
	}
	return false;
}

/**
 * Write unsigned long int to output stream
 */
bool ZLibOutput::write(const unsigned long &ul) {

	char buf[50];
	int bytes = sprintf(buf, "%lu", ul);
	if (bytes > 0) {
		return write((const char*) &buf);
	}
	return false;
}

/**
 * Write long to output stream
 */
bool ZLibOutput::write(const long &l) {

	char buf[50];
	int bytes = sprintf(buf, "%li", l);
	if (bytes > 0) {
		return write((const char*) &buf);
	}
	return false;
}

/**
 * Write double to output stream
 */
bool ZLibOutput::write(const double &d) {

	char buf[50];
	int bytes = sprintf(buf, "%f", d);
	if (bytes > 0) {
		return write((const char*) &buf);
	}
	return false;
}

/**
 * Writes a newline but does not flush the buffer because that can lead to poor
 * compression rates.
 */
bool ZLibOutput::endl() {
	return writeBuffer(eol, eollen);
}


