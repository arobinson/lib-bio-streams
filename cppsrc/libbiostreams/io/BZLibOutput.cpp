/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * BZLibOutput.cpp
 *
 *  Created on: 27 Oct 2014
 *      Author: arobinson
 */

#include <sstream>
#include <cstdlib>
#include <cstring>

#include "BZLibOutput.h"

BZLibOutput::BZLibOutput(std::string filename) : OutputBase(filename) {
	handle = NULL;
	fhandle = NULL;
	bzFile = NULL;

	// learn the eol char(s) for this system
	std::stringstream s;
	s << std::endl;
	strncpy(eol, s.str().c_str(), 2);
	eol[2] = '\0';
	eollen = s.str().size();
}

BZLibOutput::~BZLibOutput() {
	int bzerror;

	if (bzFile != NULL) {
		BZ2_bzWriteClose(&bzerror, (void*)bzFile, 0, (unsigned int*)NULL, (unsigned int*)NULL);
		if (bzerror != BZ_OK)
			std::cerr << "BZWriteClose Error: " << bzerror << std::endl;
	}
	bzFile = NULL;

	if (handle != NULL)
		fflush(handle);
	handle = NULL;
	if (fhandle != NULL) {
		fclose(fhandle);
	}
	fhandle = NULL;
}



/**
 * Try to open the io source
 * @return bool, true if successful
 */
bool BZLibOutput::open() {
	int bzerror;

	int ret;

	if (handle != NULL)
		return true;

	// open the file handle
	if (filename == "-") {
		handle = stdout;
	} else if (filename == "=") {
		handle = stderr;
	} else {
		fhandle = fopen(filename.c_str(), "w");
		if (fhandle == NULL) {
			std::cerr << "Error: failed to open output file '" << filename.c_str() << "'" << std::endl;
			return false;
		}
		handle = fhandle;
	}

	// init the decompression structures
	bzFile = BZ2_bzWriteOpen(&bzerror, handle, 9, 0, 30);
	if (bzerror != BZ_OK) {
		BZ2_bzWriteClose(&bzerror, (void*)bzFile, 0, (unsigned int*)NULL, (unsigned int*)NULL);
		std::cerr << "Error: failed to open bzlib2 data structure" << std::endl;
		bzFile = NULL;
		return false;
	}

	return true;
}

/**
 * Write a null-term c-string to output stream
 */
bool BZLibOutput::write(const char *str) {
	int bzerror;
	if (str != NULL) {
		if (str[0] == '\0')
			return true;
		size_t len = strlen(str);
		BZ2_bzWrite(&bzerror, (void*)bzFile, (void*)str, len);
		if (bzerror != BZ_OK) {
			BZ2_bzWriteClose(&bzerror, (void*)bzFile, 0, (unsigned int*)NULL, (unsigned int*)NULL);
			bzFile = NULL;
			return false;
		}
		return true;
	}
	return false;
}

/**
 * Write a single character to output stream
 */
bool BZLibOutput::write(const char &c) {
	int bzerror;
	BZ2_bzWrite(&bzerror, (void*)bzFile, (void*)&c, 1);
	if (bzerror != BZ_OK) {
		BZ2_bzWriteClose(&bzerror, (void*)bzFile, 0, (unsigned int*)NULL, (unsigned int*)NULL);
		bzFile = NULL;
		return false;
	}
	return true;
}

/**
 * Write string to output stream
 */
bool BZLibOutput::write(const std::string buf) {
	int bzerror;
	BZ2_bzWrite(&bzerror, (void*)bzFile, (void*)buf.c_str(), buf.size());
	if (bzerror != BZ_OK) {
		BZ2_bzWriteClose(&bzerror, (void*)bzFile, 0, (unsigned int*)NULL, (unsigned int*)NULL);
		bzFile = NULL;
		return false;
	}
	return true;
}

/**
 * Write buffer to output stream and follow with a std::endl;
 */
bool BZLibOutput::writeln(const std::string buf) {
	int bzerror;
	BZ2_bzWrite(&bzerror, (void*)bzFile, (void*)buf.c_str(), buf.size());
	if (bzerror != BZ_OK) {
		BZ2_bzWriteClose(&bzerror, (void*)bzFile, 0, (unsigned int*)NULL, (unsigned int*)NULL);
		bzFile = NULL;
		return false;
	}
	return endl();
}

/**
 * Write unsigned long int to output stream
 */
bool BZLibOutput::write(const unsigned long &ul) {
	int bzerror;
	int bytes = sprintf(out, "%lu", ul);
	if (bytes > 0) {
		BZ2_bzWrite(&bzerror, (void*)bzFile, (void*)out, bytes);
		if (bzerror != BZ_OK) {
			BZ2_bzWriteClose(&bzerror, (void*)bzFile, 0, (unsigned int*)NULL, (unsigned int*)NULL);
			bzFile = NULL;
			return false;
		}
		return true;
	}
	return false;
}

/**
 * Write long to output stream
 */
bool BZLibOutput::write(const long &l) {
	int bzerror;
	int bytes = sprintf(out, "%li", l);
	if (bytes > 0) {
		BZ2_bzWrite(&bzerror, (void*)bzFile, (void*)out, bytes);
		if (bzerror != BZ_OK) {
			BZ2_bzWriteClose(&bzerror, (void*)bzFile, 0, (unsigned int*)NULL, (unsigned int*)NULL);
			bzFile = NULL;
			return false;
		}
		return true;
	}
	return false;
}

/**
 * Write double to output stream
 */
bool BZLibOutput::write(const double &d) {
	int bzerror;

	char buf[50];
	int bytes = sprintf(buf, "%f", d);
	if (bytes > 0) {
		BZ2_bzWrite(&bzerror, (void*)bzFile, (void*)buf, bytes);
		if (bzerror != BZ_OK) {
			BZ2_bzWriteClose(&bzerror, (void*)bzFile, 0, (unsigned int*)NULL, (unsigned int*)NULL);
			bzFile = NULL;
			return false;
		}
		return true;
	}
	return false;
}

/**
 * Writes a newline but does not flush the buffer because that can lead to poor
 * compression rates.
 */
bool BZLibOutput::endl() {
	int bzerror;
	BZ2_bzWrite ( &bzerror, bzFile, eol, eollen );
	if (bzerror != BZ_OK) {
		BZ2_bzWriteClose(&bzerror, (void*)bzFile, 0, (unsigned int*)NULL, (unsigned int*)NULL);
		bzFile = NULL;
		return false;
	}
	return true;
}
