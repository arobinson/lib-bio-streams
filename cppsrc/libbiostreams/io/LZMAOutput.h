/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * LZMAOutput.h
 *
 *  Created on: 28 Oct 2014
 *      Author: arobinson
 */

#ifndef LIB_BIO_STREAMS_CPPSRC_IO_LZMAOUTPUT_H_
#define LIB_BIO_STREAMS_CPPSRC_IO_LZMAOUTPUT_H_

#include <lzma.h>

#include "OutputBase.h"

#define LZMAOUTPUT_INCHUNK 8*1024
#define LZMAOUTPUT_OUTCHUNK LZMAOUTPUT_INCHUNK/8

class LZMAOutput: public OutputBase {
protected:

	FILE *handle;							///< copy of a file handle to read from (i.e. don't close it as it could be stdin)
	FILE *fhandle;							///< file handle if a file was opened (i.e. close if not null as its a file we opened)
	lzma_stream strm;						///< lzma state structure
	uint8_t in[LZMAOUTPUT_INCHUNK];			///< input buffer for lzma deflation
	uint8_t out[LZMAOUTPUT_OUTCHUNK];		///< output buffer for lzma deflation
	lzma_action action;						///< running state of compressor (change to LZMA_FINISH to flush)

	uint8_t eol[3];							///< the end of line character(s) to use.
	unsigned short eollen;					///< the length of EOL characters


	/**
	 * Copy n bytes from char* array to uint8_t* array.
	 * Note: len(dst) >= len(src) and both must be <= n
	 *
	 * @param dst uint8_t, the destination array
	 * @param src const char*, the source array (bytes to be copied)
	 * @param n size_t, the number of bytes to copy
	 * @return uint8_t, the pointer passed as dst
	 */
	uint8_t *charToUint8(uint8_t* dst, const char* src, size_t n);

	/**
	 * Copy at most n bytes from char* array to uint8_t* array OR after reaching first '\0'.
	 * Note: len(dst) >= len(src) and both must be <= n
	 *
	 * @param dst uint8_t, the destination array
	 * @param src const char*, the source array (bytes to be copied)
	 * @param n size_t, the number of bytes to copy
	 * @return size_t, the number of bytes copied (inc '\0')
	 */
	size_t charToUint8_ncpy(uint8_t* dst, const char* src, size_t n);

	/**
	 * Writes the given buffer to the output filehandle in compressed form
	 */
	bool writeBuffer(uint8_t *buffer, size_t bytes);

public:
	LZMAOutput(std::string filename);
	virtual ~LZMAOutput();

    /**
     * Try to open the io source
     * @return bool, true if successful
     */
    virtual bool open();

    /**
     * Write null-term character array to output stream
     */
    virtual bool write(const char *c);

    /**
     * Write character to output stream
     */
    virtual bool write(const char &c);

    /**
     * Write buffer to output stream
     */
    virtual bool write(const std::string buf);

    /**
     * Write buffer to output stream and follow with a std::endl;
     */
    virtual bool writeln(const std::string buf);

    /**
     * Write unsigned long int to output stream
     */
    virtual bool write(const unsigned long &ul);

    /**
     * Write long to output stream
     */
    virtual bool write(const long &l);

    /**
     * Write double to output stream
     */
    virtual bool write(const double &d);

    /**
     * Writes a newline but does not flush the buffer because that can lead to poor
     * compression rates.
     */
    virtual bool endl();
};

#endif /* LIB_BIO_STREAMS_CPPSRC_IO_LZMAOUTPUT_H_ */
