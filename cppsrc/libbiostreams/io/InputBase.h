/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 03/09/2014
 *      Author: arobinson
 */

#ifndef INPUTBASE_H_
#define INPUTBASE_H_

#define INPUTBASE_HEADER_BUFFER_SIZE 128

#include <cstring>
#include <string>

class InputBase {
protected:
    std::string filename;
    unsigned char headerbytes[INPUTBASE_HEADER_BUFFER_SIZE];
    unsigned int headersize;

public:
    InputBase(std::string filename, const unsigned char* headerbytes, unsigned int headersize);
    virtual ~InputBase();

    /**
     * Try to open the io source
     * @return bool, true if successful
     */
    virtual bool open() = 0;

    /**
     * Try to reset io source pointer to beginning (if makes sense)
     * @return bool, true if successful
     */
    virtual bool reset() = 0;

    /**
     * Reads a line from the Input source
     * @return std::string, the line of text
     */
    virtual std::string readLine() = 0;

    /**
     * Indicates if the end of file has been reached
     */
    virtual bool eof() = 0;

    /**
     * Getter for filename attribute
     */
    std::string getFilename() { return filename; }

    /**
     * Setter for header bytes
     */
    void setHeaderBytes(const unsigned char* headerbytes, unsigned int headersize)
    { memcpy(this->headerbytes, headerbytes, headersize); this->headersize = headersize; }
};

#endif /* INPUTBASE_H_ */
