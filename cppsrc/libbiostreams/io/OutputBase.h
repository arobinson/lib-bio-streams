/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * OutputBase.h
 *
 *  Created on: 26/09/2014
 *      Author: arobinson
 */

#ifndef OUTPUTBASE_H_
#define OUTPUTBASE_H_

#include <string>
#include <iostream>
#include <ostream>

class OutputBase {
protected:
    std::string filename;

public:

	OutputBase(std::string filename);
	virtual ~OutputBase();

    /**
     * Try to open the io source
     * @return bool, true if successful
     */
    virtual bool open() = 0;

    /**
     * Write null-term character array to output stream
     */
    virtual bool write(const char *c) = 0;

    /**
     * Write character to output stream
     */
    virtual bool write(const char &c) = 0;

    /**
     * Write c++ string to output stream
     */
    virtual bool write(const std::string buf) = 0;

    /**
     * Write c++ string to output stream and follow with a std::endl;
     */
    virtual bool writeln(const std::string buf) = 0;

    /**
     * Write unsigned long int to output stream
     */
    virtual bool write(const unsigned long &i) = 0;

    /**
     * Write long to output stream
     */
    virtual bool write(const long &i) = 0;

    /**
     * Write double to output stream
     */
    virtual bool write(const double &i) = 0;

    /**
     * Writes a newline and flushes buffer
     */
    virtual bool endl() = 0;

    /**
     * Getter for filename attribute
     */
    std::string getFilename() { return filename; }

    /// iostream functions ///
    friend OutputBase &operator<<(OutputBase &out, const std::string *c) {
    	out.write(*c);
//    	std::cout << "operator<< std::string*" << std::endl;
    	return out;
    }
    friend OutputBase &operator<<(OutputBase &out, const std::string &c) {
    	out.write(c);
//    	std::cout << "operator<< std::string&" << std::endl;
    	return out;
    }
    friend OutputBase &operator<<(OutputBase &out, const char *c) {
    	out.write(c);
//    	std::cout << "operator<< char*" << std::endl;
    	return out;
    }
    friend OutputBase &operator<<(OutputBase &out, const char &c) {
    	out.write(c);
//    	std::cout << "operator<< char&" << std::endl;
    	return out;
    }
    friend OutputBase &operator<<(OutputBase &out, const unsigned long &i) {
    	out.write(i);
//    	std::cout << "operator<< unsigned long" << std::endl;
    	return out;
    }
    friend OutputBase &operator<<(OutputBase &out, const unsigned int &i) {
    	long l = i;
    	out.write(l);
//    	std::cout << "operator<< unsigned int" << std::endl;
    	return out;
    }
    friend OutputBase &operator<<(OutputBase &out, const unsigned short &s) {
    	long l = s;
    	out.write(l);
//    	std::cout << "operator<< unsigned int" << std::endl;
    	return out;
    }
    friend OutputBase &operator<<(OutputBase &out, const long &i) {
    	out.write(i);
//    	std::cout << "operator<< long" << std::endl;
    	return out;
    }
    friend OutputBase &operator<<(OutputBase &out, const int &i) {
    	long l = i;
    	out.write(l);
//    	std::cout << "operator<< int" << std::endl;
    	return out;
    }
    friend OutputBase &operator<<(OutputBase &out, const short &s) {
    	long l = s;
    	out.write(l);
//    	std::cout << "operator<< short" << std::endl;
    	return out;
    }
//    friend OutputBase &operator<<(OutputBase &out, const long double &d) {
//    	out.write(d);
//    	std::cout << "operator<< long double" << std::endl;
//    	return out;
//    }
    friend OutputBase &operator<<(OutputBase &out, const double &d) {
    	out.write(d);
//    	std::cout << "operator<< double" << std::endl;
    	return out;
    }
    friend OutputBase &operator<<(OutputBase &out, const float &f) {
    	double d = f;
    	out.write(d);
//    	std::cout << "operator<< float" << std::endl;
    	return out;
    }

    // custom endl type
    typedef OutputBase& (*OutputBaseManipulator)(OutputBase&);
    OutputBase& operator<<(OutputBaseManipulator manip) {
		return manip(*this);
	}
    static OutputBase &endl(OutputBase &out) {
    	out.endl();
    	return out;
    }

    // std::endl type
    typedef std::basic_ostream<char, std::char_traits<char> > CoutType;
	typedef CoutType& (*StandardEndLine)(CoutType&);
	OutputBase& operator<<(StandardEndLine manip) {
		this->endl();
		return *this;
	}
};


#endif /* OUTPUTBASE_H_ */
