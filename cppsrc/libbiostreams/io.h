/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * io.h
 *
 *  Created on: 5 Nov 2014
 *      Author: arobinson
 */

#ifndef CPPSRC_IO_H_
#define CPPSRC_IO_H_

#include <libbiostreams/io/BufferedInput.h>

#ifdef LBS_BZLIB_FOUND
# include <libbiostreams/io/BZLibInput.h>
# include <libbiostreams/io/BZLibOutput.h>
#endif

#include <libbiostreams/io/EmptyInput.h>
#include <libbiostreams/io/InputBase.h>

#ifdef LBS_LZMA_FOUND
# include <libbiostreams/io/LZMAInput.h>
# include <libbiostreams/io/LZMAOutput.h>
#endif

#include <libbiostreams/io/OpenErrorInput.h>
#include <libbiostreams/io/OutputBase.h>
#include <libbiostreams/io/PlainInput.h>
#include <libbiostreams/io/PlainOutput.h>
#include <libbiostreams/io/UnsupportedInput.h>

#ifdef LBS_ZLIB_FOUND
# include <libbiostreams/io/ZLibInput.h>
# include <libbiostreams/io/ZLibOutput.h>
#endif

#endif /* CPPSRC_IO_H_ */
