/*
 * complement.h
 *
 *  Created on: 04/09/2014
 *      Author: arobinson
 */

#ifndef COMPLEMENT_H_
#define COMPLEMENT_H_

#include <string>

#include <libbiostreams/datatypes/Sequence.h>

/**
 * Returns the orig string in reverse order
 */
std::string reverse(std::string &orig);

/**
 * Returns the DNA complement of orig
 */
std::string complement(std::string &orig);

/**
 * Returns the reverse complement of orig
 */
std::string reverseComplement(std::string &orig);

/**
 * Performs an inplace reversal of orig
 */
void reverse(Sequence *orig);

/**
 * Performs an inplace complement of orig
 */
void complement(Sequence *orig);

/**
 * Performs an inplace reverse complement of orig
 */
void reverseComplement(Sequence *orig);


#endif /* COMPLEMENT_H_ */
