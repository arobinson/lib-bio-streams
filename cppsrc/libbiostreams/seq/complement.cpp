/*
 * complement.cpp
 *
 *  Created on: 04/09/2014
 *      Author: arobinson
 */

#include <sstream>

#include "complement.h"

const char* translate =  "NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNTNGNNNCNNNNNNNNNNNNANNNNNNNNNNNNTNGNNNCNNNNNNNNNNNNANNNNNNNNNNN";

/**
 * Returns the orig string in reverse order
 */
std::string reverse(std::string &orig) {
	return std::string(orig.rbegin(), orig.rend());
}

/**
 * Returns the DNA complement of orig
 */
std::string complement(std::string &orig) {
	std::stringstream out;
	for (int i = 0; i < orig.length(); i++) {
		out << translate[orig[i]&127];
	}
	return out.str();
}

/**
 * Returns the reverse complement of orig
 */
std::string reverseComplement(std::string &orig) {
	std::stringstream out;
	for (int i = orig.length() -1; i >= 0; i--) {
		out << translate[orig[i]&127];
	}
	return out.str();
}


/**
 * Performs an inplace reversal of orig
 */
void reverse(Sequence *orig) {
	orig->seq = reverse(orig->seq);
	orig->qual = reverse(orig->qual);
}

/**
 * Performs an inplace complement of orig
 */
void complement(Sequence *orig) {
	orig->seq = complement(orig->seq);
}

/**
 * Performs an inplace reverse complement of orig
 */
void reverseComplement(Sequence *orig) {
	orig->seq = reverseComplement(orig->seq);
	orig->qual = reverse(orig->qual);
}
