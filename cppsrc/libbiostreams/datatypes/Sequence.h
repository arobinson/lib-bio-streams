/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/* 
 *  Created on: 03/09/2014
 *      Author: arobinson
 */

#ifndef SEQUENCE_H_
#define SEQUENCE_H_

#include <string>
#include <ostream>

#include <libbiostreams/io/OutputBase.h>

#include "Record.h"

class Sequence : public Record {
public:
    Sequence();
    virtual ~Sequence();

    std::string id;
    std::string annotation;
    std::string seq;
    std::string qual;

    friend std::ostream &operator<<(std::ostream &out, Sequence &c) {
    	if (c.qual.length() == 0 && c.seq.length() != 0) {
            out << ">" << c.id;
            if (c.annotation.length() > 0)
            	out << " " << c.annotation;
            out << std::endl;
            out << c.seq << std::endl;
    	} else {
            out << "@" << c.id;
            if (c.annotation.length() > 0)
            	out << " " << c.annotation;
            out << std::endl;
            out << c.seq << std::endl;
            out << "+" << std::endl;
            out << c.qual << std::endl;
    	}
        return out;
    }
    friend std::ostream &operator<<(std::ostream &out, Sequence *c) {
    	if (c->qual.length() == 0 && c->seq.length() != 0) {
            out << ">" << c->id;
            if (c->annotation.length() > 0)
            	out << " " << c->annotation;
            out << std::endl;
            out << c->seq << std::endl;
    	} else {
            out << "@" << c->id;
            if (c->annotation.length() > 0)
            	out << " " << c->annotation;
            out << std::endl;
            out << c->seq << std::endl;
            out << "+" << std::endl;
            out << c->qual << std::endl;
    	}
        return out;
    }

    friend OutputBase &operator<<(OutputBase &out, Sequence &c) {
    	if (c.qual.length() == 0 && c.seq.length() != 0) {
            out << ">" << c.id;
            if (c.annotation.length() > 0)
            	out << " " << c.annotation;
            out << c.seq;
            out.endl();
    	} else {
            out << "@" << c.id;
            if (c.annotation.length() > 0)
            	out << " " << c.annotation;
            out.endl();
            out << c.seq;
            out.endl();
            out << "+";
            out.endl();
            out << c.qual;
            out.endl();
    	}
        return out;
    }
    friend OutputBase &operator<<(OutputBase &out, Sequence *c) {
    	if (c->qual.length() == 0 && c->seq.length() != 0) {
            out << ">" << c->id;
            if (c->annotation.length() > 0)
            	out << " " << c->annotation;
            out.endl();
            out << c->seq;
            out.endl();
    	} else {
            out << "@" << c->id;
            if (c->annotation.length() > 0)
            	out << " " << c->annotation;
            out.endl();
            out << c->seq;
            out.endl();
            out << "+";
            out.endl();
            out << c->qual;
            out.endl();
    	}
        return out;
    }
};


#endif /* SEQUENCE_H_ */
