/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * SequenceReader.h
 *
 *  Created on: 04/09/2014
 *      Author: arobinson
 */

#ifndef SEQUENCEREADER_H_
#define SEQUENCEREADER_H_

#include <string>

#include "Reader.h"

#include <libbiostreams/datatypes/Sequence.h>

enum SequenceType {
	AUTO = 0,
	FASTA = 1,
	FASTQ = 2,
	RAW = 4,
	RAWM = 8
};

class SequenceReader : public Reader {
protected:
	SequenceType *type;

	Sequence* readRaw();
	Sequence* readRawM();
	Sequence* readFastA();
	Sequence* readFastQ();
	Sequence* readAUTO(); ///< special read method that peaks at first character to determine type
	Sequence* (SequenceReader::*reader)();	///< pointer to active readXX() method.

	void extractHeader(std::string &header, Sequence* seq);

	std::string linebuffer;
	long seqcount;

public:
	SequenceReader(InputBase *input, SequenceType type = AUTO);
	virtual ~SequenceReader();

	/**
	 * Set which type is supported
	 */
	void setType(SequenceType type);

	/**
	 * Set which type is supported
	 */
	void setType(std::string type);

	/**
	 * Returns the next record from the reader.
	 *
	 * @return Record*, the next record OR NULL when no more available (or error)
	 */
	virtual Record* next();
};

#endif /* SEQUENCEREADER_H_ */
