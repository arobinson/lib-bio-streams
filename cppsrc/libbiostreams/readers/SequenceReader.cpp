/**
 * Copyright (c) 2014, Andrew Robinson, La Trobe University & Victorian Life Sciences Computation Initiative
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of La Trobe University, Victorian Life Sciences Computation Initiative nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

/*
 * SequenceReader.cpp
 *
 *  Created on: 04/09/2014
 *      Author: arobinson
 */

#include <cstdlib>
#include <sstream>
#include <iostream>

#include <libbiostreams/io/BufferedInput.h>

#include "SequenceReader.h"

SequenceReader::SequenceReader(InputBase *input, SequenceType type) : Reader(input) {
	setType(type);
	seqcount = 1;
	if (input != NULL)
		input->open();
}

SequenceReader::~SequenceReader() {
}



/**
 * Set which type is supported
 */
void SequenceReader::setType(SequenceType type) {
	if (input != NULL) {
		switch (type) {
		case RAW:
			reader = &SequenceReader::readRaw;
			break;
		case RAWM:
			reader = &SequenceReader::readRawM;
			break;
		case FASTA:
			reader = &SequenceReader::readFastA;
			break;
		case FASTQ:
			reader = &SequenceReader::readFastQ;
			break;
		default:
			reader = &SequenceReader::readAUTO;
			break;
		}
	} else {
		reader = NULL;
	}
}

/**
 * Set which type is supported
 */
void SequenceReader::setType(std::string type) {
	if (input != NULL) {
		if (type.compare("RAW") == 0)
			reader = &SequenceReader::readRaw;
		else if (type.compare("RAWM") == 0)
			reader = &SequenceReader::readRawM;
		else if (type.compare("FASTA") == 0)
			reader = &SequenceReader::readFastA;
		else if (type.compare("FASTQ") == 0)
			reader = &SequenceReader::readFastQ;
		else
			reader = &SequenceReader::readAUTO;
	} else {
		reader = NULL;
	}
}

/**
 * Returns the next record from the reader.
 *
 * NOTE: caller 'owns' the returned instance
 *
 * @return Record*, the next record OR NULL when no more available (or error)
 */
Record* SequenceReader::next() {

	if (this->reader != NULL) {
		return (*this.*reader)();
	}
	return NULL;
}


/**
 * Reads each line as a sequence (and sets id to raw1, raw2 ...)
 */
Sequence* SequenceReader::readRaw() {
    linebuffer = this->input->readLine();
    if (!this->input->eof()) {
    	Sequence* seq = new Sequence();
    	std::stringstream id;
    	id << "raw" << seqcount;
    	seq->id = id.str();
    	seq->seq = linebuffer;
    	seqcount ++;
    	return seq;
    }
    return NULL;
}
/**
 * Reads entire stream as a single (multiline) sequence
 */
Sequence* SequenceReader::readRawM() {

	// make sure there is something to read
    if (this->input->eof())
    	return NULL;

    // read all bases from stream
    std::stringstream bases;
    while (!this->input->eof()) {
    	bases << this->input->readLine();
    }

    // create read
	Sequence* seq = new Sequence();
	seq->id = "raw1";
	seq->seq = linebuffer;
	seqcount ++;
	return seq;
}
/**
 * Reads sequences in FASTA format
 */
Sequence* SequenceReader::readFastA() {
	Sequence* seq = new Sequence();

	// Header
	if (*(linebuffer.begin()) != '>')
		linebuffer = this->input->readLine();
    if (this->input->eof()) { delete seq; return NULL; }
	extractHeader(linebuffer, seq);

	// Sequence
	do {
		linebuffer = this->input->readLine();
		if (*(linebuffer.begin()) == '>' || this->input->eof()) break;			 // Stop because: next seq header || EOF
		seq->seq.append(linebuffer);
	} while (true);

	seqcount ++;
    return seq;
}
/**
 * Reads sequences in FASTQ format
 */
Sequence* SequenceReader::readFastQ() {
	Sequence* seq = new Sequence();

    linebuffer = this->input->readLine(); // Header
    if (this->input->eof()) { delete seq; return NULL; }
	extractHeader(linebuffer, seq);
    linebuffer = this->input->readLine(); // Sequence
    if (this->input->eof()) { delete seq; return NULL; }
	seq->seq = linebuffer;
    linebuffer = this->input->readLine(); // + line
    if (this->input->eof()) { delete seq; return NULL; }
    linebuffer = this->input->readLine(); // Quality
    if (this->input->eof()) { delete seq; return NULL; }
	seq->qual = linebuffer;

	seqcount ++;
    return seq;
}
/**
 * Peaks at first character to identify sequence type
 */
Sequence* SequenceReader::readAUTO() {

	// buffer the input stream
	InputBase *inputOrig = input;
	BufferedInput *inputBuffered = new BufferedInput(inputOrig, (const unsigned char*)"", 0);
	input = inputBuffered;

	// determine the seq type
	linebuffer = inputBuffered->peakLine();
	switch (*(linebuffer.begin())) {
	case '>':
		reader = &SequenceReader::readFastA;
		break;
	case '@':
		reader = &SequenceReader::readFastQ;
		break;
	default:
		reader = &SequenceReader::readRaw;
		break;
	}
	linebuffer = "";

	// read the sequence (with correct reader)
	Sequence* seq = (Sequence*) next();

	// return the input to normal
	input = inputOrig;

	return seq;
}
/**
 * Extracts the id and annotation from a FastA/Q header line and places value
 * in the provided sequence object
 */
void SequenceReader::extractHeader(std::string &header, Sequence* seq) {
	if (header.length() == 0/* || seq == NULL*/)
		return;
	size_t breakpoint = header.find(' ');
	if (breakpoint != std::string::npos) {
		seq->id = header.substr(1, breakpoint - 1);
		seq->annotation = header.substr(breakpoint + 1);
	} else {
		seq->id = header.substr(1, breakpoint);
	}
}
